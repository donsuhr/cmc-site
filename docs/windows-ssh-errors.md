#cmc-site windows development machine setup - fixing ssh certificate chain errors.

## 1. Export certificate
- From the start menu, search for `Manage Computer Certificates`
- In the `Trusted root Certification Authorities\Certificates` Folder find `Campus Management Root CA`
![](./images/windows-ssh-errors/certificates.png)
- Right click &gt; open 
- On the details tab, select `Copy to File...`
![](./images/windows-ssh-errors/cert-details.png)
- During the wizard choose Base-64 encoded option
![](./images/windows-ssh-errors/cert-export.png)

## 2. Add the certificate contents to the mingw64 ca-bundle
- Make a copy of the current `ca-bundle.crt` located in `C:/Program Files/Git/mingw64/ssl/certs/`
- Open the copy, perhaps `ca-budleCopy.crt` in a text editor
- Add the contents of the file exported in step 1 to the end of the file
![](./images/windows-ssh-errors/edit-cert.png)

## 3. Update git to use the new config file
Issue the following command to update git

```
git config --global http.sslcainfo 'C:/Program Files/Git/mingw64/ssl/certs/ca-bundleCopy.crt'
```

where `ca-bundleCopy.crt` is the name of the file you created and edited in step 2.

## 4. Update npm to use the same config file

```
npm config set cafile = 'C:/Program Files/Git/mingw64/ssl/certs/ca-bundleCopy.crt'
```

where `ca-bundleCopy.crt` is the name of the file you created and edited in step 2.

