# Working with cmc-site

## Getting to the terminal
Many commands in this document are assumed to be executed from a command line. As long as your computer is set up correctly any terminal of choice will work (cmd.exe, PowerShell, MINGW64)

If your unsure how to open your terminal and are using SourceTree, there is a terminal button in the toolbar

![install-p4merge](./images/create-a-page/sourceTreeTerminal.png)


## Start the local web server
To start the local web server, issue the following command at a terminal:

```sh
npm run grunt serve -- --open
```
The `-- --open` parameter is optional. It opens a browser window once the server is running. If omitted the website can be viewed at `http://localhost:9001`.

This server is running file watchers for javascript, sass and html.

## Create a Page
The source files for pages are located in the `app/pages/` directory. When the Metalsmith static site generator processes the files, they will be transformed into `index.html` files. So, given a file `./app/pages/about/foo.html` a new file will be created `./dist/about/foo/index.html`

To create a new page in the site, simply save a new file to the desired directory in the `./app/pages/` folder.

**NOTE** that file watchers do not see files created after the watcher is started. If the file watchers are already running, restart them after creating a new file.

1. Save a new file to `./app/pages/`. For example, `./app/pages/test/foo.html`
2. Add the desired metadata from the [metadata section](#metadata) of this document.
3. Start or restart the local server.

<div id='metadata'/>
### Page Metadata YAML
The top of each html page has YAML data to define the pages metadata.

- title: {string} The title of the page. Used in metadata.
- label: {string} Overrides the title in the left nav menu.
- layout: {string} The layout of the page. Defaults to `cmc-site.hbs`.
- link-to: {string} Url. Causes the left nav item to link to the given url.
- menu-priority: {number} [0-1] Alter the alphabetical ordering of pages in the section. A value of `1` will be closer to the top than a value of `0`. Defaults to `0.5`.
- publish: {string|date} [draft, private, 2021-12-21, **1994-01-05T08:15:30-05:00**] Alters the visibility of pages during publishing. In a production environment `draft` and `private` will not be shown. Pages with a date will not be shown until that date. [publish plugin info](https://github.com/mikestopcontinues/metalsmith-publish). [Date Format ISO 8601](https://www.w3.org/TR/NOTE-datetime) 
- language: {string} Sets `html lang` attribute. Defaults to `pt-br` in `/pt-br/`, `en-in` in `/en-in/` and `en-us` otherwise.
- og-type: {string} Sets the Facebook OG Type. Defaults to `article`
- private: {bool} true to hide in sitemap
- lastmod: {string|date} lastmod for sitemap [sitemap plugin](https://github.com/ExtraHop/metalsmith-sitemap)
- banner-image: {string} Relative Url. Also used for Google Custom Search and Facebook OG metadata.
- og-image: {string} Relative URL. Sets the image for `<meta property="og:image">`. If none is set, it defaults to the page `banner-image` then  `config.facebook.fbDefaultImg`. [fb og info](https://developers.facebook.com/docs/reference/opengraph)
- google-image: {string} [Relative URL | `disabled`] Sets the image for `<PageMap> <Datatype> [thumbnail]`. Will default to `banner-image` if `banner-image` is set. Otherwise it will be explicity `disabled` (no image shown).
- pageCssClass: {string} Adds a css class to the page `<body>`.
- show-in-quick-launch: {boolean} [true | false] Toggle display in quick launch menus. Defaults to `true` in most sections. See `/build/metalsmith.js` for section specific defaults.
- show-in-left-nav: {boolean} [true | false] Toggle display in main menu. Defaults to `true`
- meta-description: {string} Adds a `<meta type="description">` and `<meta property="og:description">` 
- additional-stylesheets: {Object[]} Add an additional stylesheet to the page.
    - href: {string} Relative URL.
    - inline: [true, false] Copy the css into the page on publish
- additional-scripts: {Object[]} Add an additional stylesheet to the page.
    - src: {string} Relative Url.
    - inline: {boolean} [true | false] Copy the css into the page on publish

### regarding the publish date:
- no time (2017-09-21)
    - local dev machine is -04:00, page always shows day before
    - server is +00:00, page shows with date given
    - will publish if pushed after 8PM the day before the date given

- adding time (2017-09-21T13:12:00)
    - local dev machine is -04:00, puts yesterdays date if time given is before 04:00
    - server is +00:00, page shows with date given
    - will publish if time given -04:00 is still before the current time when pushed

- adding time and zone (2017-09-21T13:12:00-04:00)
    - local dev machine puts date supplied
    - sever will put tomorrows date if time given is after 8PM
    - will publish if time given is before the current time when pushed

### A typical Example: 

```yaml
---
# <!-- @formatter:off -->
title:   Advisories and Analysts
label: 4-Year Private
meta-description: Institutions depend on Campus Management ...
banner-image: /images/banners/mgmt/Banner_MgmtHome.png
# <!-- @formatter:on -->
---
```

### Page Content Layout

The pages are laid out in two stages. The first layout is an outer level and is analogous to a "Master Page." It is specified in YAML metadata with the `layout` property and it defaults to `cmc-site.hbs`. The layout is located in `/app/metalsmith/layouts/`. 

The second stage creates the inner level of the page using `handlebars-layouts`. There are currently one layouts located in `./app/metalsmith/partials/hb-layouts/`. [`v3`]. Its use is optional. It provides the standard wrapper tags to the main content. ie: `div.layout__bg--main` > `div.layout__container` > `div.layout__article-wrapper` > `main`

```html

{{#extend "hb-layouts/v3"}}

{{#content "body"}}
    <article class="cmc-article">
        <img class="cmc-article__article-image" src="{{banner-image}}"/>
    
        <h1 class="cmc-article__title">{{title}}</h1>
        <p class="cmc-article__subtitle">
            Keep Alumni and Donors Engaged with Your Institution
        </p>
    
        <p class="cmc-article__intro-p">
            Private colleges and universities depend heavily on building 
            strong relationships with students and their families over
            generations. That’s why institutions depend on Campus 
            Management’s enterprise CRM solution to engage students and 
            alumni through highly personalized communications and 
            services.
        </p>
    </article>
{{/content}}

{{#content "after-body"}}
    {{>request-demo__product-center__case-studies}}
{{/content}}

{{/extend}}

```

## Common styles

```html
<p class="cmc-article__title"><!-- small grey text --></p>
<p class="cmc-article__subtitle"><!-- large blue text --></p>
<p class="cmc-article__intro-p"><!-- medium green text --></p>

```
