#cmc-site development windows machine setup

- [Install/Update Git](#installGit)
- [Install/Update Node](#installNode)
- [Install/Update Yarn](#installYarn)
- [Install/Update Source Tree](#installSourceTree)
- [Install/Update P4Merge](#installP4)
- [Set up SSH](#ssh)


<div id='installGit'/>
##Install or Update Git 

[Source](https://confluence.atlassian.com/bitbucketserver/installing-and-upgrading-git-776640906.html#InstallingandupgradingGit-InstallorupgradeGitonWindows)

Download the latest stable Git release from the [Git website](http://git-scm.com/download/win).

Do **not** select `Use Git Bash only` when installing or upgrading Git for the Bitbucket Server instance -- this will not work with Bitbucket Server.

![msysgit-path](./images/machine-setup/msysgit-path.png)

If you are installing Git for the first time, set your user name and email:

```
git config --global user.name "John Doe"
git config --global user.email johndoe@example.com
```

<div id='installNode'/>
##Install or Update Node
Download the latest LTS release from the [Nodejs website](https://nodejs.org/en/download/) 

![download-node](./images/machine-setup/downloadNode.png)

##Update NPM
Use the instructions found [here](https://github.com/felixrieseberg/npm-windows-upgrade)

<div id='installYarn'/>
##Install or Update Yarn
Use the instructions found [here](https://yarnpkg.com/en/docs/install)

<div id='installSourceTree'/>
##Install or Update SourceTree
SourceTree is a free optional Git GUI. 

To upgrade:

- Open SourceTree
- Navigate to `Help` > `Check for Updates`

To install, download the latest release from the [Atlassian Website](https://www.atlassian.com/software/sourcetree)


<div id='installP4'/>
##Install or Update p4Merge
p4Merge is a free optional merge tool.

[Direct Link](http://www.perforce.com/downloads/perforce/r15.2/bin.ntx64/p4vinst64.exe)

During installation, **install only** the p4Merge tool:
![install-p4merge](./images/machine-setup/p4MergeInstall.png)

After installation, set p4merge as your difftool:

```
git config --global merge.tool p4merge

git config --global mergetool.p4merge.path 'C:\Program Files\Perforce\p4merge.exe'

git config --global diff.tool p4merge

git config --global difftool.p4merge.path 'C:\Program Files\Perforce\p4merge.exe'

```

<div id='ssh'/>
##Set up SSH
Use the instructions [here](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html) to set up SSH

##Get access to the project.
request access to the following repos:

- git+ssh://git@bitbucket.org/zorg128/cmc-site.git
- git+ssh://git@bitbucket.org/zorg128/cmc-auth.git



##Speed up the project
### Disable Windows Indexing for the folder
To speed up working with the files in cmc-site project, disable indexing on your project folder. Follow the [instructions](http://helpdeskgeek.com/windows-7/windows-7-file-search-indexing-options/) here.

### Disable Windows Defender real time scanning for the folder
[Info](http://stackoverflow.com/questions/24612297/why-is-my-ember-cli-build-time-so-slow-on-windows)

[Instructions](https://github.com/ember-cli/ember-cli/issues/1294#issuecomment-48151542)

Also exclude the Yarn folder `%LocalAppData%\Yarn`

##Clone and run the project
Clone:

`git clone -b static-site git@bitbucket.org:zorg128/cmc-site.git`

Install:

`npm install`

Get or create a `.env` file:

```
AUTH0_AUDIENCE=TmbmVVrsFLOnzWK8XvBBI99P9SoUBSRH
AUTH0_ISSUER=https://cmc.auth0.com/
AUTH0_DOMAIN=cmc.auth0.com
AUTH0_ENDPOINT=https://cmc.auth0.com/api/v2/

CORS_ORIGIN_CMC=https://cmc.suhrthing.com:9000
CORS_ORIGIN_PRODUCT_CENTER=https://product-center.suhrthing.com:9000
CORS_ORIGIN_API=https://api.suhrthing.com:9000
CORS_ORIGIN_TLD=suhrthing.com

GOOGLE_ANALYTICS_ID=UA-7629132-1
LIVE_CHAT_ID=6327581
LEAD_LANDER_ID=14236

# from cmc-test. used to dl the product center to the dist folder.
AWS_ACCESS_KEY_ID__CMC_DEMOCENTER_DL=###
AWS_SECRET_ACCESS_KEY__CMC_DEMOCENTER_DL=###

FB_APP_ID=537400829608466
FB_DEFAULT_IMG=/SiteCollectionImages/CMC-v2/cmc-logo.png
FB_SITE_NAME=Campus Management Corp.

```

Run: 

`npm run grunt serve -- --open`
