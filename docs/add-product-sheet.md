# Add a Product Sheet

This document describes adding a product sheet to an S3 bucket and adding its metadata.

## Requirements

You will need to be able to log into the CMC admin website currently located at [https://admin-3.campusmanagement.com](https://admin-3.campusmanagement.com).
Credentials are currently created manually on the Auth0 account.

If you have S3 access, bulk actions may be easier using the S3 Bucket page. S3 access is required to delete files.

If you directed to an Amazon Web Services login page requesting an email address, use the following to log in with your username instead [https://912225610445.signin.aws.amazon.com/console](https://912225610445.signin.aws.amazon.com/console)

## Info

The admin site first pulls a list of files from a given S3 bucket. It then queries the MongoDB database for the same bucket name for the metadata. The two lists are then merged based on the file name from S3. Since the metadata is maintained separately it can become unsynchronized.

Only files with metadata and their `display` property set to true (checked) will show up on the public website.

Metadata with no associated file will turn red in the admin site. If no file replacement is expected to uploaded, the row should be deleted.

## Add the Product Sheet file

After logging into the [admin site](https://admin-3.campusmanagement.com). Choose the correct site and language from the navigation. This example will assume the corporate site and 'en-us' language group.

![choose a site](./images/add-product-sheet/product-sheets--choose-a-site.png)

To upload or replace files, use the click the 'Choose Files' button.  Or use the S3 Bucket management page

Once the file is uploaded, its metadata can be edited.

## Delete a Product Sheet file

Use the S3 admin website to delete files. There is a link to the bucket admin page in the upper right of the admin table.  Click 'Open Bucket' to load the S3 page.
![open bucket](./images/add-product-sheet/product-sheets--open-bucket.png)

Right click the file you would like to delete and select 'Delete' from the context menu.

![delete file](./images/add-product-sheet/product-sheets--delete-file.png)


After deleting the file on S3 refresh the admin page. You will see the rows with missing files turn red.

![missing file](./images/add-product-sheet/product-sheets--missing-file.png)


Click the 'Edit' button on the row and then click 'Delete Metadata'.



