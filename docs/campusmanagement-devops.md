#CampusManagement.com Cloud Providers

##Static Hosting 
[Netlify](https://www.netlify.com/)

Host for all static pages. All pages are static except the Product Center and administration site.


###Pricing
$7.50/mo

###campus url(s)
- https://**www**.campusmanagement.com/
- https://www.talisma.com (...etc.)


##Dynamic Hosting 
[Amazon Web Services](https://aws.amazon.com/)

- EC2: Host for dynamic pages and api.
- S3: Host for pdf assets and other assets not stored in version control.

###Pricing
Varies. ~$40/mo

###campus url(s)
- https://**product-center**.campusmanagement.com
- https://**api**.campusmanagement.com
- https://**admin**.campusmanagement.com
- https://product-center.talisma.com (...etc.)


##SSL 
[Let's Encrypt](https://letsencrypt.org/)

###Pricing
$0


##Database Hosting
[mLab](https://mlab.com/)

MongoDB host. Hosts all lists. 

- Product Sheets URLs and their descriptions
- Case Studies URLs and their descriptions
- Password Protected Document URLs
- Demo Request Form Submissions
- Contact Form Submissions
- etc

###Pricing
$15/mo

##Realtime Database Hosting
[Firebase](https://firebase.google.com/)

Provides real time database for CampusInsight schedules. 
###Pricing
The free tier would be sufficient. Consider 1 month of paid tier during the conference.

$25/yr

##Authentication Provider
[Auth0](https://auth0.com/) 

Provides database hosting for authentication and user profile information.

###Pricing
$12/mo


##Mail Server
[MailGun](http://www.mailgun.com/)

Sends mail to users, admin and Janus for submitted forms.

###Pricing
Varies: $0 - $5/mo

###campus url(s)
- mg.campusmanagement.com


##Search
[Google Site Search](https://www.google.com/work/search/products/gss.html)

###Pricing 
$9/mo (removes google branding and ads)


##Git Version Control
[Bitbucket](https://bitbucket.org/)

The project is currently hosted across several **private** git repos. However, the project does not require private hosting. The project does not contain sensitive data. Password protected assets (Product Center and Legal Documentation) is store on Amazon s3.

- https://bitbucket.org/zorg128/cmc-site
- https://bitbucket.org/zorg128/cmc-democenter-server
- https://bitbucket.org/zorg128/cmc-api
- https://bitbucket.org/zorg128/cmc-auth
- https://bitbucket.org/zorg128/cmc-admin-client
- etc

###Pricing
- Public repos are free.
- Private repos are free for the first five users. $10/mo for 10 users.
