# Steps to push updates to live site on Netlify

## 1. Close terminal
This allows sync to happen without errors.

## 2. Commit
Commit any updates you have made to your local site.

## 3. Pull
Pull any possible updates another team member has made to the site.

## 4. Push
Push your updates.

## 5. Switch to Netlify
Double-click on Netlify in left side menu. Netlify menu text should now be bold.

## 6. Pull
This will pull any updates from live Netlify site into your local site.

## 7. Right click on Static menu text
Select Merge static site into current branch. Click OK in dialog box.

## 8. Push
This will push updates.

## 9. Switch back to Static
This ensures future updates only effect your local site.