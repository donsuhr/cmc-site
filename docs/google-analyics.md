# Google Analyics

## Events
### nav
#### Users who used the main navigation
1. Behavior > Events > Overview
2. Select `Event Category` of `nav`
3. Change Primary Dimension to `Event Action`
4. Select `Use Main Nav`

##### Values:
+ **0**: Users who used the main navigation when it was already opened
+ **1**: Users who used the main navigation when it was closed. The user had to toggle the nav to use it
 
#### Users who toggled the main nav in desktop mode
   
1. Behavior > Events > Overview
2. Select  `Event Category` of `nav`
3. Change Primary Dimension to `Event Action`
4. Select `toggle main nav`

##### Values:
+ **0**: Users who toggled the menu when the menu was set to opened: user likely closed the menu.
+ **1**: Users who toggled the menu when the menu was set to closed: user likely opened the menu.

### Demo Center

#### Users who logged into the Demo Center
1. Behavior > Events > Overview
2. Select  `Event Category` of `democenter`
3. Change Primary Dimension to `Event Action`
4. Select `login`

##### Values:
+ **0**: User who did not have a cookie set. Either not a return visitor, has LocalStorage disabled or it was cleared since last visit.
+ **1**: Return visiter