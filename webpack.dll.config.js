'use strict';

const webpack = require('webpack');

const production = process.env.NODE_ENV === 'production';
const mode = production ? 'production' : 'development';

module.exports = {
    mode,
    entry: {
        dll: [
            'algoliasearch',
            'backbone',
            'basil.js',
            'bootstrap-sass/assets/javascripts/bootstrap/carousel',
            'bootstrap-sass/assets/javascripts/bootstrap/modal',
            'bootstrap-sass/assets/javascripts/bootstrap/transition',
            'error-tojson',
            'form-serialize',
            'garlicjs',
            // 'google-maps',
            'hammerjs',
            'handlebars/runtime',
            'headroom.js',
            'jquery',
            'jquery-ui',
            'js-cookie',
            'jstree',
            'jwt-decode',
            'lodash',
            'moment',
            'parsleyjs',
            'query-string',
            'sal.js',
            'sanitize-html',
            'superfish',
            'underscore',
            'url-parse',
            'vanilla-lazyload',
            'whatwg-fetch',
        ],
    },

    output: {
        filename: '[name].bundle.js',
        path: __dirname,
        // The name of the global variable which the library's
        // require() function will be assigned to
        library: '[name]_lib',
    },
    resolve: {
        modules: ['node_modules'],
        alias: {
            handlebars: 'handlebars/runtime',
            jquery: 'jquery/src/jquery',
        },
    },

    plugins: [
        new webpack.ProvidePlugin({
            'window.jQuery': 'jquery', // garlic, headroom, bootstrap
            jQuery: 'jquery', // bootstrap parsley superfish
        }),
        new webpack.DllPlugin({
            // The path to the manifest file which maps between
            // modules included in a bundle and the internal IDs
            // within that bundle
            path: '[name]-manifest.json',
            // The name of the global variable which the library's
            // require function has been assigned to. This must match the
            // output.library option above
            name: '[name]_lib',
        }),
    ],
};
