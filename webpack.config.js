'use strict';

const webpack = require('webpack');
const path = require('path');
const fs = require('fs');
const AssetsPlugin = require('assets-webpack-plugin');
const SriPlugin = require('webpack-subresource-integrity');
const glob = require('glob');

const production = process.env.NODE_ENV === 'production';
const mode = production ? 'production' : 'development';

const entry = () => {
    const cmcSite = [path.join(__dirname, 'app/scripts/index.js')];
    if (!production) {
        cmcSite.unshift(
            'webpack/hot/dev-server',
            'webpack-hot-middleware/client?reload=true',
        );
    }

    return glob
        .sync('./app/scripts/pages/!(*.e2e).js', { absolute: true })
        .reduce(
            (acc, x) => {
                const ext = path.extname(x);
                if (!production || !x.includes('.dev')) {
                    acc[`page--${path.basename(x, ext)}`] = x;
                }
                return acc;
            },
            {
                'cmc-site': cmcSite,
            },
        );
};

const plugins = [
    new SriPlugin({
        hashFuncNames: ['sha384'],
        enabled: mode === 'production',
    }),
    new AssetsPlugin({
        entrypoints: false,
        prettyPrint: true,
        filename: 'webpack-assets.json',
        integrity: true,
    }),
    new AssetsPlugin({
        entrypoints: true,
        prettyPrint: true,
        filename: 'webpack-manifest.json',
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.ProvidePlugin({
        'window.jQuery': 'jquery', // garlic, headroom, bootstrap
        jQuery: 'jquery', // bootstrap parsley superfish
    }),
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
        },
    }),
];
if (!production) {
    plugins.push(new webpack.HotModuleReplacementPlugin());

    const manifestFile = path.resolve(process.cwd(), 'dll-manifest.json');
    let manifestJson;
    try {
        manifestJson = JSON.parse(fs.readFileSync(manifestFile));
    } catch (e) {
        manifestJson = false;
    }
    if (manifestJson) {
        plugins.push(
            new webpack.DllReferencePlugin({
                context: '.',
                manifest: manifestJson,
            }),
        );
    }
}

module.exports = {
    stats: 'errors-only',
    devtool: production ? 'source-map' : '#eval-source-map',
    context: path.join(__dirname, 'app', 'scripts'),
    entry,
    mode,
    plugins,
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            maxInitialRequests: 25,
            maxAsyncRequests: Infinity,
            minSize: 20000,
            chunks: 'all',
        },
    },
    resolve: {
        modules: ['node_modules', path.resolve(__dirname, 'app')],
        alias: {
            config$: path.join(__dirname, 'config.js'),
            handlebars: 'handlebars/runtime',
            jquery: 'jquery/src/jquery',
        },
    },

    output: {
        crossOriginLoading: 'anonymous',
        path: path.join(__dirname, 'dist/'),
        filename: production
            ? 'scripts/[name]-[chunkhash].js'
            : 'scripts/[name].js',
        chunkFilename: production
            ? 'scripts/[name]-[chunkhash].js'
            : 'scripts/[name].js',
        publicPath: '/',
    },
    module: {
        rules: [
            {
                test: /nls[/\\]\S*\.json/,
                loader: 'amdi18n-loader',
                include: path.join(__dirname, '/app/components/'),
                type: 'javascript/auto',
            },
            {
                test: /\.js$/,
                use: ['babel-loader'],
                include: [
                    path.join(__dirname, '/app/'),
                    path.join(__dirname, '/config.js'),
                    path.join(__dirname, '/node_modules/dom-utils'),
                    path.join(__dirname, '/node_modules/lazyload'),
                    path.join(__dirname, '/node_modules/google-maps'),
                    /\/node_modules\/cmc-/,
                ],
                exclude: [path.join(__dirname, '/app/metalsmith/helpers/')],
            },
            {
                test: /\.hbs$/,
                use: [
                    {
                        loader: 'handlebars-loader',
                        query: {
                            debug: false,
                            runtime: 'handlebars/runtime',
                            helperDirs: [`${__dirname}/app/metalsmith/helpers`],
                            partialDirs: [
                                `${__dirname}/app/metalsmith/partials`,
                            ],
                        },
                    },
                ],
                include: [
                    path.join(__dirname, '/app/'),
                    /\/node_modules\/cmc-/,
                ],
            },
            {
                test: /\.html$/,
                loader: 'underscore-template-loader',
                query: {
                    variable: 'data',
                },
            },
        ],
    },
};
