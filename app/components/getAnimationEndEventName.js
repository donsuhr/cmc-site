// no tests https://github.com/tmpvar/jsdom/issues/1781
import _forOwn from 'lodash/forOwn';

const eventName = (function getAnimationEvent() {
    let transitionEndEvent = null;
    const el = document.createElement('fakeelement');
    const transitions = {
        animation: 'animationend',
        WebkitAnimation: 'webkitAnimationEnd',
        OAnimation: 'oAnimationEnd',
        msAnimation: 'MSAnimationEnd',
    };

    // eslint-disable-next-line consistent-return
    _forOwn(transitions, (value, key, object) => {
        if (el.style[key] !== undefined) {
            transitionEndEvent = value;
            return false;
        }
    });
    return transitionEndEvent;
}());

export default eventName;
