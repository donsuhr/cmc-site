/* global dataLayer */

/*
Core Web Vitals
  Cumulative Layout Shift (CLS)
  First Input Delay (FID)
  Largest Contentful Paint (LCP)
Other Web Vitals
  First Contentful Paint (FCP)
  Time to First Byte (TTFB)
*/

import {
    getCLS, getFID, getLCP, getTTFB, getFCP,
} from 'web-vitals';

function sendToGoogleAnalytics({ name, delta, id }) {
    // Assumes the global `dataLayer` array exists, see:
    // https://developers.google.com/tag-manager/devguide
    if (typeof dataLayer !== 'undefined') {
        dataLayer.push({
            event: 'web-vitals',
            event_category: 'Web Vitals',
            event_action: name,
            // Google Analytics metrics must be integers, so the value is rounded.
            // For CLS the value is first multiplied by 1000 for greater precision
            // (note: increase the multiplier for greater precision if needed).
            event_value: Math.round(name === 'CLS' ? delta * 1000 : delta),
            // The `id` value will be unique to the current page load. When sending
            // multiple values from the same page (e.g. for CLS), Google Analytics can
            // compute a total by grouping on this ID (note: requires `eventLabel` to
            // be a dimension in your report).
            event_label: id,
        });
    }
}

getCLS(sendToGoogleAnalytics);
getFID(sendToGoogleAnalytics);
getLCP(sendToGoogleAnalytics);
getTTFB(sendToGoogleAnalytics);
getFCP(sendToGoogleAnalytics);
