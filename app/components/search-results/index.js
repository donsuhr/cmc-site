/* eslint no-underscore-dangle: ["error", {"allow": ["_highlightResult"]}] */
import UrlParse from 'url-parse';
import sanitizeHtml from 'sanitize-html';
import queryString from 'query-string';
import get from 'lodash/get';
import searchClient from '../../scripts/algoliaSearchClient';
import template from './search-results.template.hbs';
import snippetFn from './snippet';

function resultMapper(x) {
    const title = sanitizeHtml(get(x._highlightResult, 'title.value', ''), {
        allowedTags: ['em'],
    });
    const snippet = x._highlightResult.snippet
        ? x._highlightResult.snippet.value
        : false;

    const preCleanDescription = get(
        x._highlightResult,
        'description.value',
        '',
    );
    const preCleanBody = get(x._highlightResult, 'body.value', '');

    const preClean = `${preCleanDescription}${
        preCleanDescription ? ' -' : ''
    } ${preCleanBody}`.replace(/({{[^}]+}})/g, '');

    const cleanBody = sanitizeHtml(preClean, {
        allowedTags: ['em'],
    });
    const body = snippet
        || snippetFn(
            'em',
            cleanBody
                .replace(/(?:\n|\r)+/g, '')
                .replace(/\s\s+/g, ' ')
                .trim(),
            get(x._highlightResult, 'body.matchedWords', []),
            20,
        );

    const publishDate = x.publish && new Date(x.publish);
    const publish = publishDate
        ? `${
            publishDate.getUTCMonth() + 1
        }/${publishDate.getUTCDate()}/${publishDate.getUTCFullYear()}`
        : false;
    const products = x.products ? x.products.join(',') : false;

    return {
        title,
        body,
        url: x.url,
        category: x.category,
        type: x.type,
        publish,
        products,
        bannerImage: x['banner-image'],
    };
}

export default function search(index, query) {
    const searchIndex = searchClient.initIndex(index);
    searchIndex
        .search(query, {
            attributesToRetrieve: [
                'url',
                'publish',
                'type',
                'banner-image',
                'products',
            ],
            attributesToSnippet: [],
            attributesToHighlight: ['body', 'title', 'description'],
        })
        .then((content) => {
            const target = document.querySelector('#SearchResultContainer');
            if (content.nbHits > 0) {
                const vm = content.hits.map(resultMapper);
                target.innerHTML = template({
                    data: vm,
                });
            } else {
                const querySanitized = sanitizeHtml(query);
                target.innerHTML = `<p>No items matched the query '${querySanitized}'`;
            }
        })
        .catch((err) => {
            const target = document.querySelector('#SearchResultContainer');
            if (err) {
                target.innerHTML = 'Error loading search results';
            }
        });
}

export function getIndexFromDom() {
    return document
        .getElementById('SearchResultContainer')
        .getAttribute('data-index');
}

export function getQueryFrom404Url(url) {
    const urlParts = new UrlParse(url);
    const page = decodeURIComponent(urlParts.pathname);

    return page
        .split('/')
        .join(' ')
        .replace(/(en-us|pt-br|en-in|Pages|\.aspx|\.html)/gi, '')
        .replace(/\s\s+|-/gi, ' ')
        .trim();
}

export function getQueryFromSearch(windowSearch) {
    const parsed = queryString.parse(windowSearch);
    return parsed.q;
}
