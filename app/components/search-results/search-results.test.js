const {
    describe, it, afterEach, beforeEach,
} = require('mocha');
const { expect } = require('chai');
const nock = require('nock');
const sut = require('./index');

describe('search-results', () => {
    beforeEach(() => {
        nock.disableNetConnect();
    });

    afterEach(() => {
        nock.cleanAll();
    });

    it('returns the index from the dom', () => {
        document.body.innerHTML = `
            <div id="SearchResultContainer" class="app" data-index="cmc--en-us">
                <div class="loading--placeholder">Loading...</div>
            </div>
            `;
        expect(sut.getIndexFromDom()).to.equal('cmc--en-us');
    });

    it('returns a query for a 404 page', () => {
        expect(sut.getQueryFrom404Url('/pt-br/foo/bar')).to.equal('foo bar');
        expect(sut.getQueryFrom404Url('/en-us/foo.aspx')).to.equal('foo');
        expect(
            sut.getQueryFrom404Url('/en-us/Pages/about-us/partners.aspx'),
        ).to.equal('about us partners');
    });

    it('returns a query from a `q` param', () => {
        expect(sut.getQueryFromSearch('x=y&q=search')).to.equal('search');
        expect(sut.getQueryFromSearch('q=search')).to.equal('search');
        expect(sut.getQueryFromSearch('q=hello%20world')).to.equal(
            'hello world',
        );
    });

    it('shows a search error on error', (done) => {
        document.body.innerHTML = `
            <div id="SearchResultContainer" class="app" data-index="cmc--en-us">
                <div class="loading--placeholder">Loading...</div>
            </div>
        `;
        nock(/algolia/)
            .filteringPath(/.*/, '')
            .post('')
            .reply(404);
        sut.default('cmc--en-us', 'campus');
        setTimeout(() => {
            expect(
                document.getElementById('SearchResultContainer').textContent,
            ).to.equal('Error loading search results');
            done();
        }, 50);
    });

    it('shows no match error on zero results', (done) => {
        document.body.innerHTML = `
            <div id="SearchResultContainer" class="app" data-index="cmc--en-us">
                <div class="loading--placeholder">Loading...</div>
            </div>
        `;
        nock(/algolia/)
            .filteringPath(/.*/, '')
            .post('')
            .replyWithFile(200, `${__dirname}/fixtures/zero-results.json`);

        sut.default('cmc--en-us', 'nothing');
        setTimeout(() => {
            expect(
                document.getElementById('SearchResultContainer').textContent,
            ).to.equal("No items matched the query 'nothing'");
            done();
        }, 10);
    });

    it('shows 20 results', (done) => {
        document.body.innerHTML = `
            <div id="SearchResultContainer" class="app" data-index="cmc--en-us">
                <div class="loading--placeholder">Loading...</div>
            </div>
        `;
        nock(/algolia/)
            .filteringPath(/.*/, '')
            .post('')
            .replyWithFile(200, `${__dirname}/fixtures/campus.json`);

        sut.default('cmc--en-us', 'campus');
        setTimeout(() => {
            expect(
                document.querySelectorAll('.search-result-item').length,
            ).to.equal(20);
            done();
        }, 10);
    });
});
