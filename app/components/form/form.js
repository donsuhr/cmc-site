/* eslint-disable no-underscore-dangle */

import $ from 'jquery';
import _each from 'lodash/each';
import _keys from 'lodash/keys';
import formSerialize from 'form-serialize';
import config from 'config';
import { checkStatus, parseJSON } from 'fetch-json-helpers';
import formalizer from './formalizer';
import { cloneNavigator } from '../cloneNavigator';
import 'parsleyjs';
import 'garlicjs';
import {
    getLang,
    eventEmitter as menuLangEmitter,
    EVENT_LANG_CHANGED,
} from '../menu/menu-lang-filter';

let hasLocalStorage = true;

try {
    localStorage.setItem('localStorageTest', true);
} catch (e) {
    hasLocalStorage = false;
}

const createForm = function createForm(options) {
    const _options = $.extend({}, options);
    const _$el = $(`#${options.formDomID}`);
    let _formalizer = null;
    let _$loadingEl;
    let _$formErrorEl;

    if (!{}.hasOwnProperty.call(options, 'formDomID')) {
        throw new Error('CMC-form init error: missing formDomID');
    }

    if (!{}.hasOwnProperty.call(options, 'leadSource')) {
        throw new Error('CMC-form init error: missing leadSource');
    }

    if (!{}.hasOwnProperty.call(options, 'endpoint')) {
        throw new Error('CMC-form init error: missing endpoint');
    }

    if (
        _options.successRedirectUrl === ''
        && !{}.hasOwnProperty.call(options, 'cb')
    ) {
        throw new Error(
            'CMC-form init error: missing successRedirectUrl or a cb',
        );
    }

    function hideLoading() {
        if (_$loadingEl && _$loadingEl.length) {
            _$loadingEl.remove();
        }
    }

    function showLoading() {
        hideLoading();
        _$loadingEl = $('<div class="loading">Loading...</div>');
        _$el.append(_$loadingEl);
        const x = _$el.innerWidth() / 2 - _$loadingEl.innerWidth() / 2;
        const y = Math.max(
            0,
            _$el.innerHeight() / 2 - _$loadingEl.innerHeight() / 2,
        );
        _$loadingEl.css({ left: x, top: y });
    }

    function hideError() {
        if (_$formErrorEl && _$formErrorEl.length) {
            _$formErrorEl.remove();
        }
    }

    function showError() {
        hideError();
        _$formErrorEl = $(
            '<p class="error">There was an error processing your request. Please try again later</p>',
        );
        _$el.append(_$formErrorEl);
    }

    function clearGarlicLocalStorage() {
        if (!hasLocalStorage) {
            return;
        }
        _each(_keys(localStorage), (key) => {
            if (key.match(/^garlic/)) {
                localStorage.removeItem(key);
            }
        });
    }

    function initFormalizer() {
        const hasFormalizerOptions = {}.hasOwnProperty.call(_options, 'formalizerOptions')
            && typeof _options.formalizerOptions === 'object';
        if (hasFormalizerOptions) {
            const formalizerOptions = $.extend({}, _options.formalizerOptions, {
                $el: _$el,
                $saveBtn: _$el.find('[name="btnSave"]'),
            });
            _formalizer = formalizer.createInstance(formalizerOptions);
        }
    }

    function processFormalizer() {
        return _formalizer ? _formalizer.submitFormalizer() : Promise.resolve();
    }

    function postToUnify(obj) {
        let title;
        if (Object.hasOwnProperty.call(obj, 'title')) {
            title = obj.title;
        } else if (Object.hasOwnProperty.call(obj, 'jobTitle')) {
            title = obj.jobTitle;
        } else {
            title = '';
        }
        const unifyObj = {
            firstname: Object.hasOwnProperty.call(obj, 'firstName')
                ? obj.firstName
                : '',
            lastname: Object.hasOwnProperty.call(obj, 'lastName')
                ? obj.lastName
                : '',
            email: Object.hasOwnProperty.call(obj, 'email') ? obj.email : '',
            phone: Object.hasOwnProperty.call(obj, 'phone') ? obj.phone : '',
            instorg: Object.hasOwnProperty.call(obj, 'institution')
                ? obj.institution
                : '',
            reason: Object.hasOwnProperty.call(obj, 'reasonForInquiry')
                ? obj.reasonForInquiry
                : '',
            solution: Object.hasOwnProperty.call(obj, 'solution')
                ? obj.solution
                : '',
            message: Object.hasOwnProperty.call(obj, 'message')
                ? obj.message
                : '',
            salutation: Object.hasOwnProperty.call(obj, 'salutation')
                ? obj.salutation
                : '',
            institutiontype: Object.hasOwnProperty.call(obj, 'institutionType')
                ? obj.institutionType
                : '',
            country: Object.hasOwnProperty.call(obj, 'country')
                ? obj.country
                : '',
            title,
            source: obj.leadSource,
            url: obj.url,
        };

        let request;

        if (!unifyObj.email.includes('nounify')) {
            request = fetch(
                'https://prod-125.westus.logic.azure.com:443/workflows/dff61b83926240f98a819d8b0025db69/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=LWkDgWmMs-rbArSgcydPEN0ed3B5MBTw_5vaT-VX5D0&=',
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(unifyObj),
                },
            );
        } else {
            request = Promise.resolve({ 'nounify-email': true });
        }

        return { request, unifyObj };
    }

    async function submitForm() {
        const form = _$el.get(0);
        const obj = formSerialize(form, {
            hash: true,
            empty: false,
            disabled: true,
        });

        obj.navigator = cloneNavigator();
        obj.url = window.location.href;
        obj.referrer = document.referrer;

        const optionsLeadSource = Object.hasOwnProperty.call(
            options,
            'leadSource',
        )
            ? options.leadSource
            : '';

        if (Object.hasOwnProperty.call(obj, 'webcast')) {
            obj.leadSource = `${optionsLeadSource} - webcast - ${obj.webcast}`;
        } else if (Object.hasOwnProperty.call(obj, 'caseStudy')) {
            obj.leadSource = `${optionsLeadSource} - case study - ${obj.caseStudy}`;
        } else {
            obj.leadSource = optionsLeadSource;
        }

        if (options.postToUnify) {
            const { request: unifyRequest, unifyObj: unifyData } = postToUnify(
                obj,
            );
            obj.unifyObj = unifyData;

            const timeout = new Promise((resolve, reject) => {
                setTimeout(() => {
                    reject(new Error('Timed out in 3000ms.'));
                }, 3000);
            });

            const unifyRequestProcessed = unifyRequest.then(
                (response) => {
                    if (response.status) {
                        return response.text().then((text) => {
                            let headers;
                            try {
                                headers = Object.fromEntries(
                                    response.headers.entries(),
                                );
                            } catch (e) {
                                // ie
                                headers = Object.fromEntries(
                                    Object.entries(response.headers.map),
                                );
                            }
                            return {
                                status: response.status,
                                body: text,
                                headers,
                            };
                        });
                    }
                    // was nounify email
                    return { status: -1, body: response };
                },
                (error) => ({
                    status: -2,
                    body: {
                        error: `Fetch reject - network error - ${error.message}`,
                    },
                }),
            );

            await Promise.race([unifyRequestProcessed, timeout]).then(
                (result) => {
                    obj.unifyResult = result;
                },
                (error) => {
                    obj.unifyResult = { status: -3, body: error.message };
                },
            );
        } else {
            obj.unifyObj = { was: 'false' };
        }

        const endpoint = `${config.domains.api}/${options.endpoint}`;

        fetch(endpoint, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(obj),
        })
            .then(checkStatus)
            .then(parseJSON)
            .then((result) => {
                clearGarlicLocalStorage();
                if (_options.cb) {
                    _options.cb(null, result);
                }
                if (_options.successRedirectUrl) {
                    window.location.href = _options.successRedirectUrl;
                }
            })
            .catch((error) => {
                // eslint-disable-next-line no-console
                console.log('request failed', error);
                showError();
                hideLoading();
                if (_options.cb) {
                    _options.cb(error, null);
                }
            });
    }

    function updateSaveButtonScript() {
        if (hasLocalStorage) {
            _$el.garlic();
        }
        const parsleyApi = _$el.parsley();
        const $saveBtn = _$el.find('[name="btnSave"]');

        initFormalizer();
        $saveBtn.off().on('click', (event) => {
            event.preventDefault();
            $saveBtn.prop('disabled', true);
            const valid = parsleyApi.validate();
            if (valid) {
                showLoading();
                processFormalizer().then(() => {
                    submitForm();
                    $saveBtn.prop('disabled', false);
                });
            }
        });
    }

    function updateHiddenFields() {
        _$el.find('[title="loginName"]').hide();
        if (options.hideTitle !== false) {
            _$el.find('[title^="Title"], [title^="Form ID"]')
                .val(_options.leadSource)
                .hide();
        }
        _$el.show();

        const groupInput = document.querySelector("[name='group']");
        if (groupInput) {
            getLang().then((lang) => {
                groupInput.value = lang;
            });
        }
    }

    function getEl() {
        return _$el;
    }

    updateSaveButtonScript();
    updateHiddenFields();
    menuLangEmitter.on(EVENT_LANG_CHANGED, updateHiddenFields);

    return {
        getEl,
        updateHiddenFields,
        showLoading,
        hideLoading,
    };
};

export default {
    createForm,
};
