import $ from 'jquery';
import formSerialize from 'form-serialize';
import { createFunctionWithTimeout } from '../createFunctionWithTimeout';

const createInstance = function createInstance(options) {
    window.llfrmid = options.id;
    window.jQuery = $;

    function submitFormalizer() {
        return new Promise((resolve, reject) => {
            const { pairs, $el } = options;

            const currentUrl = window.location.href;
            const referer = document.referrer || '';

            const fields = formSerialize($el.closest('form').get(0), {
                hash: true,
                empty: true,
                disabled: true,
            });
            const llformfullname = `${fields[pairs.firstName] || ''} ${
                fields[pairs.lastName || '']
            }`;

            const llformalyzerURL = encodeURIComponent(
                `${currentUrl}?LLM=${fields[pairs.email]
                    || ''}&LLN=${llformfullname}&LLPH=${fields[pairs.phone]
                    || ''}&LLCO=${fields[pairs.company] || ''}`,
            );

            const llfrmid = options.id || 1808011;
            const fp = Object.hasOwnProperty.call(window, 'llfp')
                ? window.llfp
                : 0;
            const referUrl = `https://tracking.leadlander.com/api/tracking?accountId=${llfrmid}&r=form&page=${llformalyzerURL}&fp=${fp}&referer=${referer}`;

            const iframeEl = document.createElement('iframe');
            iframeEl.setAttribute('width', '0');
            iframeEl.setAttribute('height', '0');
            iframeEl.setAttribute('frameborder', '0');
            iframeEl.setAttribute('id', 'FormalizerIframe');
            iframeEl.setAttribute('scrolling', 'no');
            iframeEl.setAttribute('tabindex', '-1');
            iframeEl.setAttribute('style', 'display: none;');
            iframeEl.setAttribute('src', referUrl);
            document.body.appendChild(iframeEl);
            iframeEl.addEventListener(
                'load',
                createFunctionWithTimeout(() => {
                    resolve(true);
                }, 1500),
            );
        });
    }

    return {
        submitFormalizer,
    };
};

export default {
    createInstance,
};
