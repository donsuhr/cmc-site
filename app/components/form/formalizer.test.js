import { describe, it } from 'mocha';
import { expect } from 'chai';
import $ from 'jquery';
import formalizer from './formalizer';

describe('formalizer', () => {
    it('renders', () => {
        document.body.innerHTML = `
            <form id="testForm">
                <input name="firstName" value="firstName" />
                <input name="lastName" value="lastName" />
                <input name="email" value="email" />
                <input name="phone" value="phone" />
                <input name="company" value="company" />
            </form>
        `;
    });

    it('adds an iframe', () => {
        const sut = formalizer.createInstance({
            id: 123,
            pairs: {
                email: 'email',
                firstName: 'firstName',
                lastName: 'lastName',
                phone: 'phone',
                company: 'company',
            },
            $el: $('#testForm'),
        });
        sut.submitFormalizer();
        expect(document.querySelector('iframe').src).to.match(/i=123&/);
        expect(document.querySelector('iframe').src).to.match(
            /LLN%3DfirstName%20lastName/,
        );
        expect(document.querySelector('iframe').src).to.match(/LLPH%3Dphone/);
        expect(document.querySelector('iframe').src).to.match(/LLCO%3Dcompany/);
        expect(document.querySelector('iframe').src).to.match(/LLM%3Demail/);
    });
});
