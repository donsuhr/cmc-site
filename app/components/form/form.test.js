import {
    beforeEach, afterEach, describe, it,
} from 'mocha';
import { expect } from 'chai';
import $ from 'jquery';
import nock from 'nock';
import sinon from 'sinon';
import config from '../../../config';
import form from './form';

describe('form', () => {
    beforeEach(() => {
        nock.disableNetConnect();

        nock(`${config.domains.api}/`)
            .post('/endpoint')
            .reply(200, '{"success": "success"}');

        document.body.innerHTML = `
            <form id="testForm">
                <input name="firstName" value="firstName" />
                <input name="lastName" value="lastName" />
                <input name="email" value="email" />
                <input name="phone" value="phone" />
                <input name="company" value="company" />
                <input type="submit" name="btnSave" />
                <input title="loginName" name="loginName" value="loginName" />
                <input title="Title" name="Title" value="Title" />
            </form>
        `;

        global.localStorage = {
            getItem(key) {
                return this[key];
            },
            setItem(key, value) {
                this[key] = value;
            },
            removeItem(key) {
                delete this[key];
            },
        };
    });

    afterEach(() => {
        nock.cleanAll();
    });

    it('throws when formDomID missing', () => {
        const options = {
            // formDomID: 'testForm',
            leadSource: 'leadSource',
            endpoint: 'endpoint',
            cb: () => {},
        };
        expect(form.createForm.bind(form, options)).to.throw(
            'CMC-form init error: missing formDomID',
        );
    });

    it('throws when leadSource missing', () => {
        const options = {
            formDomID: 'testForm',
            // leadSource: 'leadSource',
            endpoint: 'endpoint',
            successRedirectUrl: 'successRedirectUrl',
        };
        expect(form.createForm.bind(form, options)).to.throw(
            'CMC-form init error: missing leadSource',
        );
    });
    it('throws when endpoint missing', () => {
        const options = {
            formDomID: 'testForm',
            leadSource: 'leadSource',
            // endpoint: 'endpoint',
            successRedirectUrl: 'successRedirectUrl',
        };
        expect(form.createForm.bind(form, options)).to.throw(
            'CMC-form init error: missing endpoint',
        );
    });

    it('throws when successRedirectUrl or callback missing', () => {
        const options = {
            formDomID: 'testForm',
            leadSource: 'leadSource',
            endpoint: 'endpoint',
            successRedirectUrl: '',
        };
        expect(form.createForm.bind(form, options)).to.throw(
            'CMC-form init error: missing successRedirectUrl or a cb',
        );
    });

    it('shows a loading ', (done) => {
        form.createForm({
            formDomID: 'testForm',
            leadSource: 'leadSource',
            endpoint: 'endpoint',
            cb: () => {
                expect(document.querySelectorAll('.error').length).to.equal(0);
                done();
            },
        });
        $('[name=btnSave]').trigger('click');
        expect(document.querySelectorAll('.loading').length).to.equal(1);
    });

    it('shows an error on 500', (done) => {
        nock.cleanAll();
        nock(`${config.domains.api}/`)
            .post('/endpoint')
            .reply(500, '{error:"error"}');
        sinon.stub(console, 'log'); // hide console output
        form.createForm({
            formDomID: 'testForm',
            leadSource: 'leadSource',
            endpoint: 'endpoint',
            cb: () => {
                expect(document.querySelectorAll('.loading').length).to.equal(
                    0,
                );
                expect(document.querySelectorAll('.error').length).to.equal(1);
                // eslint-disable-next-line no-console
                console.log.restore();
                done();
            },
        });
        $('[name=btnSave]').trigger('click');
    });

    it('hides loginName and title', () => {
        form.createForm({
            formDomID: 'testForm',
            leadSource: 'leadSource',
            endpoint: 'endpoint',
            cb: () => {},
        });
        expect($('[title="loginName"]').css('display')).to.equal('none');
        expect($('[title="Title"]').css('display')).to.equal('none');
    });

    it('does not hide title if hideTitle is false', () => {
        document.body.innerHTML = `
            <form id="testForm">
                <input name="firstName" value="firstName" />
                <input name="lastName" value="lastName" />
                <input name="email" value="email" />
                <input name="phone" value="phone" />
                <input name="company" value="company" />
                <input type="submit" name="btnSave" />
                <input title="loginName" name="loginName" value="loginName" />
                <input title="Title" name="Title" value="Title" />
            </form>
        `;
        form.createForm({
            formDomID: 'testForm',
            leadSource: 'leadSource',
            endpoint: 'endpoint',
            cb: () => {},
            hideTitle: false,
        });
        expect($('[name="firstName"]').css('display')).to.equal('inline-block');
        expect($('[title="Title"]').css('display')).to.equal('inline-block');
    });

    it('can have a formalizer', (done) => {
        document.body.innerHTML = `
            <form id="testForm">
                <input name="firstName" value="firstName" />
                <input name="lastName" value="lastName" />
                <input name="email" value="email" />
                <input name="phone" value="phone" />
                <input name="company" value="company" />
                <input type="submit" name="btnSave" />
                <input title="loginName" name="loginName" value="loginName" />
                <input title="Title" name="Title" value="Title" />
            </form>
        `;
        form.createForm({
            formDomID: 'testForm',
            leadSource: 'leadSource',
            endpoint: 'endpoint',
            cb: () => {},
            formalizerOptions: {
                id: 123,
                pairs: {
                    // llname : existing element name
                    email: 'email',
                    firstName: 'firstName',
                    lastName: 'lastName',
                    phone: 'phone',
                },
            },
        });
        $('[name=btnSave]').trigger('click');
        setTimeout(() => {
            // wait for promise to call submitForm for nock to catch it
            expect(document.querySelector('iframe').src).to.match(/i=123&/);
            done();
        }, 10);
    });
});
