import {
    beforeEach, afterEach, describe, it,
} from 'mocha';
import { expect } from 'chai';
import nock from 'nock';
import $ from 'jquery';
import AppView from '../views/appView';
import Router from '../router';
import config from '../../../../config';
import productSheetData from './fixtures/product-sheets.json';

const router = new Router();

describe('s3-doc-list', () => {
    describe('appView', () => {
        beforeEach(() => {
            nock.disableNetConnect();
            nock(`${config.domains.api}/`)
                .get('/docType/site/group/')
                .replyWithFile(
                    200,
                    `${__dirname}/fixtures/product-sheets.json`,
                );

            document.body.innerHTML = `
                <section id="s3docListApp">
                    <div class="loading--placeholder">Loading...</div>
                </section>
            `;
        });

        afterEach(() => {
            nock.cleanAll();
        });

        it('renders', (done) => {
            // eslint-disable-next-line no-new
            new AppView({
                el: $('#s3docListApp'),
                router,
                docType: 'docType',
                site: 'site',
                group: 'group',
            });
            setTimeout(() => {
                expect(document.querySelector('.s3-doc-list')).not.to.a('null');
                done();
            }, 10);
        });

        it('works with preload data', (done) => {
            // eslint-disable-next-line no-new
            new AppView({
                el: $('#s3docListApp'),
                router,
                docType: 'docType',
                site: 'site',
                group: 'group',
                preloadData: productSheetData.data,
            });
            setTimeout(() => {
                expect(
                    document.querySelectorAll('.s3-doc-list li').length,
                ).to.equal(productSheetData.data.length);
                done();
            }, 20);
        });
    });
});
