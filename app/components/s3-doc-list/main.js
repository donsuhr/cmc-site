/* globals s3DocListPreloadData */
/* eslint-disable no-new */

import Backbone from 'backbone';
import AppView from './views/appView';
import Router from './router';

const api = {
    init(options) {
        const router = new Router();
        const preloadData = typeof s3DocListPreloadData !== 'undefined'
            ? s3DocListPreloadData
            : [];
        new AppView({
            el: options.$el,
            router,
            preloadData,
            docType: options.docType,
            site: options.site,
            group: options.group,
            template: options.template,
            itemTemplate: options.itemTemplate,
            featureItemTemplate: options.featureItemTemplate,
            itemsPerPage: options.itemsPerPage,
            firstPageItemsPerPage: options.firstPageItemsPerPage,
            filterMenuOptions: options.filterMenuOptions,
            model: options.model,
            type: options.type,
            ListCollectionClass: options.ListCollectionClass,
        });
        if (
            !Object.hasOwnProperty.call(options, 'history')
            || options.history === true
        ) {
            Backbone.history.start();
        }
    },
};

export default api;
