import Backbone from 'backbone';
import _ from 'underscore';
import config from 'config';
import Model from './s3ItemModel';
import { featured, notFeatured } from './feature-filter';

const ListCollection = Backbone.Collection.extend({
    url() {
        const { site, docType, group } = this.options;
        const inNavQuery = this.options.inNavLang
            ? `?in-nav=${this.options.inNavLang}`
            : '';
        return `${config.domains.api}/${docType}/${site}/${group}/${inNavQuery}`;
    },
    comparator(item) {
        return -item.get('priority');
    },
    initialize(models, options) {
        this.options = options;
        this.options.firstPageItemsPerPage = options.firstPageItemsPerPage || options.itemsPerPage;
        if (!Object.hasOwnProperty.call(options, 'model') || !options.model) {
            this.model = Model;
        }
    },
    setInNavLang(lang) {
        this.options.inNavLang = lang;
    },
    parse(response) {
        return response.data;
    },

    applyFilter(filter) {
        const clonedFilter = _.clone(filter.attributes);
        _.each(clonedFilter, (value, key) => {
            if (!value) {
                delete clonedFilter[key];
            }
        });

        // product, institution-types is a list of possible
        // options, filter separately
        let selectedProduct;
        let selectedType;
        if (clonedFilter.products) {
            selectedProduct = clonedFilter.products;
            delete clonedFilter.products;
        }
        if (clonedFilter['institution-types']) {
            selectedType = clonedFilter['institution-types'];
            delete clonedFilter['institution-types'];
        }
        let result = _.keys(clonedFilter).length
            ? this.where(clonedFilter)
            : this.models;
        if (selectedProduct) {
            result = _.filter(result, (value, index, collection) => _.contains(value.get('products'), selectedProduct));
        }
        if (selectedType) {
            result = _.filter(result, (value, index, collection) => _.contains(value.get('institution-types'), selectedType));
        }
        return new ListCollection(result, this.options);
    },

    applyPaging({ page }) {
        const {
            firstPageItemsPerPage,
            itemsPerPage,
            featureItemTemplate,
        } = this.options;
        const showFeatured = !!featureItemTemplate;
        const start = page === 1 ? 0 : firstPageItemsPerPage + itemsPerPage * (page - 2);
        const end = page === 1 ? firstPageItemsPerPage : start + itemsPerPage;

        const featuredItems = page === 1 && showFeatured ? featured(this.models) : null;
        const regularItems = showFeatured
            ? notFeatured(this.models)
            : this.models;

        const collection = new ListCollection(
            regularItems.slice(start, end),
            this.options,
        );
        collection.featured = new ListCollection(featuredItems, this.options);

        return collection;
    },

    getUniqueProducts() {
        return _.unique(
            _.filter(_.flatten(this.pluck('products'), (x) => x !== '')),
        ).sort();
    },

    getUniqueTypes() {
        return _.unique(
            _.filter(
                _.flatten(this.pluck('institution-types'), (x) => x !== ''),
            ),
        ).sort();
    },
});

export default ListCollection;
