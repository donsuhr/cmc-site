import _ from 'underscore';

const isFeatured = (value) => value.get('featured') === true;

function featured(models) {
    return _.filter(models, (value, index, collection) => isFeatured(value));
}

function notFeatured(models) {
    return _.filter(models, (value, index, collection) => !isFeatured(value));
}

export { featured, notFeatured };
