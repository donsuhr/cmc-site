import $ from 'jquery';
import Backbone from 'backbone';
import template from '../templates/pagination.hbs';
import { featured } from '../feature-filter';
import smoothScroll from '../../smoothScroll';

function isInViewport(elem) {
    const bounding = elem.getBoundingClientRect();
    return (
        bounding.top >= 0
        && bounding.left >= 0
        && bounding.bottom
            <= (window.innerHeight || document.documentElement.clientHeight)
        && bounding.right
            <= (window.innerWidth || document.documentElement.clientWidth)
    );
}

export default Backbone.View.extend({
    initialize(options) {
        this.currentPage = 1;
        this.options = options;
        this.options.firstPageItemsPerPage = options.firstPageItemsPerPage || options.itemsPerPage;
        // first load
        this.listenTo(
            this.model.get('filteredCollection'),
            'sync',
            this.onFilteredListChanged,
        );
        // filtered results
        this.listenTo(
            this.model,
            'change:filteredCollection',
            this.onFilteredListChanged,
        );
    },
    events: {
        'click button': 'onClick',
    },

    onClick(event) {
        event.preventDefault();
        const action = $(event.target).data('action');
        let render = false;
        if (action === 'next') {
            if (this.currentPage + 1 <= this.totalPages) {
                this.currentPage += 1;
                render = true;
            }
        } else if (action === 'prev') {
            if (this.currentPage - 1 >= 1) {
                this.currentPage -= 1;
                render = true;
            }
        } else {
            this.currentPage = parseInt(action, 10);
            render = true;
        }
        if (render) {
            this.render();
            if (!isInViewport(this.$el.get(0))) {
                smoothScroll.scrollTo($(this.$el.get(0)));
            }
            this.trigger('pagination:change', {
                page: this.currentPage,
                totalPages: this.totalPages,
            });
        }
    },

    onFilteredListChanged() {
        const {
            showFeatured,
            itemsPerPage,
            firstPageItemsPerPage,
        } = this.options;
        this.currentPage = 1;

        const featuredFiltered = showFeatured
            ? featured(this.model.get('filteredCollection').models)
            : [];
        const unfeaturedLength = this.model.get('filteredCollection').length
            - featuredFiltered.length;

        this.totalPages = Math.ceil(
            Math.max(0, unfeaturedLength - firstPageItemsPerPage)
                    / itemsPerPage,
        ) + 1;
        this.trigger('pagination:change', {
            page: this.currentPage,
            totalPages: this.totalPages,
        });
        this.render();
    },

    render(event) {
        const items = [];
        for (let i = 1; i <= this.totalPages; i += 1) {
            items.push({
                text: i,
                page: i,
                ariaLabel:
                    i === this.currentPage ? `${i}, current page` : false,
                active: i === this.currentPage,
            });
        }
        const content = template({
            prevEnabled: this.currentPage > 1,
            nextEnabled: this.currentPage < this.totalPages,
            totalPages: this.totalPages,
            currentPage: this.currentPage,
            items,
        });
        this.$el.html(content);
    },
});
