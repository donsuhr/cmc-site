import Backbone from 'backbone';
import template from '../templates/listItemView.hbs';

export default Backbone.View.extend({
    tagName: 'li',
    initialize(options) {
        this.template = options.template || template;
    },
    render() {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    },
});
