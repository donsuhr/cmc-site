import Backbone from 'backbone';
import ItemView from './itemView';

export default Backbone.View.extend({
    initialize(options) {
        // first load
        this.listenTo(
            this.model.get('pagedCollection'),
            'sync reset',
            this.render,
        );
        // set by filter update
        this.listenTo(this.model, 'change:pagedCollection', this.render);
        this.template = options.template;
        this.templateSupplied = !!options.template;
        this.featureItemTemplate = options.featureItemTemplate;
    },

    render(event) {
        const items = this.model.get('pagedCollection');
        if (this.templateSupplied) {
            let content = this.template(items.toJSON());
            if (items.featured && items.featured.length > 0) {
                const featuredItemsContent = this.featureItemTemplate(
                    items.featured.toJSON(),
                );
                content = featuredItemsContent + content;
            }
            this.$el.html(content);
        } else {
            this.$el.empty();
            items.each((item) => {
                const view = new ItemView({
                    model: item,
                    template: this.template,
                });
                this.$el.append(view.render().el);
            });
        }
        return this;
    },
});
