import Backbone from 'backbone';
import LoadingAnimation from '../../loading-view';
import SelectedFilterView from '../../list-view-common/selected-filter-view';
import template from '../templates/appView.hbs';
import ListCollection from '../listCollection';
import ListView from './listView';
import MenuView from './menuView';
import PaginationView from './paginationView';
import Filter from '../filter';
import {
    getLang,
    eventEmitter as menuLangEmitter,
    EVENT_LANG_CHANGED,
} from '../../menu/menu-lang-filter';

export default Backbone.View.extend({
    initialize(options) {
        this.router = options.router;
        this.itemsPerPage = options.itemsPerPage;
        this.loading = new LoadingAnimation({ $container: this.$el });
        this.listCollection = options.ListCollectionClass
            ? new options.ListCollectionClass(null, options)
            : new ListCollection(null, options);
        this.filter = new Filter();
        this.template = options.template || template;
        this.render();

        this.selectedFilterModel = new Backbone.Model({
            allItemsCollection: this.listCollection,
            filteredCollection: this.listCollection,
            pagedCollection: this.listCollection,
            filter: this.filter,
        });

        this.parterListView = new ListView({
            el: this.$el.find('.s3-doc-list'),
            model: this.selectedFilterModel,
            template: options.itemTemplate,
            featureItemTemplate: options.featureItemTemplate,
        });

        this.menuView = new MenuView({
            el: this.$el.find('.s3-doc-list__menu'),
            model: this.selectedFilterModel,
            filterMenuOptions: options.filterMenuOptions,
        });

        this.paginationView = new PaginationView({
            el: this.$el.find('.s3-doc-list__pagination'),
            model: this.selectedFilterModel,
            itemsPerPage: options.itemsPerPage,
            firstPageItemsPerPage: options.firstPageItemsPerPage,
            showFeatured: !!options.featureItemTemplate,
        });

        this.selectedFilterView = new SelectedFilterView({
            el: this.$el.find('.horizontal-menu__selected-filter-text'),
            model: this.selectedFilterModel,
            filterMenuOptions: options.filterMenuOptions,
        });

        this.listenTo(
            this.listCollection,
            'sync reset',
            this.onListCollectionSync,
        );
        this.listenTo(
            this.listCollection,
            'error',
            this.onListCollectionSyncError,
        );
        this.listenTo(this.listCollection, 'request', this.onSyncStart);
        this.listenTo(
            this.paginationView,
            'pagination:change',
            this.onPaginationChange,
        );

        this.listenTo(this.filter, 'change', this.filterListView);
        this.router.on('route:xfilter', this.filter.setState, this.filter);
        this.filter.on('change', this.router.updateFilter, this.router);

        if (
            typeof options.preloadData !== 'undefined'
            && options.preloadData.length
        ) {
            this.listCollection.reset(options.preloadData);
        } else {
            const boundOnMenuLangChange = this.onMenuLangChange.bind(this);
            getLang().then(boundOnMenuLangChange);
            menuLangEmitter.on(EVENT_LANG_CHANGED, boundOnMenuLangChange);
        }
    },
    render(event) {
        this.$el.html(this.template());
    },
    onListCollectionSync(event) {
        this.onSyncEnd();
        this.filterListView();
    },
    onMenuLangChange(lang) {
        if (this.currentMenuLang !== lang) {
            this.listCollection.setInNavLang(lang);
            this.listCollection.fetch();
            this.currentMenuLang = lang;
        }
    },
    filterListView() {
        const filtered = this.listCollection.applyFilter(this.filter);
        this.selectedFilterModel.set('filteredCollection', filtered);
    },
    onPaginationChange({ page }) {
        if (this.itemsPerPage) {
            const paged = this.selectedFilterModel
                .get('filteredCollection')
                .applyPaging({
                    page,
                });
            this.selectedFilterModel.set('pagedCollection', paged);
        } else {
            this.selectedFilterModel.set(
                'pagedCollection',
                this.selectedFilterModel.get('filteredCollection'),
            );
        }
    },
    onListCollectionSyncError(collection, resp, options) {
        // eslint-disable-next-line no-console
        console.log('Error:', resp, resp.stack);
        this.onSyncEnd();
    },
    onSyncStart() {
        this.loading.show();
    },
    onSyncEnd() {
        this.loading.hide();
    },
});
