/* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */

import Backbone from 'backbone';
import config from 'config';

export default Backbone.Model.extend({
    parse(response) {
        if (response['override-url']) {
            response.fullUrl = response['override-url'];
        } else {
            // /docs/:type/:site/:group/:file
            // https://cmcv3-:type--:site--:group.s3.amazonaws.com/:file   200
            const {
                docType,
                site,
                group,
                s3BucketPrefix,
            } = this.collection.options;
            response.fullUrl = process.env.NODE_ENV === 'production'
                ? `${
                    config.domains.self
                }/docs/${docType}/${site}/${group}/${response.s3url}`
                : `https://${s3BucketPrefix}${docType}--${site}--${group}.s3.amazonaws.com/${
                    response.s3url
                }`;

            if (docType === 'case-studies') {
                const langPrefix = group === 'en-us' ? '' : `${group}/`;
                // response.fullUrl = langPrefix === 'pr-br/' ?
                //     `${config.domains.self}/${langPrefix}solucoes/
                //          dl-case-study/${response._id}/${response.s3url}/`
                //     : `${config.domains.self}/${langPrefix}higher-education-resources/
                //          dl-case-study/${response._id}/${response.s3url}/`;
                response.fullUrl = langPrefix === 'pt-br/'
                    ? `/solucoes/dl-case-study/${response._id}/${
                        response.s3url
                    }/`
                    : `/${langPrefix}higher-education-resources/dl-case-study/${
                        response._id
                    }/${response.s3url}/`;
            }
        }
        return response;
    },
});
