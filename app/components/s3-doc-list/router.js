import Backbone from 'backbone';
import _ from 'underscore';
import $ from 'jquery';

export default Backbone.Router.extend({
    routes: {
        'filter(/products/:1)(/institution-types/:2)': 'filter',
    },
    filter(products, institutionType) {
        this.trigger('route:xfilter', {
            products,
            'institution-types': institutionType,
        });
    },
    updateFilter(filter) {
        let url = 'filter';
        _.map(filter.attributes, (value, key) => {
            if ($.trim(value) !== '') {
                url += `/${key}/${value}`;
            }
        });
        this.navigate(url, { trigger: false });
    },
});
