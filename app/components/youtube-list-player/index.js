import $ from 'jquery';

export default function init() {
    $('.youtube-player .youtube-player__button-list').on(
        'click',
        'button',
        (event) => {
            // eslint-disable-line prefer-arrow-callback
            const $button = $(event.target);
            const $list = $button.closest('.youtube-player__button-list');
            const $playerNode = $list.closest('.youtube-player');

            const newQuote = $button.find('.youtube-player__quote-src').text();
            const newAuthor = $button
                .find('.youtube-player__author-src')
                .html();
            const newSrc = $button
                .find('.youtube-player__video-url')
                .text()
                .trim();
            const quoteTarget = $playerNode.find('.youtube-player__quote');
            const authorTarget = $playerNode.find('.youtube-player__author');
            const playerTarget = $playerNode.find(
                '.youtube-player__video-iframe',
            );

            quoteTarget.text(newQuote);
            authorTarget.html(newAuthor);
            playerTarget.prop('src', newSrc);

            $list.find('button').removeClass('active');
            $button.addClass('active');
        },
    );
}
