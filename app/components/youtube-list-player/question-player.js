import $ from 'jquery';
import smoothScroll from '../smoothScroll';

export default function init() {
    $('.youtube-player__question-button-list').on(
        'click',
        '.youtube-player__question-button',
        (event) => {
            // eslint-disable-line prefer-arrow-callback
            const $button = $(event.target);
            const $playerNode = $('#dynamic-youtube-player');

            const newSrc = $button
                .find('.youtube-player__video-url')
                .text()
                .trim();
            const newTitle = $button
                .find('.youtube-player__video-title')
                .text()
                .trim();

            $playerNode.prop('src', newSrc);
            $playerNode.prop('title', newTitle);
            smoothScroll.scrollTo($playerNode, undefined, 26 * 4);
        },
    );
}
