import Backbone from 'backbone';
import _ from 'underscore';
import listItem from './listItem';

const ListCollection = Backbone.Collection.extend({
    model: listItem,
    url() {
        return 'https://hbapi.hirebridge.com/v2/CareerCenter/GetJobs?cid=5915';
    },
    comparator(item) {
        return -item.get('joblistid');
    },
    initialize(models, options) {
        this.options = options;
    },
    applyFilter(filter) {
        const clonedFilter = _.clone(filter.attributes);
        _.each(clonedFilter, (value, key) => {
            if (!value) {
                delete clonedFilter[key];
            }
        });

        // product, institution-types is a list of possible
        // options, filter separately
        let selectedProduct;
        let selectedType;
        if (clonedFilter.products) {
            selectedProduct = clonedFilter.products;
            delete clonedFilter.products;
        }
        if (clonedFilter['institution-types']) {
            selectedType = clonedFilter['institution-types'];
            delete clonedFilter['institution-types'];
        }
        let result = _.keys(clonedFilter).length
            ? this.where(clonedFilter)
            : this.models;
        if (selectedProduct) {
            result = _.filter(result, (value, index, collection) => _.contains(value.get('products'), selectedProduct));
        }
        if (selectedType) {
            result = _.filter(result, (value, index, collection) => _.contains(value.get('institution-types'), selectedType));
        }
        return new ListCollection(result, this.options);
    },

    getUniqueLocations() {
        return _.unique(
            _.filter(_.flatten(this.pluck('joblocname'), (x) => x !== '')),
        ).sort();
    },

    getUniqueCategory() {
        return _.unique(
            _.filter(_.flatten(this.pluck('jobcatname'), (x) => x !== '')),
        ).sort();
    },
});

export default ListCollection;
