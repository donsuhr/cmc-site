import Backbone from 'backbone';
import LoadingAnimation from '../../loading-view';
import SelectedFilterView from '../../list-view-common/selected-filter-view';
import template from '../templates/current-openings-app.html';
import ListCollection from '../listCollection';
import ListView from './listView';
import MenuView from './menuView';
import Filter from '../filter';

export default Backbone.View.extend({
    initialize(options) {
        this.router = options.router;
        this.render();
        this.loading = new LoadingAnimation({ $container: this.$el });
        this.listCollection = new ListCollection(null, options);
        this.filter = new Filter();

        this.selectedFilterModel = new Backbone.Model({
            allItemsCollection: this.listCollection,
            filteredCollection: this.listCollection,
            filter: this.filter,
        });

        this.parterListView = new ListView({
            el: this.$el.find('.s3-doc-list'),
            model: this.selectedFilterModel,
        });

        this.menuView = new MenuView({
            el: this.$el.find('.s3-doc-list__menu'),
            model: this.selectedFilterModel,
            filterMenuOptions: options.filterMenuOptions,
        });

        this.selectedFilterView = new SelectedFilterView({
            el: this.$el.find('.horizontal-menu__selected-filter-text'),
            model: this.selectedFilterModel,
            filterMenuOptions: options.filterMenuOptions,
        });

        this.listenTo(
            this.listCollection,
            'sync reset',
            this.onListCollectionSync,
        );
        this.listenTo(
            this.listCollection,
            'error',
            this.onListCollectionSyncError,
        );
        this.listenTo(this.listCollection, 'request', this.onSyncStart);
        this.listenTo(this.filter, 'change', this.filterListView);
        this.router.on('route:xfilter', this.filter.setState, this.filter);
        this.filter.on('change', this.router.updateFilter, this.router);

        if (
            typeof options.preloadData !== 'undefined'
            && options.preloadData.length
        ) {
            this.listCollection.reset(options.preloadData, { parse: true });
        } else {
            this.listCollection.fetch();
        }
    },
    render(event) {
        this.$el.html(template());
    },
    onListCollectionSync(event) {
        this.onSyncEnd();
        this.filterListView();
    },
    filterListView() {
        const filtered = this.listCollection.applyFilter(this.filter);
        this.selectedFilterModel.set('filteredCollection', filtered);
    },
    onListCollectionSyncError(collection, resp, options) {
        console.log('Error:', resp, resp.stack); // eslint-disable-line no-console
        this.onSyncEnd();
    },
    onSyncStart() {
        this.loading.show();
    },
    onSyncEnd() {
        this.loading.hide();
    },
});
