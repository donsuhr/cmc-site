import Backbone from 'backbone';
import _ from 'underscore';
import SubMenu from '../../menu/horizontalMenuItemView';

import 'superfish';

const removeMotion = window.matchMedia('(prefers-reduced-motion: reduce), (update: slow)').matches;
const speed = removeMotion ? 0 : 'fast';
const speedOut = removeMotion ? 0 : 'fast';

const superFishOptions = {
    cssArrows: false,
    speed,
    speedOut,
    animation: { height: 'show' },
    animationOut: { height: 'hide' },
};

export default Backbone.View.extend({
    submenus: {},
    initialize(options) {
        this.options = options;
        this.options.filterMenuOptions = this.options.filterMenuOptions || [
            {
                field: 'products',
                text: 'Select Product',
                itemFnName: 'getUniqueProducts',
            },
        ];
        this.submenus = {};

        this.listenTo(this.model.get('filter'), 'change', this.onFilterChange);
        this.listenTo(
            this.model.get('allItemsCollection'),
            'request',
            this.renderReset,
        );
        this.listenTo(
            this.model.get('allItemsCollection'),
            'sync reset',
            this.updateMenuItemData,
        );

        this.render();
    },

    events: {
        'horizontalMenuItem:change': 'onMenuChange',
    },

    renderReset() {
        this.$el.superfish('destroy');
        _.map(this.submenus, (value, key) => {
            value.setItems([]);
        });
    },

    updateMenuItemData() {
        const items = this.model.get('allItemsCollection');
        this.options.filterMenuOptions.forEach((x) => {
            this.submenus[x.field].setItems(items[x.itemFnName]());
        });
        this.$el.superfish('destroy').superfish(superFishOptions);
        this.onFilterChange();
    },

    render(event) {
        this.options.filterMenuOptions.forEach((x) => {
            this.submenus[x.field] = new SubMenu({
                items: [],
                property: x.field,
                unselectedText: x.text,
            });
            this.$el.append(this.submenus[x.field].el);
        });
    },

    onFilterChange(event) {
        const filter = this.model.get('filter');
        this.options.filterMenuOptions.forEach((x) => {
            this.submenus[x.field].setSelectedText(filter.get(x.field));
        });
    },

    onMenuChange(event, data) {
        this.model.get('filter').set(data.property, data.value);
    },
});
