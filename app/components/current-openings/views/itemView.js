import Backbone from 'backbone';
import template from '../templates/current-openings-item.hbs';

export default Backbone.View.extend({
    tagName: 'li',
    className: 'current-openings__item',
    initialize(options) {
        this.template = options.template || template;
    },
    render() {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    },
});
