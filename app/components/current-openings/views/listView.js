import Backbone from 'backbone';
import 'jquery-ui/ui/core';
import 'jquery-ui/ui/widget';
import 'jquery-ui/ui/widgets/accordion';
import smoothScroll from '../../smoothScroll';

import ItemView from './itemView';

export default Backbone.View.extend({
    initialize() {
        // first load
        this.listenTo(
            this.model.get('filteredCollection'),
            'sync reset',
            this.render,
        );
        // set by filter update
        this.listenTo(this.model, 'change:filteredCollection', this.render);
    },

    render(event) {
        this.$el.empty();
        const items = this.model.get('filteredCollection');
        items.each((item) => {
            const view = new ItemView({ model: item });
            this.$el.append(view.render().el);
        });
        const removeMotion = window.matchMedia(
            '(prefers-reduced-motion: reduce), (update: slow)',
        ).matches;

        if (this.$el.hasClass('ui-widget')) {
            this.$el.accordion('destroy');
        }
        this.$el.accordion({
            header: '> li .current-openings__item__header',
            animate: removeMotion ? 0 : 200,
            heightStyle: 'content',
            collapsible: true,
            active: true,
            activate: (activateEvent, ui) => {
                if (
                    Object.hasOwnProperty.call(ui.newHeader, 'length')
                    && ui.newHeader.length > 0
                ) {
                    smoothScroll.scrollTo(ui.newHeader);
                }
            },
        });

        return this;
    },
});
