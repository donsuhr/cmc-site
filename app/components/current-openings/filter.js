import Backbone from 'backbone';
import _ from 'underscore';
import $ from 'jquery';

export default Backbone.Model.extend({
    setState(options) {
        _.map(options, (value, key) => {
            this.set(key, $.trim(value));
        });
    },
});
