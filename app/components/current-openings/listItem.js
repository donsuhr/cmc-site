import Backbone from 'backbone';

export default Backbone.Model.extend({
    parse(response) {
        response.publicdate = new Date(response.publicdate);
        response.description = response.description.replace(
            /<p>&nbsp;<\/p>/g,
            '',
        );
        response.skills = response.skills.replace(/<p>&nbsp;<\/p>/g, '');
        return response;
    },
});
