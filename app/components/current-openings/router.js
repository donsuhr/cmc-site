import Backbone from 'backbone';
import _ from 'underscore';
import $ from 'jquery';

export default Backbone.Router.extend({
    routes: {
        'filter(/joblocname/:1)(/jobcatname/:2)': 'filter',
    },
    filter(joblocname, jobcatname) {
        this.trigger('route:xfilter', {
            joblocname,
            jobcatname,
        });
    },
    updateFilter(filter) {
        let url = 'filter';
        _.map(filter.attributes, (value, key) => {
            if ($.trim(value) !== '') {
                url += `/${key}/${value}`;
            }
        });
        this.navigate(url, { trigger: false });
    },
});
