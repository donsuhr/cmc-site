import { buildElContainer, buildBg, buildContainerTitle } from './utils';

function buildText1620() {
    const p = document.createElement('p');
    p.classList.add('ht--p--16-22', 'mb0');
    p.textContent = `We have a saying at Campus Management: “Every line of code we write
        hare your passion for transforming communities, nations, and lives. Our CampusNexus
        solutions built on Microsoft enable you to serve a...`;
    return p;
}

function buildText1() {
    const p = document.createElement('p');
    p.classList.add('mb0');
    p.textContent = `We have a saying at Campus Management: “Every line of code we write
        solutions built on Microsoft enable you to serve a...`;
    return p;
}

function buildVideo() {
    const div = document.createElement('div');
    div.classList.add('max-width-66--sm', 'block', 'c');
    div.innerHTML = `
                <div class="video-wrapper video-wrapper--16x9">
                    <iframe
                        frameborder="0"
                        allowfullscreen
                        title="Youtube Video..."
                        class="lazyload"
                        data-src="https://www.youtube.com/embed/wfgxIzOJKxA?rel=0"></iframe>
                </div>
            `;
    return div;
}

function buildQuote() {
    const bq = document.createElement('blockquote');
    bq.innerHTML = `
            <p class="ht--s--36 mb2--sm">
                “Financial aid automation is a way to be more timely without
                compromising accuracy."
            </p>
            <cite
                class="testimonial__author--v2 c-black ht--p--14-075 i"
            >
                <div class="testimonial__author__name--v2 b">
                    Kim Black,
                </div>
                <div class="testimonial__author__title--v2">
                    Quality Control Specialist,
                </div>
                <div class="testimonial__author__company--v2">
                    ECPI University
                </div>
            </cite>
        `;
    return bq;
}

function buildTextImgLeft() {
    const frag = document.createDocumentFragment();
    const text1 = buildText1();
    text1.classList.remove('mb0');
    text1.classList.add('mb2--sm');
    frag.appendChild(text1);
    const ul = document.createElement('ul');
    ul.innerHTML = `
                <li>
                    <img loading="lazy" class="intro-img"
                    src="/images/pages/higher-education-resources/case-studies/classroom-size-icon-50x50.png"
                    alt=""
                    inline-img-dims
                    >
                    <h3 class="ht--p--400 ht--p--18 has-intro-img mb0">
                        Classroom-Size Workflow
                    </h3>
                    <p class="mb2--sm has-intro-img">
                        A classroom-size workflow now helps in the course
                        classroom for their courses.
                    </p>
                </li>
                <li>
                    <img loading="lazy" class="intro-img"
                    src="/images/pages/higher-education-resources/case-studies/enrollment-mgmt-icon-50x50.png"
                    alt=""
                    inline-img-dims
                    >
                    <h3 class="ht--p--400 ht--p--18 has-intro-img mb0">
                        Enrollment Management Wizard Workflow
                    </h3>
                    <p class="mb0 has-intro-img">
                        mismatched and inaccurate data entry errors have been
                        eliminated by nearly 100 percent.
                    </p>
                 </li>
           `;
    frag.appendChild(ul);
    return frag;
}

function buildTestimonial() {
    const div = document.createElement('div');
    div.classList.add('testimonial');
    div.innerHTML = `
                <p class="testimonial__quote">
                    We always want to keep top talent in the Rio Grande Valley,
                    high-caliber students by recruiting here.
                </p>
                <div class="testimonial__author">
                    <div class="testimonial__author__name">
                        Dr. Maggie Hinojosa,
                    </div>
                    <div class="testimonial__author__title">
                        Vice President of Strategic Enrollment Management,
                    </div>
                    <div class="testimonial__author__company">
                        The University of Texas Rio Grande Valley
                    </div>
                </div>
            `;
    return div;
}

function build() {
    const ret = document.createElement('div');
    const bg = buildBg();

    const container = buildElContainer();
    const isAddTitle = document.getElementById('FullWidthAddIntroTitle')
        .checked;
    if (isAddTitle) {
        container.appendChild(buildContainerTitle());
    }

    const contentMap = {
        text1620: buildText1620,
        text1: buildText1,
        quote: buildQuote,
        textImgLeft: buildTextImgLeft,
        testimonial: buildTestimonial,
        video: buildVideo,
    };

    const selectEl = document.getElementById('FullWidthContent');
    const selectVal = selectEl.options[selectEl.selectedIndex].value;
    container.appendChild(contentMap[selectVal]());

    bg.appendChild(container);
    ret.appendChild(bg);
    return ret;
}

export { build };
