import {
    buildBg,
    getFieldset,
    buildElContainer,
    buildContainerTitle,
    getColorBoxesChecked,
    getColorBoxValue,
} from './utils';

import { build as buildLR } from './content/left-right-div';
import { build as buildAutoDiv } from './content/auto-div';

function build() {
    const fieldset = getFieldset();
    const ret = document.createElement('div');
    const bg = buildBg();
    const isBgReversed = fieldset.querySelector('#BoxV4ContainerBgReverse')
        .checked;
    if (isBgReversed) {
        bg.classList.add('reverse');
    }

    const container = buildElContainer();
    const box = document.createElement('div');

    box.classList.add('box-v4');
    const typeSelect = fieldset.querySelector('#BoxV4Type');
    const typeValue = typeSelect.options[typeSelect.selectedIndex].value;
    const className = `box-v4--${typeValue}`;
    box.classList.add(className);

    const isAutoContent = typeValue.includes('flex') || typeValue.includes('auto');
    document.querySelectorAll('.l-r-option').forEach((x) => {
        x.hidden = isAutoContent;
    });
    document.querySelectorAll('.auto-content-option').forEach((x) => {
        x.hidden = !isAutoContent;
    });
    const colorBoxesValue = getColorBoxValue();
    document.querySelectorAll('.auto-color-option').forEach((x) => {
        x.hidden = colorBoxesValue !== 'auto';
    });
    if (colorBoxesValue === 'auto') {
        const autoColorEl = fieldset.querySelector('#AutoColorBoxes');
        const autoColorVal = autoColorEl.options[autoColorEl.selectedIndex].value;
        box.classList.add(`box-v4--${autoColorVal}s`);
    }

    // ----

    if (!isAutoContent) {
        const left = buildLR('left', container);
        const right = buildLR('right', container);

        if (document.getElementById('SwapLR').checked) {
            box.appendChild(right);
            box.appendChild(left);
        } else {
            box.appendChild(left);
            box.appendChild(right);
        }
    } else {
        const ii = document.getElementById('AutoContentCount').value;
        for (let i = 0; i < ii; i += 1) {
            box.appendChild(buildAutoDiv());
        }
    }

    // ----
    const isAddTitle = document.getElementById('AddIntroTitle').checked;
    if (isAddTitle) {
        container.prepend(buildContainerTitle());
    }
    // ----
    const isColorBoxes = getColorBoxesChecked();
    fieldset.querySelectorAll('.item-color-select').forEach((x) => {
        x.hidden = !isColorBoxes;
    });
    container.appendChild(box);
    bg.appendChild(container);
    ret.appendChild(bg);
    return ret;
}

export { build };
