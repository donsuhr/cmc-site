function build(hStart = 2) {
    const ret = document.createDocumentFragment();
    const h2 = document.createElement(`h${hStart}`);
    h2.classList.add('b', 'mb0-5');
    h2.textContent = 'Client Success Story';
    const p0 = document.createElement('p');
    p0.classList.add('ht--s--22-28', 'c-theme-primary-dark');
    p0.textContent = 'Making a Difference in the Life of Every Student';
    const p1 = document.createElement('p');
    p1.textContent = 'Watch this video to hear how Lancaster Bible College uses CampusNexus Occupation Insight to make a difference in the life of every student.';
    const p2 = document.createElement('p');
    p2.textContent = 'Utilize CampusNexus to prepare students for the workforce while collaborating with employers and the community.';
    ret.appendChild(h2);
    ret.appendChild(p0);
    ret.appendChild(p1);
    ret.appendChild(p2);
    return ret;
}
export { build };
