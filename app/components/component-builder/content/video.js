function build() {
    const ret = document.createElement('div');
    ret.classList.add('video-wrapper', 'video-wrapper--16x9');
    const iframe = document.createElement('iframe');
    iframe.classList.add('lazyload');
    iframe.setAttribute('data-src', 'https://www.youtube.com');
    iframe.setAttribute('frameborder', '0');
    iframe.setAttribute('allowfullscreen', '');
    iframe.setAttribute('title', 'youtube video - ...');
    ret.appendChild(iframe);
    return ret;
}

export { build };
