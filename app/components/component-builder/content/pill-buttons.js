function build(count) {
    const ul = document.createElement('ul');
    ul.classList.add('pill-list');
    const isCentered = document.getElementById('pillButtonCenter').checked;
    if (isCentered) {
        ul.classList.add('pill-list--centered');
    }
    if (count > 1) {
        ul.classList.add('pill-list--multiple');
    }
    for (let i = 0; i < count; i += 1) {
        const li = document.createElement('li');
        li.classList.add('pill-list__item');

        const a = document.createElement('a');
        a.classList.add('pill-button');
        a.textContent = 'Learn More';
        a.href = 'https://www.google.com';
        a.target = '_blank';
        a.rel = 'noopener';
        li.appendChild(a);
        ul.appendChild(li);
    }

    return ul;
}

export { build };
