function build() {
    const div = document.createElement('div');
    div.classList.add('c', 'flexcol', 'min-height-7');
    div.innerHTML = `
                <h3 class="ht--s--22-28 c-theme-primary-dark ">
                    Management Team
                </h3>
                <a class="pill-button c block bottom" href="/about-us/management-team/">View</a>
            `;
    return div;
}

export { build };
