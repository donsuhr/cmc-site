function build(fill = false, size = 'md') {
    const img = document.createElement('img');
    img.setAttribute('alt', '');
    img.setAttribute('loading', 'lazy');
    img.setAttribute('inline-img-dims', '');
    if (fill) {
        img.src = '/images/pages/higher-education-resources/box-LBC-case-study.jpg';
        img.classList.add('img-cover');
    } else {
        if (size === 'md') {
            img.src = '/images/pages/news-and-events/events/engagement-crm-system-of-intelligence.png';
        } else {
            img.src = '/images/pages/products/education-partners/verification-icon.png';
        }
        img.classList.add('max-width-100');
    }

    return img;
}
export { build };
