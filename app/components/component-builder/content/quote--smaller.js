function build() {
    const citeHtml = `<div class="testimonial__author__name--v2 b">Chris Wodka,</div>
                <div class="testimonial__author__title--v2">
                    Chief Financial Officer
                </div>
                <div class="testimonial__author__company--v2">
                    Central Arizona College
                </div>`;
    const blockquote = document.createElement('blockquote');
    const bubble = document.createElement('div');
    bubble.classList.add('testimonial', 'smaller');
    const quote = document.createElement('p');
    quote.textContent = 'Our shared commitment to supporting Oregon’s economy by preparing the workforce for current and future job markets, not only brought our consortium together, but allowed Oregon Co...';
    quote.classList.add(
        'c-black',
        'ht--p--18',
        'testimonial__quote--smaller',
    );
    bubble.appendChild(quote);
    const footer = document.createElement('footer');
    const cite = document.createElement('cite');
    cite.classList.add(
        'testimonial__author--v2',
        'c-black',
        'ht--p--14-075',
        'i',
    );
    cite.innerHTML = citeHtml;
    footer.appendChild(cite);
    blockquote.appendChild(bubble);
    blockquote.appendChild(footer);
    return blockquote;
}

export { build };
