import { build as buildShortText } from './text-short';

let internalCounter = 0;

function buildImg() {
    const div = document.createElement('div');
    div.classList.add('c');
    div.innerHTML = `
                    <img loading="lazy" class="c"
                         src="/images/pages/about-us/careers/careers-serve-edu-icon.png"
                         alt=""
                         inline-img-dims >
                    <p>
                        <span class="ht--s--30-46 block">31 Years</span>
                        <span class="ht--p--18 block">Serving Higher Education</span>
                    </p>
                `;
    return div;
}

function buildGreyBoxLonger() {
    const div = document.createElement('div');
    div.classList.add('flexcol');
    div.innerHTML = `
                <h3 class="reverse-spans">
                    <span class="ht--s--22-28 c-theme-primary-dark">
                        Success Profiles from 5 Award-Winning Institutions
                    </span>
                    <span class="block ht--p--16-20">
                        Success Stories Download
                    </span>
                </h3>
                <a
                    class="pill-button block c bottom"
                    href="/docs/case-studies/cmc/"
                    target="_blank"
                    >Download
                </a>
            </div>
            `;
    return div;
}

function buildCircle() {
    const div = document.createElement('div');
    div.classList.add('circle-bg');
    if (internalCounter % 2 === 0) {
        div.innerHTML = `
                    <p class="c i">
                        <span class="ht--s--50 c-grey--800">4x</span>
                        <span class="ht--p--22-i b c-black"
                            >Enrollment Increase Across Campuses</span
                        >
                    </p>
            `;
    } else {
        div.innerHTML = `
                <p class="c">
                    <span class="ht--s--20 b c-theme-primary-dark"
                        >Transmit</span
                    >
                    <span class="c-theme-tertiary ht--s--70 i mb0-5"
                        >121,919</span
                    >
                    <span class="ht--p--14-075 b c-grey--800"
                        >Student Records Without<br />Any Failure</span
                    >
                </p>
            `;
    }
    internalCounter += 1;
    return div;
}

function buildSmallImgLeft() {
    const div = document.createElement('div');
    if (internalCounter % 2 === 0) {
        div.innerHTML = `
        <!-- consider using ul/li -->
        <img loading="lazy"
            class="intro-img"
            src="/images/pages/products/education-partners/automated-eligibility-icon-sm.png"
            alt=""
            inline-img-dims>
        <h3 class="ht--p--400 ht--p--18 has-intro-img mb0 c-grey--800">
            Eligibility
        </h3>
        <p class="has-intro-img">
            Assign rules to any fund source based on over 20 Title
            IV configurable rule options.
        </p>
        `;
    } else {
        div.innerHTML = `
        <img
            loading="lazy"
            class="intro-img"
            src="/images/pages/products/education-partners/automated-disbursement-icon-sm.png"
            alt=""
            inline-img-dims>
        <h3 class="ht--p--400 ht--p--18 has-intro-img mb0 c-grey--800">
            Disbursement Payment Posting
        </h3>
        <p class="has-intro-img">
            Pay all scholarships, grants, institutional aid/state
            aid and COD fund sources automatically.
        </p>
        `;
    }
    internalCounter += 1;
    return div;
}

function build() {
    const contentMap = {
        img: buildImg,
        greyBox: buildShortText,
        circle: buildCircle,
        greyBoxLonger: buildGreyBoxLonger,
        smallImgLeft: buildSmallImgLeft,
    };

    const selectEl = document.getElementById('AutoContent');
    const selectVal = selectEl.options[selectEl.selectedIndex].value;

    return contentMap[selectVal]();
}

export { build };
