function build(hStart = 2) {
    const ret = document.createDocumentFragment();
    const category = document.createElement(`h${hStart}`);
    category.classList.add('ht--p--16-20', 'mb0');
    category.textContent = 'NEW GARTNER REPORT - April 2020';
    ret.appendChild(category);
    const h2 = document.createElement(`h${hStart}`);
    h2.classList.add('ht--s--28', 'c-theme-primary-dark');
    h2.textContent = 'Lorem ipsum dolor sit amet';
    const p0 = document.createElement('p');
    p0.classList.add('ht--s--22');
    p0.textContent = 'By 2030, 60 percent of the population age 25-34 will have a post-secondary credential.';

    const p1 = document.createElement('p');
    p1.textContent = 'Consectetur adipiscing elit. Pellentesque non augue vel nibh condimentum gravida. ';
    ret.appendChild(h2);
    ret.appendChild(p0);
    ret.appendChild(p1);
    return ret;
}

export { build };
