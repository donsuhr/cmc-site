import { getFieldset, getColorBoxValue, setColorBoxValue } from '../utils';
import { build as buildImg } from './img';
import { build as buildText1 } from './text1';
import { build as buildText2 } from './text2';
import { build as buildShortText } from './text-short';
import { build as buildVideo } from './video';
import { build as buildQuoteSmaller } from './quote--smaller';
import { build as buildPillButtons } from './pill-buttons';

let userColorBoxValue;
const colorBoxEl = document.getElementById('ColorBoxes');
colorBoxEl.addEventListener('change', () => {
    userColorBoxValue = getColorBoxValue();
});

function addPills(div) {
    const addPillButtons = parseInt(
        document.querySelector('[name=pillButtons]:checked').value,
        10,
    );
    if (addPillButtons) {
        div.appendChild(buildPillButtons(addPillButtons));
    }
}

function showHideAddPills(selectVal) {
    const fieldset = getFieldset();
    const liEl = fieldset.querySelectorAll('.pills-options');
    const hasOptions = ['text1', 'text2'];
    liEl.forEach((x) => {
        x.hidden = !hasOptions.includes(selectVal);
    });
}

function checkColorFillBox() {
    const leftSelectEl = document.getElementById('LeftContent');
    const leftSelectVal = leftSelectEl.options[leftSelectEl.selectedIndex].value;

    const rightSelectEl = document.getElementById('RightContent');
    const rightSelectVal = rightSelectEl.options[rightSelectEl.selectedIndex].value;

    if (leftSelectVal === 'imgFill' || rightSelectVal === 'imgFill') {
        colorBoxEl.disabled = true;
        setColorBoxValue('manual');
    } else {
        colorBoxEl.disabled = false;
        setColorBoxValue(userColorBoxValue);
    }
}

function build(lr = 'left', container) {
    userColorBoxValue = userColorBoxValue ?? getColorBoxValue();
    const lrUcFirst = `${lr.charAt(0).toUpperCase()}${lr.slice(1)}`;
    const isAddTitle = document.getElementById('AddIntroTitle').checked;
    const hStart = isAddTitle ? 3 : 2;
    const contentMap = {
        'img-md': () => buildImg(false, 'md'),
        'img-sm': () => buildImg(false, 'sm'),
        imgFill: () => buildImg(true),
        text1: () => buildText1(hStart),
        text2: () => buildText2(hStart),
        'text-short': buildShortText,
        'quote--smaller': buildQuoteSmaller,
        video: buildVideo,
    };

    let div = document.createElement('div');
    const selectId = `${lrUcFirst}Content`;
    const selectEl = document.getElementById(selectId);
    const selectVal = selectEl.options[selectEl.selectedIndex].value;
    const colorId = `BoxV4Container${lrUcFirst}ItemColor`;
    const colorEl = document.getElementById(colorId);
    if (selectVal === 'imgFill') {
        div = contentMap[selectVal]();
        container.classList.add('container--color-boxes');
    } else {
        div.appendChild(contentMap[selectVal]());
    }
    div.classList.add(lr);
    checkColorFillBox();
    const isColorBoxesManual = getColorBoxValue() === 'manual';
    colorEl.disabled = selectVal === 'imgFill' || !isColorBoxesManual;
    if (isColorBoxesManual && selectVal !== 'imgFill') {
        const colorVal = colorEl.options[colorEl.selectedIndex].value;
        if (colorVal !== 'none') {
            div.classList.add(colorVal);
        }
    }
    if (lr === 'left') {
        showHideAddPills(selectVal);
        addPills(div);
    }

    return div;
}

export { build };
