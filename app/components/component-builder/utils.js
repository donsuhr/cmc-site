function getBoxType() {
    const boxTypeEl = document.querySelector('#BoxType');
    const type = boxTypeEl.options[boxTypeEl.selectedIndex].value;
    return type;
}

function getFieldset() {
    const fieldSet = document.querySelector(`.fieldset--${getBoxType()}`);
    return fieldSet;
}

function getSelectedContainerHeight() {
    const fieldSet = getFieldset();
    const heightSelectEl = fieldSet.querySelector('.container-height');
    if (heightSelectEl) {
        return heightSelectEl.options[heightSelectEl.selectedIndex].value;
    }
    return false;
}

function getColorBoxValue() {
    const fieldSet = getFieldset();
    const colorBoxesEl = fieldSet.querySelector('#ColorBoxes');
    const index = colorBoxesEl?.selectedIndex;
    const colorBoxesVal = colorBoxesEl?.options[index]?.value ?? false;
    return colorBoxesVal;
}

function getColorBoxesChecked() {
    const value = getColorBoxValue();
    return value && value !== 'no';
}

function setColorBoxValue(value) {
    const fieldSet = getFieldset();
    const colorBoxesEl = fieldSet.querySelector('#ColorBoxes');
    const currentVal = getColorBoxValue();
    if (colorBoxesEl && currentVal !== value) {
        colorBoxesEl.value = value;
        const event = new Event('change');
        colorBoxesEl.dispatchEvent(event);
    }
}

function buildElContainer(size) {
    const fieldSet = getFieldset();
    const container = document.createElement('div');
    container.classList.add('container');
    if (getColorBoxesChecked()) {
        container.classList.add('container--color-boxes');
    }

    const selectedHeight = getSelectedContainerHeight();

    if (size || selectedHeight) {
        const heightValue = size || selectedHeight;
        if (heightValue !== '1') {
            container.classList.add(`container--${heightValue}`);
        }
    }

    const topHeavyEl = fieldSet.querySelector('.checkbox--top-heavy');
    if (topHeavyEl) {
        const isTopHeavy = topHeavyEl.checked;
        if (isTopHeavy) {
            container.classList.add('container--header--top-heavy');
        }
    }
    return container;
}

function getSelectedBgColor() {
    const fieldset = getFieldset();
    const selectBgColorEl = fieldset.querySelector('.bg-color-select');
    return selectBgColorEl.options[selectBgColorEl.selectedIndex].value;
}

function buildContainerTitle() {
    const h2 = document.createElement('h2');
    h2.textContent = 'Texas Service Center – Helping Community Colleges Meet 60x30TX Goals';
    h2.classList.add('c', 'ht--s--28');
    if (getSelectedBgColor() === 'bgc-theme-tertiary') {
        h2.classList.add('c-white');
    } else {
        h2.classList.add('c-theme-primary-dark');
    }
    const selectedHeight = getSelectedContainerHeight();
    if (getColorBoxesChecked()) {
        // for search: pt1 pt2--sm pt2-5--sm
        h2.classList.add(`pt${selectedHeight}--sm`);
    }
    if (selectedHeight !== '1') {
        // for search: mb2--sm mb2-5--sm
        h2.classList.add(`mb${selectedHeight}--sm`);
    }
    return h2;
}

function buildBg() {
    const bg = document.createElement('div');
    bg.classList.add(getSelectedBgColor());
    if (getSelectedBgColor() === 'bgc-theme-tertiary') {
        bg.classList.add('c-white');
    }
    if (getSelectedBgColor().startsWith('bgc-grey')) {
        bg.classList.add('c-grey--1000');
    }
    return bg;
}

export {
    getBoxType,
    getFieldset,
    buildElContainer,
    buildContainerTitle,
    buildBg,
    getSelectedBgColor,
    getColorBoxesChecked,
    getColorBoxValue,
    setColorBoxValue,
    getSelectedContainerHeight,
};
