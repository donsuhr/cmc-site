import { getFieldset, buildElContainer } from './utils';

function build() {
    const fieldset = getFieldset();
    const addImgEl = fieldset.querySelector('#HeaderAddImg');
    const hasAddImg = addImgEl.checked;

    const ret = document.createElement('div');
    ret.classList.add('output-container--header');

    const addSocialMediaEl = fieldset.querySelector('#HeaderAddSocial');
    const addSocialMedia = addSocialMediaEl.checked;
    if (addSocialMedia) {
        const socialShareButtons = document.createElement('div');
        socialShareButtons.classList.add(
            'output-container__social-share',
            'remove',
        );
        ret.appendChild(socialShareButtons);
    }

    const container = buildElContainer();
    container.classList.add('container--header');

    const header = document.createElement('header');
    header.classList.add('c', 'c-white');

    if (hasAddImg) {
        const img = document.createElement('img');
        img.classList.add('block', 'c', 'mb1');
        img.setAttribute('alt', '');
        img.setAttribute(
            'src',
            '/images/pages/demos/NMICC/NMICC-logo-circle.png',
        );
        container.classList.add('container--header-with-img');
        header.appendChild(img);
    }

    const h1 = document.createElement('h1');
    h1.classList.add('ht--s--38-45');
    h1.textContent = 'Lorem ipsum dolor sit amet';
    header.appendChild(h1);
    const p1 = document.createElement('p');
    p1.classList.add('ht--p--16-22', 'mb0');
    p1.textContent = 'Consectetur adipiscing elit. Pellentesque non augue vel nibh condimentum gravida. ';
    header.appendChild(p1);
    container.appendChild(header);
    ret.appendChild(container);
    return ret;
}

export { build };
