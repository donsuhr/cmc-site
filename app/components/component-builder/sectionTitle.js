import { buildElContainer } from './utils';

function build() {
    const ret = document.createElement('div');
    ret.classList.add('output-container--header');
    const container = buildElContainer(2);
    const h2 = document.createElement('h2');
    h2.classList.add('section-label', 'ht--s--30-46');
    h2.textContent = 'On-Demand Webinars';
    container.appendChild(h2);
    ret.appendChild(container);

    return ret;
}
export { build };
