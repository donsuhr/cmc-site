import Backbone from 'backbone';
import _ from 'underscore';
import $ from 'jquery';

const template = _.template(
    `<% _.each(data.items, function(i) { %>
    <button class="selected-filter__remove-btn" data-prop="<%= i.prop %>"><%= i.text %></button>
    <% }); %>`,
    { variable: 'data' },
);

export default Backbone.View.extend({
    initialize(options) {
        this.options = options;
        this.options.filterMenuOptions = this.options.filterMenuOptions || [
            {
                field: 'products',
            },
        ];
        this.listenTo(this.model.get('filter'), 'change', this.render);
        // first load
        this.listenTo(
            this.model.get('filteredCollection'),
            'sync',
            this.render,
        );
        // fitered results
        this.listenTo(this.model, 'change:filteredCollection', this.render);
        this.render();
    },

    events: {
        'click .selected-filter__remove-btn': 'onRemoveClicked',
    },

    render(event) {
        let html = '';
        const items = [];
        const filter = this.model.get('filter');
        const filteredCollection = this.model.get('filteredCollection');

        _.each(this.options.filterMenuOptions, (item) => {
            if (filter.has(item.field) && filter.get(item.field) !== '') {
                items.push({
                    text: filter.get(item.field),
                    prop: item.field,
                });
            }
        });
        if (items.length > 0) {
            html = template({ items });
        } else {
            html = 'All Items';
        }
        html += ` (${filteredCollection.length} items)`;
        this.$el.html(html);
    },

    onRemoveClicked(event) {
        event.preventDefault();
        const $target = $(event.target);
        const prop = $target.data('prop');
        $('main').trigger('focus');
        this.model.get('filter').set(prop, '');
    },
});
