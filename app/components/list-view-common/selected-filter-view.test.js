import { beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import _ from 'underscore';
import SelectedFilterView from './selected-filter-view';

let sut;

let currentFilter;

const items = [1, 2, 3].map((x) => ({
    toJSON() {
        return {
            logo: { url: `logo-url ${x}`, altText: `altText ${x}` },
            Title: `title ${x}`,
            Product: ['one', 'two'],
        };
    },
}));

const selectedFilterModel = {
    get(prop) {
        const props = {
            filteredCollection: {
                each: _.partial(_.each, items),
                length: items.length,
                on() {},
            },
            filter: {
                has() {
                    return true;
                },
                get(field) {
                    return currentFilter[field];
                },
                set(setProp, val) {
                    currentFilter[setProp] = val;
                },
                on() {},
            },
        };
        return props[prop];
    },
    on() {},
};

describe('list-view-common selected filter view', () => {
    beforeEach(() => {
        currentFilter = {
            Product: 'one',
            Category: '',
            Partner_x0020_Type: '',
        };
        document.body.innerHTML = `
            <div class="container"></div>
        `;
        sut = new SelectedFilterView({
            el: document.querySelector('.container'),
            model: selectedFilterModel,
            filterMenuOptions: [
                { field: 'Product' },
                { field: 'Category' },
                { field: 'Partner_x0020_Type' },
            ],
        });
    });

    it('renders', () => {
        expect(sut.$el.find('button').length).to.equal(1);
        currentFilter.Category = 'foo';
        sut.render();
        expect(sut.$el.find('button').length).to.equal(2);
        expect(sut.$el.text()).to.match(/\(3 items\)/);
    });

    it('clicks off items', () => {
        expect(sut.$el.find('button').length).to.equal(1);
        sut.$el
            .find('.selected-filter__remove-btn')
            .eq(0)
            .trigger('click');
        sut.render();
        expect(sut.$el.find('button').length).to.equal(0);
    });
});
