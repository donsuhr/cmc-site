import $ from 'jquery';
//import headroom from './menu/monkey-patch-headroom';

const removeMotion = window.matchMedia('(prefers-reduced-motion: reduce), (update: slow)').matches;
const animationSpeed = removeMotion ? 0 : 'slow';

export default {
    init() {
        $(() => {
            $('.smooth-scroll-link').on('click', (event) => {
                event.preventDefault();
                const $link = $(event.currentTarget);
                try {
                    const anchorName = $link.attr('href').split('#')[1];
                    const targetById = $(`[id=${anchorName}]`);
                    const target = targetById.length > 0
                        ? targetById
                        : $(`[name=${anchorName}]`);
                    this.scrollTo(target, anchorName);
                } catch (e) {
                    console.log('error:', e); // eslint-disable-line no-console
                }
            });
        });
    },
    scrollTo($target, anchorName, offset) {
        anchorName = typeof anchorName !== 'undefined' ? anchorName : $target.attr('id');
        const lineHeight = (() => parseInt($(document.body).css('lineHeight'), 10))();
        const totalOffset = offset || lineHeight;
        const destScrollTop = $target.offset().top - totalOffset;
        $('body').addClass('js-smooth-scrolling');
        headroom.unpin();
        $('html,body').animate({ scrollTop: destScrollTop }, animationSpeed, () => {
            $target.trigger('focus');
            setTimeout(() => {
                // wait for headroom debounce
                $('body').removeClass('js-smooth-scrolling');
            }, 110);
            if (window.history.pushState && anchorName) {
                window.history.pushState(null, null, `#${anchorName}`);
            }
        });
    },
};
