import $ from 'jquery';

function openShareWindow(type) {
    const social = {
        twitter: {
            url: 'https://twitter.com/intent/tweet?url={{url}}',
            options: 'toolbar=0, status=0, width=450, height=253',
        },
        linkedin: {
            url:
                'https://www.linkedin.com/cws/share?token=&isFramed=true&url={{url}}',
            options: 'toolbar=no,width=550,height=390',
        },
        facebook: {
            url: 'http://www.facebook.com/sharer/sharer.php?u={{url}}',
            options: 'toolbar=0, status=0, width=450, height=263',
        },
    };
    const url = social[type].url.replace(/\{\{url}}/, document.location.href);
    window.open(url, type, social[type].options);
}

function initShareButtons() {
    $('button.social-media-share__button').each((index, element) => {
        const $btn = $(element);
        const [, vendor] = /social-media-share__button--(\w+)/.exec(
            $btn.attr('class'),
        );
        $btn.on('click', (event) => {
            event.preventDefault();
            openShareWindow(vendor);
        });
    });
}

export default {
    init() {
        initShareButtons();
    },
};
