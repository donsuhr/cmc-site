const { describe, it, afterEach } = require('mocha');
const { expect } = require('chai');
const path = require('path');
const fs = require('fs');
const $ = require('jquery');
const sinon = require('sinon');

const template = fs.readFileSync(
    path.resolve(
        './app/metalsmith/partials/social-media-share-plugins-list.hbs',
    ),
    'utf-8',
);
const sut = require('./index').default;

const sandbox = sinon.createSandbox();

describe('social-media-share-buttons', () => {
    afterEach(() => {
        sandbox.restore();
        $(document.body)
            .find('*')
            .off();
    });

    it('should open a facebook share window', () => {
        document.body.innerHTML = template;
        const stub = sandbox.stub(window, 'open');
        sut.init();
        const clickEvent = new window.Event('click');
        document
            .querySelector('.social-media-share__button--facebook')
            .dispatchEvent(clickEvent);

        sinon.assert.calledOnce(stub);
        expect(stub.getCall(0).args[0]).to.match(/www\.facebook\.com/);
        expect(stub.getCall(0).args[1]).to.equal('facebook');
    });

    it('should open a linkedin share window', () => {
        document.body.innerHTML = template;
        const stub = sandbox.stub(window, 'open');
        sut.init();
        const clickEvent = new window.Event('click');
        document
            .querySelector('.social-media-share__button--linkedin')
            .dispatchEvent(clickEvent);

        sinon.assert.calledOnce(stub);
        expect(stub.getCall(0).args[0]).to.match(/www\.linkedin\.com/);
        expect(stub.getCall(0).args[1]).to.equal('linkedin');
    });
});
