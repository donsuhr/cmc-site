/* global dataLayer */

import $ from 'jquery';
import 'bootstrap-sass/assets/javascripts/bootstrap/transition';
import 'bootstrap-sass/assets/javascripts/bootstrap/carousel';
import Hammer from 'hammerjs';

let carouselElement;

function trackEvent(action, label) {
    const obj = {
        event: 'home-page-carousel',
        event_category: 'Homepage Carousel',
        event_action: action,
    };
    if (label) {
        obj.event_label = label;
    }
    if (typeof dataLayer !== 'undefined') {
        dataLayer.push(obj);
    }
}

function addTouchEventListener() {
    const listenTo = carouselElement.find('.carousel-inner');
    if (listenTo.length) {
        const hammertime = new Hammer(listenTo.get(0));
        hammertime.on('swipeleft', (event) => {
            carouselElement.carousel('next');
            trackEvent('swipeleft');
        });
        hammertime.on('swiperight', (event) => {
            carouselElement.carousel('prev');
            trackEvent('swiperight');
        });
    }
}

function stripSharepointSpaces(domEl) {
    domEl.innerHTML = domEl.innerHTML.replace(/\u200B/g, '');
}

function loadCarouselImg(nth) {
    const pictureEl = document.querySelector(
        `.carousel-bg .carousel-bg__item:nth-child(${nth})`,
    );
    const slideDiv = carouselElement
        .get(0)
        .querySelector(`.item:nth-child(${nth})`);
    if (
        pictureEl
        && Array.from(pictureEl.classList).includes('img-not-loaded')
    ) {
        const bgImgUrl = slideDiv.getAttribute('data-desktop-bg');
        const mobileImgUrl = slideDiv.getAttribute('data-mobile-bg');
        const imgEl = pictureEl.querySelector('img');
        imgEl.src = bgImgUrl;
        imgEl.alt = '';
        if (mobileImgUrl) {
            const sourceDesktop = document.createElement('source');
            sourceDesktop.setAttribute('media', '(min-width: 600px)');
            sourceDesktop.setAttribute('srcset', bgImgUrl);
            const sourceMobile = document.createElement('source');
            sourceMobile.setAttribute('srcset', mobileImgUrl);
            pictureEl.insertBefore(sourceDesktop, imgEl);
            pictureEl.insertBefore(sourceMobile, imgEl);
        }

        pictureEl.classList.remove('img-not-loaded');
        pictureEl.classList.add('preloaded');
        setTimeout(() => {
            pictureEl.classList.remove('preloaded');
        }, 400);
    }
    if (slideDiv) {
        [].forEach.call(slideDiv.querySelectorAll('.lazy'), (el) => {
            el.src = el.getAttribute('data-src');
            el.classList.remove('lazy');
        });
    }
}

function buildCarouselBgs(el) {
    const wrapper = document.querySelector('.carousel-bg');
    const items = el.querySelectorAll('.item');
    // skip the first one
    for (let i = 1; i < items.length; i += 1) {
        const picture = document.createElement('picture');
        // const sourceDesktop = document.createElement('source');
        // sourceDesktop.setAttribute('media', '(min-width: 600px)');
        const img = document.createElement('img');
        picture.setAttribute('class', 'carousel-bg__item img-not-loaded');
        picture.appendChild(img);
        wrapper.appendChild(picture);
    }
    wrapper.querySelector('.carousel-bg__item').classList.add('active');
    // trigger load of lazy load of img sub items on first screen
    loadCarouselImg(1);
    setTimeout(() => {
        // preload next screen
        loadCarouselImg(2);
    }, 6500);
}

function getType(currentItemIndex, relatedTargetIndex, itemsLength, direction) {
    if (
        relatedTargetIndex === itemsLength - 1
        && currentItemIndex === 0
        && direction !== 'left'
    ) {
        // on first item, click prev / auto prev
        return 'prev';
    }
    if (
        relatedTargetIndex === 0
        && currentItemIndex === itemsLength - 1
        && direction !== 'right'
    ) {
        // on last item, click next / auto next
        return 'next';
    }
    return relatedTargetIndex > currentItemIndex ? 'next' : 'prev';
}

function addBgEventListener($el) {
    $el.on('slide.bs.carousel', (event) => {
        const items = Array.from(
            document.querySelectorAll('#home-page-carousel .item'),
        );
        const currentItem = document.querySelector(
            '#home-page-carousel .item.active',
        );
        const { relatedTarget, direction } = event;
        const currentItemIndex = items.indexOf(currentItem);
        const relatedTargetIndex = items.indexOf(relatedTarget);
        loadCarouselImg(relatedTargetIndex + 1);
        const currentBg = document.querySelector(
            `.carousel-bg__item:nth-child(${currentItemIndex + 1})`,
        );
        const nextBg = document.querySelector(
            `.carousel-bg__item:nth-child(${relatedTargetIndex + 1})`,
        );
        const type = getType(
            currentItemIndex,
            relatedTargetIndex,
            items.length,
            direction,
        );
        nextBg.classList.add(type);
        // force reflow
        nextBg.offsetWidth; // eslint-disable-line no-unused-expressions
        currentBg.classList.add(direction);
        nextBg.classList.add(direction);

        function onTransEnd() {
            [].forEach.call(
                document.querySelectorAll('.carousel-bg .carousel-bg__item'),
                (el) => {
                    el.classList.remove(direction);
                    el.classList.remove('active');
                    el.classList.remove(type);
                },
            );
            nextBg.classList.add('active');
        }

        setTimeout(onTransEnd, 700);

        if (relatedTargetIndex + 2 <= items.length) {
            setTimeout(() => {
                loadCarouselImg(relatedTargetIndex + 2);
            }, 500);
        }
    });

    $el.find('.carousel-indicators li').on('click', (event) => {
        trackEvent('click-indicator', event.currentTarget.dataset.slideTo);
    });
    $el.find('.carousel-control').on('click', (event) => {
        trackEvent(`click-${event.currentTarget.dataset.slide}`);
    });
    $el.find('.item a').on('click', (event) => {
        trackEvent('click-link', event.target.href);
    });
}

export default {
    init() {
        carouselElement = $('#home-page-carousel');
        stripSharepointSpaces(carouselElement.get(0));
        addTouchEventListener();

        carouselElement.carousel({
            interval: carouselElement.data('interval'),
        });
        if ($('body').hasClass('v2')) {
            addBgEventListener(carouselElement);
            buildCarouselBgs(carouselElement.get(0));
        }
    },
};
