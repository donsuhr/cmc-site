import { describe, it } from 'mocha';
import { expect } from 'chai';
import sinon from 'sinon';
import homePageCarousel from './home-page-carousel';

describe('home-page-carousel', () => {
    it('renders', () => {
        const clock = sinon.useFakeTimers({ toFake: ['setInterval'] });
        document.body.innerHTML = `
            <div id="home-page-carousel" data-interaval="100">
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        one
                    </div>
                    <div class="item">
                        two
                    </div>
                    <div class="item">
                        three
                    </div>
                </div>
            </div>
        `;
        expect(homePageCarousel.init.bind(null)).to.not.throw();
        clock.restore();
    });
});
