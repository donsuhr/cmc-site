import $ from 'jquery';
import config from 'config';
import { Loader as GoogleMapsLoader } from 'google-maps';

export default {
    async init() {
        // https://console.developers.google.com/apis/credentials
        const loader = new GoogleMapsLoader(config.google.googleMapsKey);
        const google = await loader.load();

        if ($('.contact-us-details__map--us').length) {
            const corpLatLang = new google.maps.LatLng(26.396765, -80.104344);
            const corpMap = new google.maps.Map(
                $('.contact-us-details__map--us').get(0),
                {
                    zoom: 16,
                    center: corpLatLang,
                },
            );
            const corpMarker = new google.maps.Marker({
                position: corpLatLang,
                title: 'Campus Management Corp',
            });
            corpMarker.setMap(corpMap);
        }

        if ($('.contact-us-details__map--india').length) {
            const talismaLatLang = new google.maps.LatLng(12.978575, 77.658802);
            const talismaMap = new google.maps.Map(
                $('.contact-us-details__map--india').get(0),
                {
                    zoom: 16,
                    center: talismaLatLang,
                },
            );
            const talsimaMarker = new google.maps.Marker({
                position: talismaLatLang,
                title: 'Campus Management Corp',
            });
            talsimaMarker.setMap(talismaMap);
        }

        if ($('.contact-us-details__map--brazil').length) {
            const brazilLatLang = new google.maps.LatLng(
                -23.5702738,
                -46.6907486,
            );
            const brazilMap = new google.maps.Map(
                $('.contact-us-details__map--brazil').get(0),
                {
                    zoom: 16,
                    center: brazilLatLang,
                },
            );
            const brazilMarker = new google.maps.Marker({
                position: brazilLatLang,
                title: 'Campus Management Corp',
            });
            brazilMarker.setMap(brazilMap);
        }

        if ($('.contact-us-details__map--cincinnati').length) {
            const cincinnatiLatLang = new google.maps.LatLng(
                39.2497471,
                -84.3774229,
            );
            const cincinnatiMap = new google.maps.Map(
                $('.contact-us-details__map--cincinnati').get(0),
                {
                    zoom: 16,
                    center: cincinnatiLatLang,
                },
            );
            const cincinnatiMarker = new google.maps.Marker({
                position: cincinnatiLatLang,
                title: 'Campus Management Corp',
            });
            cincinnatiMarker.setMap(cincinnatiMap);
        }

        if ($('.contact-us-details__map--mclean').length) {
            const mcleanLatLang = new google.maps.LatLng(
                38.9218955,
                -77.2299645,
            );
            const mcleanMap = new google.maps.Map(
                $('.contact-us-details__map--mclean').get(0),
                {
                    zoom: 16,
                    center: mcleanLatLang,
                },
            );
            const mcleanMarker = new google.maps.Marker({
                position: mcleanLatLang,
                title: 'Campus Management Corp',
            });
            mcleanMarker.setMap(mcleanMap);
        }

        if ($('.contact-us-details__map--uk').length) {
            const ukLatLang = new google.maps.LatLng(51.5244006, -0.1108975);
            const ukMap = new google.maps.Map(
                $('.contact-us-details__map--uk').get(0),
                {
                    zoom: 16,
                    center: ukLatLang,
                },
            );
            const mcleanMarker = new google.maps.Marker({
                position: ukLatLang,
                title: 'Campus Management Corp',
            });
            mcleanMarker.setMap(ukMap);
        }

        if ($('.contact-us-details__map--houston').length) {
            const houstonLatLang = new google.maps.LatLng(
                29.7373736,
                -95.5741012,
            );
            const houstonMap = new google.maps.Map(
                $('.contact-us-details__map--houston').get(0),
                {
                    zoom: 16,
                    center: houstonLatLang,
                },
            );
            const houstonMarker = new google.maps.Marker({
                position: houstonLatLang,
                title: 'Campus Management Corp',
            });
            houstonMarker.setMap(houstonMap);
        }

        if ($('.contact-us-details__map--charlotte').length) {
            const charlotteLatLang = new google.maps.LatLng(
                35.231228,
                -80.838278,
            );
            const charlotteMap = new google.maps.Map(
                $('.contact-us-details__map--charlotte').get(0),
                {
                    zoom: 17,
                    center: charlotteLatLang,
                },
            );
            const charlotteMarker = new google.maps.Marker({
                position: charlotteLatLang,
                title: 'Campus Management Corp',
            });
            charlotteMarker.setMap(charlotteMap);
        }
    },
};
