import {
    beforeEach, afterEach, describe, it,
} from 'mocha';
import { expect } from 'chai';
import $ from 'jquery';
import nock from 'nock';
import sinon from 'sinon';
import DocumentListView from './documentListView';
import data from './fixtures/response.json';
import config from '../../../config';

let sut;
const sandbox = sinon.createSandbox();

describe('admin-xls', () => {
    beforeEach(() => {
        nock.disableNetConnect();

        nock(`${config.domains.api}/`)
            .post('/signed/')
            .reply(200, data);

        document.body.innerHTML = `
                 <div class="password-protected-download">
                    <div class="password-protected-download__links" style="display: none;">
                        <a href="${config.domains.api}/signed/excel/cmc/en-us/regional-workshop/">
                            Round Table - Test
                        </a>
                    </div>
                    <div class="password-protected-download__view-container">
                        Loading...
                    </div>
                    <p class="password-protected-download__show-when-loaded">
                        a link
                    </p>
                </div>
            `;

        sut = new DocumentListView({
            $el: $('.password-protected-download__view-container'),
            cookieKey: 'test-sign-excel',
        });
    });

    afterEach(() => {
        nock.cleanAll();
        sandbox.restore();
        sut.destroy();
    });

    describe('documentListView', () => {
        it('renders', (done) => {
            sut.render();
            setTimeout(() => {
                expect(
                    document.querySelectorAll(
                        '.password-protected-download__list-item',
                    ).length,
                ).to.equal(
                    document.querySelectorAll(
                        '.password-protected-download__links a',
                    ).length,
                );
                expect(
                    document.querySelectorAll(
                        '.password-protected-download__list-item__link',
                    )[0].href,
                ).to.equal(data.signedRequest);
                done();
            }, 50);
        });

        it('expires the links', (done) => {
            const clock = sinon.useFakeTimers({ toFake: ['setInterval'] });

            sut.render();
            setTimeout(() => {
                clock.tick(510);
                expect(
                    document.querySelector(
                        '.password-protected-download__list-item__dl-text',
                    ).textContent,
                ).to.equal('Link Expired');
                clock.restore();
                done();
            }, 50);
        });

        it('reloads the window on refresh', (done) => {
            sut.render();
            setTimeout(() => {
                const clickEvent = new window.Event('click');
                const oldWindow = window;
                global.window = Object.create(window);
                Object.defineProperty(window, 'location', {
                    value: {
                        href: 'http://example.org/',
                    },
                });
                window.location.reload = sandbox.stub();
                document
                    .querySelector(
                        '.password-protected-download__refresh-button',
                    )
                    .dispatchEvent(clickEvent);
                expect(window.location.reload.called).to.equal(true);
                global.window = oldWindow;
                done();
            }, 50);
        });
    });

    describe('documentListView custom setup', () => {
        it('shows an unauth error', (done) => {
            nock.cleanAll();
            nock(`${config.domains.api}/`)
                .post('/signed/')
                .reply(401);
            const clock = sinon.useFakeTimers({ toFake: ['setInterval'] });
            sut.render();
            setTimeout(() => {
                expect(
                    document.querySelector(
                        '.password-protected-download__form__error',
                    ).textContent,
                ).to.equal('Token expired or invalid. Please log in again.');
                clock.restore();
                done();
            }, 150);
        });

        it('shows an  error with unknown code', (done) => {
            nock.cleanAll();
            nock(`${config.domains.api}/`)
                .post('/signed/')
                .reply(200, '{broken}');
            const clock = sinon.useFakeTimers({ toFake: ['setInterval'] });
            sinon.stub(console, 'log'); // hide console output
            sut.render();
            setTimeout(() => {
                expect(
                    document.querySelector(
                        '.password-protected-download__form__error',
                    ).textContent,
                ).to.equal('Error loading url. Please try again later.');
                // eslint-disable-next-line no-console
                console.log.restore();
                clock.restore();
                done();
            }, 50);
        });
    });
});
