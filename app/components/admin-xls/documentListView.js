// eslint-disable-next-line import/no-extraneous-dependencies
import EventEmitter from 'events';
import $ from 'jquery';
import Cookies from 'js-cookie';
import config from 'config';
import { checkStatus, parseJSON } from 'fetch-json-helpers';

import listTemplate from './templates/listView.hbs';

const EVENT_REAUTH_CLICKED = 'reauthClicked';

export default function documentListView({ $el, cookieKey }) {
    const eventEmitter = new EventEmitter();
    let expiresTimer;
    const links = {};

    function getSignedUrl(href) {
        const headers = {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        };
        const currentAuth = Cookies.get(cookieKey);
        if (currentAuth) {
            headers.Authorization = `Bearer ${currentAuth}`;
        }
        return fetch(`${config.domains.api}/signed/`, {
            method: 'POST',
            headers,
            body: JSON.stringify({
                url: href,
            }),
        })
            .then(checkStatus)
            .then(parseJSON);
    }

    function onExpired(el) {
        $(el)
            .addClass('expired')
            .find('.password-protected-download__list-item__link')
            .attr('href', '')
            .find('.password-protected-download__list-item__dl-text')
            .text('Link Expired');
        $el.find('.password-protected-download__refresh-button').css(
            'display',
            'block',
        );
    }

    function onExpireInterval(event) {
        $el.find('.password-protected-download__list-item').each(
            (index, el) => {
                const expiresEl = $(el).data('expires');
                const expiresMs = parseInt(expiresEl, 10) - Date.now();
                let expiresS = Math.floor(expiresMs / 1000);
                if (expiresS <= 0) {
                    onExpired(el);
                    expiresS = 0;
                    clearInterval(expiresTimer);
                }
                // $el.find('.expiresMsg').text(`Url expires in ${expiresS} sec.`);
            },
        );
    }

    function removeListeners() {
        $el.find('.password-protected-download__reauth-button').off();
        $el.find('.password-protected-download__refresh-button').off();
    }

    function attachListeners() {
        removeListeners();
        $el.find('.password-protected-download__reauth-button').on(
            'click',
            (event) => {
                event.preventDefault();
                eventEmitter.emit(EVENT_REAUTH_CLICKED);
            },
        );
        $el.find('.password-protected-download__refresh-button').on(
            'click',
            (event) => {
                event.preventDefault();
                window.location.reload();
            },
        );
    }

    function renderLinks() {
        const vm = {
            links,
            hasUnauthorizedItem: Object.values(links).some(
                (x) => x.status === 'error',
            ),
        };

        $el.html(listTemplate(vm));
        if (expiresTimer) {
            clearInterval(expiresTimer);
        }
        expiresTimer = setInterval(onExpireInterval, 500);
        attachListeners();
    }

    function handleGetSignedUrlErrors(error, link) {
        const statusCode = error.response && error.response.status
            ? error.response.status
            : '';
        const errors = {
            401: 'Token expired or invalid. Please log in again.',
            403: 'Role not found in token. Please log in again.',
            404: 'Not Found. Please try again later.',
            500: 'Error loading url. Please try again later.',
        };
        link.status = 'error';
        link.error = errors[statusCode] || 'Error loading url. Please try again later.';

        if (statusCode === '') {
            console.log(error); // eslint-disable-line no-console
        }
        renderLinks();
    }

    function render() {
        $el.parent()
            .find('.password-protected-download__links a')
            .each((index, link) => {
                links[link.href] = {
                    status: 'loading',
                    href: link.href,
                    title: link.text,
                };
                getSignedUrl(link.href)
                    .then((response) => {
                        links[link.href].status = 'loaded';
                        links[link.href].expires = response.expires;
                        links[link.href].signedRequest = response.signedRequest;
                        renderLinks();
                    })
                    .catch((error) => {
                        handleGetSignedUrlErrors(error, links[link.href]);
                    });
            });
        $el.parent()
            .find('.password-protected-download__show-when-loaded')
            .css('display', 'block');
    }

    function destroy() {
        removeListeners();
    }

    return {
        render,
        eventEmitter,
        EVENT_REAUTH_CLICKED,
        destroy,
    };
}
