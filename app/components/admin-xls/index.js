import authAgainstListView from '../password-protected-download/authAgainstListView';
import documentListView from './documentListView';

export default function passwordProtectedDownload() {
    function init(options) {
        const authView = authAgainstListView({
            $el: options.$el.find(
                '.password-protected-download__view-container',
            ),
            cookieKey: 'sign-excel',
        });

        const documentView = documentListView({
            $el: options.$el.find(
                '.password-protected-download__view-container',
            ),
            cookieKey: 'sign-excel',
        });

        documentView.eventEmitter.on(documentView.EVENT_REAUTH_CLICKED, () => {
            documentView.destroy();
            authView.logout();
            authView.render();
        });

        authView.eventEmitter.on(authView.EVENT_AUTHENTICATED, () => {
            authView.destroy();
            documentView.render();
        });
        authView.render();

        return this;
    }

    return {
        init,
    };
}
