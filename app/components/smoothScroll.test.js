const {
    describe, it, afterEach, beforeEach,
} = require('mocha');
const { expect } = require('chai');
const sinon = require('sinon');
const $ = require('jquery');
const sut = require('./smoothScroll').default;

// let clock;
const sandbox = sinon.createSandbox();

describe('smoothScroll', () => {
    beforeEach(() => {
        // clock = sinon.useFakeTimers();
    });

    afterEach(() => {
        sandbox.restore();
        $(document)
            .find('*')
            .off()
            .finish();
    });

    it('clicking results in scrollTo called', (done) => {
        document.body.innerHTML = `
            <a href="#target0" class="smooth-scroll-link">link</a>
            <div id="target0">target</div>`;

        const stub = sandbox.stub(sut, 'scrollTo');
        sut.init();
        setTimeout(() => {
            const clickEvent = new window.Event('click');
            document
                .querySelector('.smooth-scroll-link')
                .dispatchEvent(clickEvent);

            sinon.assert.calledOnce(stub);
            done();
        }, 5);
        // clock.tick(6);
    });

    it('should add class to body', (done) => {
        document.body.innerHTML = `
            <a href="#target1" class="smooth-scroll-link">link</a>
            <div id="target1">target</div>`;
        sut.init();
        setTimeout(() => {
            const clickEvent = new window.Event('click');
            document
                .querySelector('.smooth-scroll-link')
                .dispatchEvent(clickEvent);
            expect(
                document.body.classList.contains('js-smooth-scrolling'),
            ).to.equal(true);
            done();
        }, 5);
        //  clock.tick(6);
    });

    it('should remove class from body after animation (target 2)', (done) => {
        document.body.innerHTML = `
            <a href="#target2" class="smooth-scroll-link">link</a>
            <div id="target2">target</div>`;
        sut.init();
        setTimeout(() => {
            const clickEvent = new window.Event('click');
            document
                .querySelector('.smooth-scroll-link')
                .dispatchEvent(clickEvent);
            $('html').finish();
            setTimeout(() => {
                expect(
                    document.body.classList.contains('js-smooth-scrolling'),
                ).to.equal(false);
                done();
            }, 120);
        }, 5);
        //  clock.tick(6);
    });

    it('should focus the target (target 3)', (done) => {
        document.body.innerHTML = `
            <a href="#target3" class="smooth-scroll-link">link</a>
            <div id="target3" tabindex="0">target</div>`;
        sut.init();
        setTimeout(() => {
            const clickEvent = new window.Event('click');
            document
                .querySelector('.smooth-scroll-link')
                .dispatchEvent(clickEvent);
            $('html').finish();
            expect(document.activeElement.id).to.equal('target3');
            done();
        }, 5);
        //   clock.tick(6);
    });

    it('should focus named elements (target 4)', (done) => {
        document.body.innerHTML = `
            <a href="#target4" class="smooth-scroll-link">link</a>
            <div name="target4" tabindex="0">target</div>`;
        sut.init();
        setTimeout(() => {
            const clickEvent = new window.Event('click');
            document
                .querySelector('.smooth-scroll-link')
                .dispatchEvent(clickEvent);
            $('html').finish();
            expect(document.activeElement.getAttribute('name')).to.equal(
                'target4',
            );
            done();
        }, 5);
        //   clock.tick(6);
    });

    it('should allow direct call (target 5)', () => {
        document.body.innerHTML = `
            <a href="#target5" class="smooth-scroll-link">link</a>
            <div id="target5" tabindex="0">target</div>`;
        sut.scrollTo($('#target5'));
        $('html').finish();
        expect(document.activeElement.id).to.equal('target5');
    });

    it('should push to pushstate', () => {
        document.body.innerHTML = `
            <a href="#target6" class="smooth-scroll-link">link</a>
            <div id="target6" tabindex="0">target</div>`;
        sut.scrollTo($('#target6'));
        $('html').finish();
        expect(window.location.hash).to.equal('#target6');
    });
});
