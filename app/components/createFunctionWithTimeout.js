function createFunctionWithTimeout(callback, optTimeout = 1000) {
    // https://developers.google.com/analytics/devguides/collection/analyticsjs/sending-hits
    let called = false;
    function fn() {
        if (!called) {
            called = true;
            callback();
        }
    }
    setTimeout(fn, optTimeout);
    return fn;
}

export { createFunctionWithTimeout };
