import Backbone from 'backbone';
import _ from 'underscore';
import listItem from './listItem';

const ListCollection = Backbone.Collection.extend({
    model: listItem,
    initialize(models, options) {
        this.options = options;
    },
    comparator(item) {
        return -item.get('publishDate'); // Note the minus!
    },
    applyFilter(filter) {
        const clonedFilter = _.clone(filter.attributes);
        _.each(clonedFilter, (value, key) => {
            if (!value) {
                delete clonedFilter[key];
            }
        });
        let selectedProduct;
        let selectedYear;
        if (clonedFilter.Product) {
            selectedProduct = clonedFilter.Product;
            delete clonedFilter.Product;
        }
        if (clonedFilter.year) {
            selectedYear = clonedFilter.year;
            delete clonedFilter.year;
        }
        let result = _.keys(clonedFilter).length
            ? this.where(clonedFilter)
            : this.models;
        if (selectedProduct) {
            result = _.filter(result, (value, index, collection) => _.contains(value.get('Products'), selectedProduct));
        }
        if (selectedYear) {
            result = _.filter(
                result,
                (value, index, collection) => new Date(value.get('publishDateJSON'))
                    .getFullYear()
                    .toString() === selectedYear,
            );
        }
        return new ListCollection(result, this.options);
    },
    getUniqueProducts() {
        return _.unique(_.flatten(this.pluck('Products'))).sort();
    },
    getUniqueYears() {
        return _.unique(
            _.flatten(
                this.map((x) => new Date(x.get('publishDateJSON')).getFullYear()),
            ),
        )
            .sort()
            .reverse();
    },
});

export default ListCollection;
