/* globals pressReleasesPreloadData */
/* eslint-disable no-new */

import Backbone from 'backbone';
import AppView from './views/appView';
import Router from './router';

const api = {
    init(options) {
        const router = new Router();
        const preloadData = typeof pressReleasesPreloadData !== 'undefined'
            ? pressReleasesPreloadData
            : [];
        new AppView({
            el: options.el,
            router,
            preloadData,
            site: options.site,
        });
        Backbone.history.start();
    },
};

export default api;
