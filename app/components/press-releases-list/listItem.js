import Backbone from 'backbone';

export default Backbone.Model.extend({
    parse(response) {
        response.publishDate = new Date(response.publishDateJSON);
        return response;
    },
});
