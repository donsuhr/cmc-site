import Backbone from 'backbone';
import LoadingAnimation from '../../loading-view';
import SelectedFilterView from '../../list-view-common/selected-filter-view';
import template from '../templates/pressReleasesApp.html';
import ListCollection from '../listCollection';
import ListView from './listView';
import MenuView from './menuView';
import Filter from '../filter';
import {
    getLang,
    eventEmitter as menuLangEmitter,
    EVENT_LANG_CHANGED,
} from '../../menu/menu-lang-filter';

export default Backbone.View.extend({
    initialize(options) {
        this.router = options.router;
        this.render();
        this.loading = new LoadingAnimation({ $container: this.$el });
        this.listCollection = new ListCollection(null, options);
        this.filter = new Filter();
        this.preloadData = options.preloadData;

        this.selectedFilterModel = new Backbone.Model({
            allItemsCollection: this.listCollection,
            filteredCollection: this.listCollection,
            filter: this.filter,
        });

        this.parterListView = new ListView({
            el: this.$el.find('.press-release-list'),
            model: this.selectedFilterModel,
        });

        this.menuView = new MenuView({
            el: this.$el.find('.press-release-list__menu'),
            model: this.selectedFilterModel,
        });

        this.selectedFilterView = new SelectedFilterView({
            el: this.$el.find('.horizontal-menu__selected-filter-text'),
            model: this.selectedFilterModel,
            filterMenuOptions: [{ field: 'Product' }, { field: 'year' }],
        });

        this.listenTo(
            this.listCollection,
            'sync reset',
            this.onListCollectionSync,
        );
        this.listenTo(
            this.listCollection,
            'error',
            this.onListCollectionSyncError,
        );
        this.listenTo(this.listCollection, 'request', this.onSyncStart);
        this.listenTo(this.filter, 'change', this.filterListView);
        this.router.on('route:filter', this.filter.setState, this.filter);
        this.filter.on('change', this.router.updateFilter, this.router);

        if (
            typeof this.preloadData !== 'undefined'
            && this.preloadData.length
        ) {
            getLang().then((lang) => {
                this.filterPreloadData(lang);
            });
        } else {
            // this.listCollection.fetch();
            throw new Error('preload data must be set');
        }

        menuLangEmitter.on(
            EVENT_LANG_CHANGED,
            this.filterPreloadData.bind(this),
        );
    },
    render(event) {
        this.$el.html(template());
    },
    onListCollectionSync(event) {
        this.onSyncEnd();
        this.filterListView();
    },
    filterPreloadData(lang) {
        const langKey = `in-nav--${lang}`;
        const menuLangFilteredData = this.preloadData.filter(
            (x) => Object.hasOwnProperty.call(x, langKey) && x[langKey],
        );
        this.listCollection.reset(menuLangFilteredData, { parse: true });
    },
    filterListView() {
        const filtered = this.listCollection.applyFilter(this.filter);
        this.selectedFilterModel.set('filteredCollection', filtered);
    },
    onListCollectionSyncError(collection, resp, options) {
        console.log('Error:', resp, resp.stack); // eslint-disable-line no-console
        this.onSyncEnd();
    },
    onSyncStart() {
        this.loading.show();
    },
    onSyncEnd() {
        this.loading.hide();
    },
});
