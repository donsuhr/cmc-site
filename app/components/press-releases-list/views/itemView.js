import Backbone from 'backbone';

import template from '../templates/pressRelease.html';

export default Backbone.View.extend({
    tagName: 'li',
    render() {
        this.$el.html(template(this.model.toJSON()));
        return this;
    },
});
