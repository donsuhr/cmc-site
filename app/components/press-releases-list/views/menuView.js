import Backbone from 'backbone';
import _ from 'underscore';
import SubMenu from '../../menu/horizontalMenuItemView';

import 'superfish';

const removeMotion = window.matchMedia('(prefers-reduced-motion: reduce), (update: slow)').matches;
const speed = removeMotion ? 0 : 'fast';
const speedOut = removeMotion ? 0 : 'fast';

const superFishOptions = {
    cssArrows: false,
    speed,
    speedOut,
    animation: { height: 'show' },
    animationOut: { height: 'hide' },
};

export default Backbone.View.extend({
    submenus: {},
    initialize(options) {
        this.listenTo(this.model.get('filter'), 'change', this.onFilterChange);
        this.listenTo(
            this.model.get('allItemsCollection'),
            'request',
            this.renderReset,
        );
        this.listenTo(
            this.model.get('allItemsCollection'),
            'sync reset',
            this.updateMenuItemData,
        );
        this.submenus = {};
        this.render();
    },
    events: {
        'horizontalMenuItem:change': 'onMenuChange',
    },
    renderReset() {
        this.$el.superfish('destroy');
        _.map(this.submenus, (value, key) => {
            value.setItems([]);
        });
    },
    updateMenuItemData() {
        const items = this.model.get('allItemsCollection');
        this.submenus.productsMenu.setItems(items.getUniqueProducts());
        this.submenus.yearsMenu.setItems(items.getUniqueYears());
        this.$el.superfish('destroy').superfish(superFishOptions);
        this.onFilterChange();
    },
    render(event) {
        this.submenus.yearsMenu = new SubMenu({
            items: [],
            property: 'year',
            unselectedText: 'Select Year',
        });
        this.$el.append(this.submenus.yearsMenu.el);
        this.submenus.productsMenu = new SubMenu({
            items: [],
            property: 'Product',
            unselectedText: 'Select Product',
        });
        this.$el.append(this.submenus.productsMenu.el);
    },
    onFilterChange(event) {
        const filter = this.model.get('filter');
        this.submenus.productsMenu.setSelectedText(filter.get('Product'));
        this.submenus.yearsMenu.setSelectedText(filter.get('year'));
    },
    onMenuChange(event, data) {
        this.model.get('filter').set(data.property, data.value);
    },
});
