import { beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import $ from 'jquery';
import AppView from '../views/appView';
import Router from '../router';
import preloadData from './fixtures/press-releases.json';

const router = new Router();

describe('press-releases-list', () => {
    describe('appView', () => {
        beforeEach(() => {
            document.body.innerHTML = `
                <section id="pressReleasesApp" class="app">
                    <div class="loading--placeholder">Loading...</div>
                </section>
            `;
        });

        it('renders', (done) => {
            // eslint-disable-next-line no-new
            new AppView({
                el: $('#pressReleasesApp'),
                router,
                docType: 'docType',
                site: 'site',
                group: 'group',
                preloadData,
            });
            setTimeout(() => {
                expect(document.querySelector('.press-release-list')).not.to.a(
                    'null',
                );
                done();
            }, 10);
        });

        it('works with preload data', (done) => {
            // eslint-disable-next-line no-new
            new AppView({
                el: $('#pressReleasesApp'),
                router,
                docType: 'docType',
                site: 'site',
                group: 'group',
                preloadData,
            });
            setTimeout(() => {
                expect(
                    document.querySelectorAll('.press-release-list li').length,
                ).to.equal(preloadData.length);
                done();
            }, 20);
        });

        it('throws without preload data', () => {
            expect(() => {
                // eslint-disable-next-line no-new
                new AppView({
                    el: $('#pressReleasesApp'),
                    router,
                    docType: 'docType',
                    site: 'site',
                    group: 'group',
                });
            }).to.throw(Error, 'preload data must be set');
        });
    });
});
