import { beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import ItemView from '../views/itemView';

let sut;

const item = {
    toJSON() {
        return {
            FileRef: 'fullUrl',
            Title: 'title',
            ArticleStartDate: 'ArticleStartDate',
        };
    },
};

describe('press-releases-list', () => {
    describe('itemView', () => {
        beforeEach(() => {
            document.body.innerHTML = `
                <ul class="s3-doc-list"></ul>
            `;
            sut = new ItemView({ model: item });
        });

        it('renders', () => {
            sut.render();
            expect(sut.$el.find('a').text()).to.equal('title');
            expect(sut.$el.find('p').text()).to.equal('ArticleStartDate');
        });
    });
});
