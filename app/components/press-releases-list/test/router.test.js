import {
    beforeEach, afterEach, describe, it,
} from 'mocha';
import { expect } from 'chai';
import sinon from 'sinon';
import Backbone from 'backbone';
import sut from '../router';

// let clock;
const sandbox = sinon.createSandbox();

describe('press-release-list', () => {
    describe('router', () => {
        beforeEach(() => {
            document.location.hash = '';
            Backbone.history.start();
        });

        afterEach(() => {
            sandbox.restore();
            Backbone.history.stop();
        });

        it('updates the filter on navigate', (done) => {
            const routerSpy = sandbox.spy(sut.prototype, 'filter');
            new sut(); // eslint-disable-line no-new,new-cap

            document.location.hash = '#filter/Product/foo';
            setTimeout(() => {
                expect(routerSpy.calledWith('foo')).to.equal(true);
                done();
            }, 10);
        });

        it('updates the url on updateFilter', () => {
            const router = new sut(); // eslint-disable-line no-new,new-cap
            router.updateFilter({ attributes: { Product: 'bar' } });
            expect(document.location.hash).to.equal('#filter/Product/bar');
            router.updateFilter({
                attributes: { Product: 'baz', Catagory: '' },
            });
            expect(document.location.hash).to.equal('#filter/Product/baz');
        });
    });
});
