const { describe, it } = require('mocha');
const { expect } = require('chai');
const Sut = require('../filter').default;

describe('press-releases-list', () => {
    describe('filter', () => {
        it('updates the filter state', () => {
            const sut = new Sut();
            sut.setState({ Product: ' foo ' });
            expect(sut.get('Product')).to.equal('foo');
        });
    });
});
