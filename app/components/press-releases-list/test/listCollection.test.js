const { describe, it } = require('mocha');
const { expect } = require('chai');
const Sut = require('../listCollection').default;
const preloadData = require('./fixtures/press-releases.json');

const options = {
    s3BucketPrefix: 's3BucketPrefix/',
    docType: 'docType',
    site: 'site',
    group: 'group',
};

describe('press-releases-list', () => {
    describe('listCollection', () => {
        it('is ordered by publishDate', (done) => {
            const sut = new Sut(null, options);
            sut.reset(preloadData, { parse: true });
            setTimeout(() => {
                let lastPriority = new Date('2215-12-12');
                sut.each((model) => {
                    expect(model.get('publishDate') <= lastPriority).to.equal(
                        true,
                    );
                    lastPriority = model.get('publishDate');
                });
                done();
            }, 10);
        });

        it('gets unique products', (done) => {
            const sut = new Sut(null, options);
            sut.reset(preloadData, { parse: true });
            setTimeout(() => {
                expect(sut.getUniqueProducts()).to.eql([
                    'CampusNexus CRM',
                    'CampusNexus Finance HR & Payroll',
                    'CampusNexus Student',
                ]);
                done();
            }, 10);
        });

        it('returns a new filtered collection', (done) => {
            const sut = new Sut(null, options);
            sut.reset(preloadData, { parse: true });
            setTimeout(() => {
                const filtered = sut.applyFilter({
                    attributes: { Product: 'CampusNexus Student' },
                });
                filtered.each((model) => {
                    expect(model.get('Products')).to.include(
                        'CampusNexus Student',
                    );
                });
                done();
            }, 10);
        });
    });
});
