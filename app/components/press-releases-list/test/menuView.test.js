import { beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import sinon from 'sinon';
import $ from 'jquery';
import MenuView from '../views/menuView';

let sut;

const filter = {
    val: 'two',
    set() {},
    get() {
        return this.val;
    },
    on() {},
};

const menuListCollection = {
    get(prop) {
        const props = {
            allItemsCollection: {
                getUniqueProducts() {
                    return ['one', 'two', 'three'];
                },
                getUniqueYears() {
                    return ['one', 'two', 'three'];
                },
                on() {},
            },
            filter,
        };
        return props[prop];
    },
};

describe('press-releases-list', () => {
    describe('menuView', () => {
        beforeEach(() => {
            document.body.innerHTML = `
                <ul class="press-release-list__menu sf-menu horizontal-menu"></ul>
            `;
            sut = new MenuView({
                el: $('.press-release-list__menu'),
                model: menuListCollection,
            });
        });

        it('renders', () => {
            expect(
                document.querySelector('.press-release-list__menu > li > a'),
            ).not.to.a('null');
            filter.val = '';
            sut.updateMenuItemData();
            expect(
                document.querySelector('.press-release-list__menu > li > a')
                    .textContent,
            ).to.equal('Select Year');
        });

        it('updates menu item data', () => {
            filter.val = 'two';
            sut.updateMenuItemData();
            expect(
                document.querySelector('.press-release-list__menu > li > a')
                    .textContent,
            ).to.equal('two');
        });

        it('changes the menu', () => {
            const spy = sinon.spy(filter, 'set');
            const newFilter = {
                property: 'Product',
                value: 'three',
            };
            sut.onMenuChange({}, newFilter);
            expect(
                spy.calledWith(newFilter.property, newFilter.value),
            ).to.equal(true);
        });

        it('shows default text', () => {
            document.body.innerHTML = `
                <ul class="press-release-list__menu sf-menu horizontal-menu"></ul>
            `;
            sut = new MenuView({
                el: $('.press-release-list__menu'),
                model: menuListCollection,
            });
            filter.val = '';
            sut.updateMenuItemData();
            expect(
                document.querySelector('.press-release-list__menu > li > a')
                    .textContent,
            ).to.equal('Select Year');
        });

        it('resets', () => {
            sut.updateMenuItemData();
            expect(
                document
                    .querySelectorAll('.press-release-list__menu > li')[0]
                    .querySelectorAll('ul > li').length,
            ).to.equal(4);
            sut.renderReset();
            expect(
                document
                    .querySelectorAll('.press-release-list__menu > li')[0]
                    .querySelectorAll('ul > li').length,
            ).to.equal(1);
        });
    });
});
