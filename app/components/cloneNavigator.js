import clone from 'lodash/clone';
import isNil from 'lodash/isNil';
import isEmpty from 'lodash/isEmpty';
import omitBy from 'lodash/omitBy';

function cloneNavigator() {
    const navigator = {};
    // eslint-disable-next-line guard-for-in, no-restricted-syntax
    for (const i in window.navigator) {
        navigator[i] = clone(window.navigator[i]);
    }
    return omitBy(navigator, (x) => isNil(x) || isEmpty(x));
}

export { cloneNavigator };
