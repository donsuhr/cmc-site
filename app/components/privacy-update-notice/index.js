import Basil from 'basil.js';

const basil = new Basil();

function hide(el, clicked = false) {
    const matchKey = el.getAttribute('data-match-key');
    const visible = el.style.visibility;
    el.style.cssText = `visibility: ${visible};`;
    document.body.classList.remove('has-location-notification');
    el.setAttribute('aria-hidden', true);
    el.style.cssText = 'visibility: hidden;';
    if (clicked) {
        basil.set(`location-notification--${matchKey}-dismissed`, true);
    }
}

function show(el) {
    const clickHandler = (event) => {
        event.preventDefault();
        hide(el, true);
        el.removeEventListener('click', clickHandler);
    };
    const closeBtn = el.querySelector('.location-notification-close-btn');
    if (closeBtn) {
        closeBtn.addEventListener('click', clickHandler);
    }
    el.setAttribute('aria-hidden', false);
    el.style.cssText = 'visibility: visible;';
    document.body.classList.add('has-location-notification');
}

const api = {
    init({ el }) {
        if (!el) {
            return;
        }
        const matchKey = el.getAttribute('data-match-key');
        const dismissVal = basil.get(
            `location-notification--${matchKey}-dismissed`,
        );
        el.style.cssText = 'visibility: hidden;';
        if (dismissVal === true) {
            hide(el);
        } else {
            show(el);
        }
    },
};

export { api };
