import config from 'config';
import Basil from 'basil.js';

import { checkStatus, parseJSON } from 'fetch-json-helpers';

const basil = new Basil();

function hide(el, clicked = false) {
    el.classList.add('location-notification--hidden');
    const matchKey = el.getAttribute('data-match-key');
    document.body.classList.remove('has-location-notification');
    if (clicked) {
        basil.set(`location-notification--${matchKey}-dismissed`, true);
    }
}

function show(el) {
    el.classList.remove('location-notification--hidden');
    document.body.classList.add('has-location-notification');
    const clickHandler = (event) => {
        event.preventDefault();
        hide(el, true);
        el.removeEventListener('click', clickHandler);
    };
    const closeBtn = el.querySelector('.location-notification-close-btn');
    if (closeBtn) {
        closeBtn.addEventListener('click', clickHandler);
    }
}

function doFetch(el) {
    el.classList.add('location-notification--hidden');
    const matchKey = el.getAttribute('data-match-key');
    const matchVal = el.getAttribute('data-match-value');
    const endpoint = `${config.domains.api}/location`;
    return fetch(endpoint, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
    })
        .then(checkStatus)
        .then(parseJSON)
        .then((result) => {
            if (result[matchKey] === matchVal) {
                basil.set(
                    `location-notification--${matchKey}-val`,
                    result[matchKey],
                    {
                        sameSite: 'Strict',
                        secure: true,
                    },
                );
                show(el);
            } else {
                hide(el);
            }
        });
}

const api = {
    init({ el }) {
        const matchKey = el.getAttribute('data-match-key');
        const matchVal = el.getAttribute('data-match-value');
        const cookieVal = basil.get(`location-notification--${matchKey}-val`);
        const dismissVal = basil.get(
            `location-notification--${matchKey}-dismissed`,
        );
        if (cookieVal !== null || dismissVal === true) {
            if (cookieVal === matchVal) {
                if (!dismissVal) {
                    show(el);
                }
            } else {
                hide(el);
            }
            return Promise.resolve();
        }
        return doFetch(el);
    },
};

export { api };
