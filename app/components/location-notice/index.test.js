import {
    beforeEach, afterEach, describe, it,
} from 'mocha';
import { expect } from 'chai';
import Basil from 'basil.js';
import nock from 'nock';
import config from '../../../config';
import { api } from './index';

let el;
const basil = new Basil();

describe('location-notification', () => {
    beforeEach(() => {
        nock.disableNetConnect();

        document.body.innerHTML = `
                <div
                    data-match-key="country_name" 
                    data-match-value="India">United States</div>
              `;
        el = document.querySelector('div');
        basil.remove('location-notification--country_name-val');
        basil.remove('location-notification--country_name-dismissed');
    });

    afterEach(() => {
        nock.cleanAll();
    });

    it('init calls through to server api', async () => {
        nock.cleanAll();
        const scope = nock(`${config.domains.api}/`)
            .get('/location')
            .replyWithFile(200, `${__dirname}/fixture/location-data.json`);
        await api.init({ el });
        expect(scope.isDone()).to.equal(true);
    });

    it('hides if the country does not match', async () => {
        nock.cleanAll();
        nock(`${config.domains.api}/`)
            .get('/location')
            .replyWithFile(200, `${__dirname}/fixture/location-data.json`);
        document.body.innerHTML = `
                <div 
                    data-match-key="country_name" 
                    data-match-value="India">India</div>
              `;
        await api.init({ el });
        expect(Array.from(el.classList)).to.contain(
            'location-notification--hidden',
        );
    });

    it('shows if the country matches', async () => {
        nock.cleanAll();
        nock(`${config.domains.api}/`)
            .get('/location')
            .replyWithFile(200, `${__dirname}/fixture/location-data.json`);
        document.body.innerHTML = `
                <div 
                    data-match-key="country_name" 
                    data-match-value="United States">United States</div>
              `;
        el = document.querySelector('div');
        await api.init({ el });
        expect(Array.from(el.classList)).not.to.contain(
            'location-notification--hidden',
        );
    });

    it('hides after clicking the close button', async () => {
        nock.cleanAll();
        nock(`${config.domains.api}/`)
            .get('/location')
            .replyWithFile(200, `${__dirname}/fixture/location-data.json`);
        document.body.innerHTML = `
                <div data-match-key="country_name" data-match-value="United States">
                    United States
                    <button class="location-notification-close-btn"></button>
                </div>
              `;
        el = document.querySelector('div');
        await api.init({ el });
        expect(Array.from(el.classList)).not.to.contain(
            'location-notification--hidden',
        );
        el.querySelector('button').click();
        expect(Array.from(el.classList)).to.contain(
            'location-notification--hidden',
        );
    });

    it('does not re request a requested val', async () => {
        nock.cleanAll();
        nock(`${config.domains.api}/`)
            .get('/location')
            .replyWithFile(200, `${__dirname}/fixture/location-data.json`);
        document.body.innerHTML = `
                <div data-match-key="country_name" data-match-value="United States">
                    United States
                    <button class="location-notification-close-btn"></button>
                </div>
              `;
        el = document.querySelector('div');
        await api.init({ el });
        await api.init({ el });
    });

    it('shows on next page if not dismissed', async () => {
        nock.cleanAll();
        nock(`${config.domains.api}/`)
            .get('/location')
            .replyWithFile(200, `${__dirname}/fixture/location-data.json`);
        document.body.innerHTML = `
                <div data-match-key="country_name" data-match-value="United States">
                    United States
                    <button class="location-notification-close-btn"></button>
                </div>
              `;
        el = document.querySelector('div');
        await api.init({ el });
        // hide it
        el.classList.add('location-notification--hidden');
        await api.init({ el });
        expect(Array.from(el.classList)).not.to.contain(
            'location-notification--hidden',
        );
    });

    it('does not show if dismissed', async () => {
        nock.cleanAll();
        nock(`${config.domains.api}/`)
            .get('/location')
            .replyWithFile(200, `${__dirname}/fixture/location-data.json`);
        document.body.innerHTML = `
                <div data-match-key="country_name" data-match-value="United States">
                    United States
                    <button class="location-notification-close-btn"></button>
                </div>
              `;
        el = document.querySelector('div');
        await api.init({ el });
        // hide it
        el.querySelector('button').click();
        await api.init({ el });
        expect(Array.from(el.classList)).to.contain(
            'location-notification--hidden',
        );
    });
});
