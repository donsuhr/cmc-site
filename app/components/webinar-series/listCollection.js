import config from 'config';
import s3ListCollection from '../s3-doc-list/listCollection';

const ListCollection = s3ListCollection.extend({
    url() {
        const {
            site, docType, group, type,
        } = this.options;
        return `${config.domains.api}/${docType}/${site}/${group}/${type}`;
    },
    comparator(item) {
        if (this.type === 'upcoming') {
            return item.get('date');
        }
        return -item.get('priority');
    },
    initialize(models, options) {
        // eslint-disable-next-line no-underscore-dangle, prefer-rest-params
        this.constructor.__super__.initialize.apply(this, arguments);
    },
});

export default ListCollection;
