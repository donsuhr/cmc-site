import config from 'config';

export default {
    init({ isTestMode, isAsmCaDotCom }) {
        const alwaysFalse = false; // https://github.com/webpack/webpack/issues/4857
        if (
            (process.env.NODE_ENV === 'production'
                && !isTestMode
                && !isAsmCaDotCom)
            || alwaysFalse
        ) {
            if (
                config.domains.self === 'https://www.campusmanagement.com'
                || alwaysFalse
            ) {
                const hasMatchMedia = !!window.matchMedia;
                if (
                    !hasMatchMedia
                    || (hasMatchMedia
                    && window.matchMedia('only screen and (min-width: 769px)')
                        .matches)
                ) {
                    if (window.location.search.indexOf('disableLC') === -1) {
                        setTimeout(() => {
                            window.__lc = {}; // eslint-disable-line no-underscore-dangle
                            // eslint-disable-next-line no-underscore-dangle
                            window.__lc.license = config.liveChatId;

                            const lc = document.createElement('script');
                            lc.type = 'text/javascript';
                            lc.async = true;
                            lc.src = `${
                                document.location.protocol === 'https:'
                                    ? 'https://'
                                    : 'http://'
                            }cdn.livechatinc.com/tracking.js`;
                            const s = document.getElementsByTagName(
                                'script',
                            )[0];
                            s.parentNode.insertBefore(lc, s);
                        }, 5500);
                    }
                }
            }
        }
    },
};
