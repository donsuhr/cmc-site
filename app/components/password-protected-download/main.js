import authAgainstListView from './authAgainstListView';
import documentListView from './documentListView';

export default function passwordProtectedDownload() {
    function init(options) {
        const listView = documentListView({
            $el: options.$el.find(
                '.password-protected-download__view-container',
            ),
            site: options.site,
            group: options.group,
            docType: options.docType,
            cookieKey: 'ppdu_token',
        });

        const authView = authAgainstListView({
            $el: options.$el.find(
                '.password-protected-download__view-container',
            ),
            cookieKey: 'ppdu_token',
        });

        listView.eventEmitter.on(listView.EVENT_REAUTH_CLICKED, () => {
            listView.destroy();
            authView.logout();
            authView.render();
        });

        authView.eventEmitter.on(authView.EVENT_AUTHENTICATED, () => {
            authView.destroy();
            listView.render();
        });
        authView.render();
        return this;
    }

    return {
        init,
    };
}
