// eslint-disable-next-line import/no-extraneous-dependencies
import EventEmitter from 'events';
import $ from 'jquery';
import docList from './documentList';
import listTemplate from './templates/listView.hbs';

import i18n from './nls/strings.json';

const EVENT_REAUTH_CLICKED = 'reauthClicked';

export default function documentListView({
    $el,
    site,
    group,
    docType,
    cookieKey,
}) {
    const docListInstance = docList(cookieKey);
    const eventEmitter = new EventEmitter();
    let expiresTimer;

    function onExpired(el) {
        $(el)
            .addClass('expired')
            .find('.password-protected-download__list-item__link')
            .attr('href', '')
            .find('.password-protected-download__list-item__dl-text')
            .text('Link Expired');
        $el.find('.password-protected-download__refresh-button').css(
            'display',
            'block',
        );
    }

    function onExpireInterval(event) {
        $el.find('.password-protected-download__list-item').each(
            (index, el) => {
                const expiresEl = $(el).data('expires');
                const expiresMs = parseInt(expiresEl, 10) * 1000 - Date.now();
                let expiresS = Math.floor(expiresMs / 1000);
                if (expiresS <= 0) {
                    onExpired(el);
                    expiresS = 0;
                    clearInterval(expiresTimer);
                }
                // $el.find('.expiresMsg').text(`Url expires in ${expiresS} sec.`);
            },
        );
    }

    function removeListeners() {
        $el.find('.password-protected-download__reauth-button').off();
        $el.find('.password-protected-download__refresh-button').off();
    }

    function attachListeners() {
        removeListeners();
        $el.find('.password-protected-download__reauth-button').on(
            'click',
            (event) => {
                event.preventDefault();
                eventEmitter.emit(EVENT_REAUTH_CLICKED);
            },
        );
        $el.find('.password-protected-download__refresh-button').on(
            'click',
            (event) => {
                event.preventDefault();
                window.location.reload();
            },
        );
    }

    function onListLoad(items) {
        const vm = {
            downloadTxt: i18n.download,
        };
        vm.items = items.data.filter((x) => x !== 'UNAUTHORIZED');
        vm.hasUnauthorizedItem = items.data.some((x) => x === 'UNAUTHORIZED');
        $el.html(listTemplate(vm));
        $el.parent()
            .find('.password-protected-download__show-when-loaded')
            .css('display', 'block');
        if (expiresTimer) {
            clearInterval(expiresTimer);
        }
        expiresTimer = setInterval(onExpireInterval, 500);
        attachListeners();
    }

    function handleListLoadErrors(error) {
        const statusCode = error.response && error.response.status
            ? error.response.status
            : '';
        const errors = {
            401: i18n.errors.invalidToken,
            403: i18n.errors.noAuthMatch,
            422: i18n.errors.noMatch,
            404: i18n.errors.noMatch,
        };
        const vm = {
            hasUnauthorizedItem: true,
            error: errors[statusCode] || i18n.errors.errorGettingData,
        };

        $el.html(listTemplate(vm));
        attachListeners();
        if (statusCode === '') {
            console.log(error); // eslint-disable-line no-console
        }
    }

    function render() {
        const $idElement = $el
            .parent()
            .find('.password-protected-download__ids');
        let idString = $idElement.is(':input')
            ? $idElement.val()
            : $idElement.text();
        let ids = '';

        idString = $.trim(idString);
        if (idString.toLowerCase() === 'all') {
            ids = docListInstance.LOAD_ALL;
        } else {
            ids = idString.split(/[\s,]+/g);
        }
        return docListInstance
            .load(ids, docType, site, group)
            .then(onListLoad)
            .catch(handleListLoadErrors);
    }

    function destroy() {
        removeListeners();
    }

    return {
        render,
        eventEmitter,
        EVENT_REAUTH_CLICKED,
        destroy,
    };
}
