import config from 'config';
import Cookies from 'js-cookie';
import { checkStatus, parseJSON } from 'fetch-json-helpers';

const PASSWORD_ERROR = 'PASSWORD_ERROR';
const SERVER_ERROR = 'Server Error';
const AUTH_SUCCESS = 'AUTH_SUCCESS';

export default function authAgainstList(cookieKey = 'ppdu_token') {
    function login(username, password) {
        const headers = {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        };
        const currentAuth = Cookies.get(cookieKey);
        if (currentAuth) {
            headers.Authorization = `Bearer ${currentAuth}`;
        }
        return fetch(`${config.domains.api}/password-protected-doc-user/auth`, {
            method: 'POST',
            headers,
            body: JSON.stringify({
                username,
                password,
            }),
        })
            .then(checkStatus)
            .then(parseJSON)
            .then((response) => {
                if (
                    response.error
                    && (response.error.message.indexOf('No Such user') !== -1
                        || response.error.message.indexOf(
                            'Authentication Failed',
                        ) !== -1)
                ) {
                    throw new Error(PASSWORD_ERROR);
                }
                const in45Minutes = new Date(
                    new Date().getTime() + 45 * 60 * 1000,
                );
                Cookies.set(cookieKey, response.pd_user, {
                    secure: true,
                    domain: config.domains.tld,
                    expires: in45Minutes,
                });
                return AUTH_SUCCESS;
            });
    }

    function logout() {
        Cookies.remove(cookieKey, {
            secure: true,
            domain: config.domains.tld,
        });
    }

    function getCurrentToken() {
        return Cookies.get(cookieKey);
    }

    return {
        login,
        logout,
        getCurrentToken,
        PASSWORD_ERROR,
        SERVER_ERROR,
        AUTH_SUCCESS,
    };
}
