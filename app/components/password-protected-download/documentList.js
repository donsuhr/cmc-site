import _isArray from 'lodash/isArray';
import config from 'config';
import Cookies from 'js-cookie';
import { checkStatus, parseJSON } from 'fetch-json-helpers';
import UrlParse from 'url-parse';

const SERVER_ERROR = 'Server Error';
const LOAD_ALL = 'all';

export default function docList(cookieKey) {
    function load(idList, docType, site, group) {
        if (idList !== LOAD_ALL && !_isArray(idList)) {
            throw new Error(
                `incorrect parameter type, needs array, passed: ${idList}`,
            );
        }
        const idListParam = idList === LOAD_ALL ? LOAD_ALL : idList.join(',');
        const groupParam = typeof group === 'undefined' ? '' : `${group}/`;

        const headers = {
            Accept: 'application/json',
        };
        const currentAuth = Cookies.get('ppdu_token');
        if (currentAuth) {
            headers.Authorization = `Bearer ${currentAuth}`;
        }
        const s3groupParam = group ? `--${group}` : '';
        const s3BucketPrefix = config.s3BucketPrefix || '';
        const bucket = `${s3BucketPrefix}${docType}--${site}${s3groupParam}`;
        return fetch(
            `${
                config.domains.api
            }/password-protected-doc/byIdList/${site}/${groupParam}?ids=${idListParam}&bucket=${bucket}`,
            {
                method: 'GET',
                headers,
            },
        )
            .then(checkStatus)
            .then(parseJSON)
            .then((response) => {
                response.data.forEach((x) => {
                    if (x !== 'UNAUTHORIZED') {
                        x.fullUrl = `https://.s3.amazonaws.com/${x.s3url}`;
                        if (process.env.NODE_ENV === 'production') {
                            const urlParts = new UrlParse(x.signedRequest);
                            const [
                                hpType,
                                hpSite = '',
                                hpGroup = '',
                            ] = urlParts.host
                                .substring(0, urlParts.host.indexOf('.'))
                                .split('--');
                            const localHpType = hpType.replace(
                                s3BucketPrefix,
                                '',
                            );
                            x.altUrl = `/docs/${localHpType}/${hpSite}/${hpGroup}${
                                urlParts.pathname
                            }${urlParts.query}`;
                        }
                    }
                });
                return response;
            });
    }

    return {
        load,
        SERVER_ERROR,
        LOAD_ALL,
    };
}
