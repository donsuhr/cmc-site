import {
    beforeEach, afterEach, describe, it,
} from 'mocha';
import { expect } from 'chai';
import Cookies from 'js-cookie';
import nock from 'nock';
import config from '../../../../config';
import DocList from '../documentList';

let sut;
let currentNock;
const cookieKey = 'test';

describe('password-protected-download', () => {
    describe('documentList', () => {
        beforeEach(() => {
            nock.disableNetConnect();

            currentNock = nock(`${config.domains.api}/`)
                .get(/\/password-protected-doc\/byIdList\/site\/(group\/)?/)
                .query(true)
                .replyWithFile(200, `${__dirname}/fixtures/response.json`);
            sut = new DocList(cookieKey);
        });

        afterEach(() => {
            nock.cleanAll();
        });

        it('throws when not passing an array', () => {
            expect(sut.load.bind(sut, 2)).to.throw(
                Error,
                'incorrect parameter type, needs array, passed: 2',
            );
        });

        it('can LOAD_ALL', (done) => {
            sut.load('all', 'docType', 'site', 'group').then((response) => {
                expect(response.data.length).to.be.above(0);
                done();
            });
        });

        it('works with no group', (done) => {
            sut.load('all', 'docType', 'site').then((response) => {
                expect(response.data.length).to.be.above(0);
                done();
            });
        });

        it('appends a fullUrl prop to the response items', (done) => {
            sut.load([22, 23], 'docType', 'site', 'group').then((response) => {
                response.data.forEach((x) => {
                    expect(x.fullUrl).not.to.equal(undefined);
                });
                done();
            });
        });

        it('sets an auth header if the cookie is set', (done) => {
            Cookies.set('ppdu_token', '123', {
                secure: true,
                domain: config.domains.tld,
            });
            currentNock.on('request', (req) => {
                // eslint-disable-next-line no-underscore-dangle
                expect(req._headers.authorization).to.be.eql('Bearer 123');
                currentNock.removeAllListeners('request');
                done();
            });
            sut.load('all', 'docType', 'site', 'group');
        });

        it('sets a production url', (done) => {
            const env = process.env.NODE_ENV;
            process.env.NODE_ENV = 'production';
            sut.load([22, 23], 'docType', 'site', 'group').then((response) => {
                response.data.forEach((x) => {
                    expect(x.altUrl).to.match(
                        /^\/docs\/password-protected-doc\/cmc\/en-us\//,
                    );
                });
                process.env.NODE_ENV = env;
                done();
            });
        });
    });
});
