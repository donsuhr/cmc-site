import {
    beforeEach, afterEach, describe, it,
} from 'mocha';
import { expect } from 'chai';
import $ from 'jquery';
import Cookies from 'js-cookie';
import nock from 'nock';
import sinon from 'sinon';
import config from '../../../../config';
import AuthAgainstListView from '../authAgainstListView';
import data from './fixtures/auth.json';

let sut;
const cookieKey = 'ppdu_token';

describe('password-protected-download', () => {
    describe('authAgainstListView', () => {
        beforeEach(() => {
            nock.disableNetConnect();

            nock(`${config.domains.api}/`)
                .post('/password-protected-doc-user/auth')
                .reply(200, data);

            document.body.innerHTML = `
                <div>
                    <div class="password-protected-download__ids" style="display: none;">57b3a18ff88f2157b6881188</div>
                    <div class="password-protected-download__view-container">
                        Loading...
                    </div>
                </div>
            `;
            sut = new AuthAgainstListView({
                $el: $('.password-protected-download__view-container'),
                cookieKey,
            });
        });

        afterEach(() => {
            nock.cleanAll();
            sut.destroy();
        });

        it('renders', () => {
            Cookies.remove(cookieKey, {
                secure: true,
                domain: config.domains.tld,
            });
            sut.render();
            expect(
                document.getElementById(
                    'password-protected-download__form__username',
                ).length,
            ).not.to.equal(null);
            expect(
                document.getElementById(
                    'password-protected-download__form__password',
                ).length,
            ).not.to.equal(null);
        });

        it('successful submit emits "authenticated"', (done) => {
            const spy = sinon.spy();
            Cookies.remove(cookieKey, {
                secure: true,
                domain: config.domains.tld,
            });
            sut.render();
            sut.eventEmitter.on('authenticated', spy);
            document.getElementById(
                'password-protected-download__form__username',
            ).value = 'user';
            document.getElementById(
                'password-protected-download__form__password',
            ).value = 'pass';
            const keyEvent = new window.Event('keydown');
            keyEvent.which = 13;
            document
                .getElementById('password-protected-download__form__password')
                .dispatchEvent(keyEvent);
            setTimeout(() => {
                expect(spy.called).to.equal(true);
                done();
            }, 200);
        });

        it('emits "authenticated" with cookie', (done) => {
            const spy = sinon.spy();
            sut.eventEmitter.on('authenticated', spy);
            Cookies.remove(cookieKey, {
                secure: true,
                domain: config.domains.tld,
            });
            sut.render();
            const keyEvent = new window.Event('keydown');
            keyEvent.which = 13;
            document
                .getElementById('password-protected-download__form__password')
                .dispatchEvent(keyEvent);
            // cookie set, spy called once
            setTimeout(() => {
                sut.render();
                setTimeout(() => {
                    expect(spy.calledTwice).to.equal(true);
                    done();
                }, 100);
            }, 100);
        });

        it('shows an login error', (done) => {
            nock.cleanAll();
            nock.disableNetConnect();
            nock(`${config.domains.api}/`)
                .post('/password-protected-doc-user/auth')
                .replyWithFile(
                    200,
                    `${__dirname}/fixtures/auth.no-such-user.json`,
                );
            Cookies.remove(cookieKey, {
                secure: true,
                domain: config.domains.tld,
            });
            sut.render();
            const clickEvent = new window.Event('click');

            document
                .querySelector('.password-protected-download__form__submit')
                .dispatchEvent(clickEvent);
            sinon.stub(console, 'log'); // hide console output
            setTimeout(() => {
                expect(
                    document.querySelector(
                        '.password-protected-download__form__error',
                    ).textContent,
                ).to.equal('Incorrect Username or Password.');
                // eslint-disable-next-line no-console
                console.log.restore();
                done();
            }, 100);
        });
    });
});
