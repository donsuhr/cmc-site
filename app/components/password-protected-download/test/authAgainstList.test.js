import {
    beforeEach, afterEach, describe, it,
} from 'mocha';
import { expect } from 'chai';
import Cookies from 'js-cookie';
import nock from 'nock';
import config from '../../../../config';
import AuthAgainstList from '../authAgainstList';
import data from './fixtures/auth.json';

let sut;
const cookieKey = 'test';

describe('password-protected-download', () => {
    describe('authAgainstList', () => {
        beforeEach(() => {
            nock.disableNetConnect();

            nock(`${config.domains.api}/`)
                .post('/password-protected-doc-user/auth')
                .reply(200, data);
            sut = new AuthAgainstList(cookieKey);
        });

        afterEach(() => {
            nock.cleanAll();
            nock.enableNetConnect();
        });

        it('sets a cookie on login', (done) => {
            sut.login('user', 'pass').then(() => {
                const token = Cookies.get(cookieKey);
                expect(token).to.equal(data.pd_user);
                done();
            });
        });

        it('removes the cookie on logout', (done) => {
            sut.login('user', 'pass')
                .then(sut.logout)
                .then(() => {
                    expect(Cookies.get(cookieKey)).to.equal(undefined);
                    done();
                });
        });

        it('returns the token', (done) => {
            sut.login('user', 'pass').then(() => {
                const token = Cookies.get(cookieKey);
                expect(sut.getCurrentToken()).to.equal(token);
                done();
            });
        });

        it('throws for no user', (done) => {
            nock.cleanAll();
            nock(`${config.domains.api}/`)
                .post('/password-protected-doc-user/auth')
                .replyWithFile(
                    200,
                    `${__dirname}/fixtures/auth.no-such-user.json`,
                );
            sut.login('user', 'pass').catch((error) => {
                expect(error.message).to.equal(sut.PASSWORD_ERROR);
                done();
            });
        });
    });
});
