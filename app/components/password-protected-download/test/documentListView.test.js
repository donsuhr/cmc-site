import {
    beforeEach, afterEach, describe, it,
} from 'mocha';
import { expect } from 'chai';
import $ from 'jquery';
import nock from 'nock';
import sinon from 'sinon';
import config from '../../../../config';
import DocumentListView from '../documentListView';
import data from './fixtures/response.json';

let sut;
const sandbox = sinon.createSandbox();

describe('password-protected-download', () => {
    beforeEach(() => {
        nock.disableNetConnect();
        nock(`${config.domains.api}/`)
            .get(/\/password-protected-doc\/byIdList\/site\/(group\/)?/)
            .query(true)
            .reply(200, data);

        document.body.innerHTML = `
                <div>
                    <div class="password-protected-download__ids" style="display: none;">57b3a18ff88f2157b6881188</div>
                    <div class="password-protected-download__view-container">
                        Loading...
                    </div>
                </div>
            `;
        sut = new DocumentListView({
            $el: $('.password-protected-download__view-container'),
            site: 'site',
            group: 'group',
            docType: 'doctype',
            cookieKey: 'ppdu_token',
        });
    });

    afterEach(() => {
        nock.cleanAll();
        sandbox.restore();
        if (window.location.reload.restore) {
            window.location.reload.restore();
        }
        sut.destroy();
    });

    describe('documentListView', () => {
        it('renders', (done) => {
            sut.render().then(() => {
                expect(
                    document.querySelectorAll(
                        '.password-protected-download__list-item',
                    ).length,
                ).to.equal(data.data.length);
                done();
            });
        });

        it('expires the links', (done) => {
            const clock = sinon.useFakeTimers({ toFake: ['setInterval'] });

            sut.render()
                .then(() => {
                    clock.tick(510);
                })
                .then(() => {
                    expect(
                        document.querySelector(
                            '.password-protected-download__list-item__dl-text',
                        ).textContent,
                    ).to.equal('Link Expired');
                    clock.restore();
                    done();
                });
        });

        it('reloads the window on refresh', (done) => {
            // const stub = sandbox.stub(window.location, 'reload');
            const oldWindow = global.window;
            global.window = Object.create(window);
            Object.defineProperty(window, 'location', {
                value: {
                    href: 'http://example.org/',
                },
            });
            window.location.reload = sandbox.stub();
            sut.render().then(() => {
                const clickEvent = new window.Event('click');
                document
                    .querySelector(
                        '.password-protected-download__refresh-button',
                    )
                    .dispatchEvent(clickEvent);
                expect(window.location.reload.called).to.equal(true);
                global.window = oldWindow;
                done();
            });
        });
    });

    describe('documentListView custom setup', () => {
        it('shows an unauth error', (done) => {
            nock.cleanAll();
            nock.disableNetConnect();
            nock(`${config.domains.api}/`)
                .get(/\/password-protected-doc\/byIdList\/site\/(group\/)?/)
                .query(true)
                .reply(401);
            const clock = sinon.useFakeTimers({ toFake: ['setInterval'] });
            sut = new DocumentListView({
                $el: $('.password-protected-download__view-container'),
                site: 'site',
                group: 'group',
                docType: 'doctype',
                cookieKey: 'ppdu_token',
            });
            sut.render().then(() => {
                expect(
                    document.querySelector(
                        '.password-protected-download__form__error',
                    ).textContent,
                ).to.equal('Token expired or invalid. Please log in again.');
                clock.restore();
                done();
            });
        });

        it('shows an  error with unknown code', (done) => {
            nock.cleanAll();
            nock.disableNetConnect();
            document.body.innerHTML = `
                <div>
                    <div class="password-protected-download__ids" style="display: none;">all</div>
                    <div class="password-protected-download__view-container">
                        Loading...
                    </div>
                </div>
            `;
            nock(`${config.domains.api}/`)
                .get(/\/password-protected-doc\/byIdList\/site\/(group\/)?/)
                .query(true)
                .reply(200, '{broken:}');
            sut = new DocumentListView({
                $el: $('.password-protected-download__view-container'),
                site: 'site',
                group: 'group',
                docType: 'doctype',
                cookieKey: 'ppdu_token',
            });
            sinon.stub(console, 'log'); // hide console output
            sut.render().then(() => {
                expect(
                    document.querySelector(
                        '.password-protected-download__form__error',
                    ).textContent,
                ).to.equal(
                    'Error retrieving documents. Please try again later',
                );
                // eslint-disable-next-line no-console
                console.log.restore();
                done();
            });
        });
    });
});
