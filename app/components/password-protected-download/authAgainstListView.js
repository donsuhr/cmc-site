// eslint-disable-next-line import/no-extraneous-dependencies
import EventEmitter from 'events';
import $ from 'jquery';
import decode from 'jwt-decode';
import formTemplate from './templates/formView.html';
import authAgainstList from './authAgainstList';

import i18n from './nls/strings.json';

const EVENT_AUTHENTICATED = 'authenticated';

export default function authAgainstListView({ $el, cookieKey }) {
    const authAgainstListInstance = authAgainstList(cookieKey);
    const eventEmitter = new EventEmitter();
    let formEnabled;

    function checkAlreadySubmitted() {
        const token = authAgainstListInstance.getCurrentToken();
        if (!token) {
            return false;
        }
        const jwt = decode(token);
        return jwt.exp > Date.now() / 1000;
    }

    function showError(textKey) {
        if (textKey) {
            $el.find('.password-protected-download__form__error').text(textKey);
        } else {
            $el.find('.password-protected-download__form__error').text('');
        }
    }

    function hideError() {
        showError(false);
    }

    function setSubmitButtonText(text) {
        $el.find('.password-protected-download__form__submit').text(text);
    }

    function toggleEnableForm(forceOn) {
        const toggleOn = typeof forceOn === 'undefined' ? !formEnabled : !!forceOn;
        if (toggleOn) {
            $el.find(':input').prop('disabled', false);
        } else {
            $el.find(':input').prop('disabled', true);
        }
        formEnabled = toggleOn;
    }

    function handleAuthenticationErrors(error) {
        showError(i18n.errors.errorGettingData);
        if (error instanceof Error) {
            if (error.message === authAgainstListInstance.PASSWORD_ERROR) {
                showError(i18n.errors.loginError.wrongUP);
            }
        }
        toggleEnableForm(true);
        setSubmitButtonText(i18n.login);
        throw new Error(
            typeof error.message !== 'undefined' ? error.message : error,
        );
    }

    function onSubmitClicked(event) {
        event.preventDefault();
        toggleEnableForm(false);
        setSubmitButtonText(i18n.authenticating);
        hideError();

        const username = $.trim($el.find('[name="username"]').val());
        const password = $el.find('[name="password"]').val();

        authAgainstListInstance
            .login(username, password)
            .catch(handleAuthenticationErrors)
            .then(() => {
                eventEmitter.emit('authenticated');
                toggleEnableForm(false);
                setSubmitButtonText(i18n.loading);
            })
            .catch((error) => {
                // any remaining errors
                // eslint-disable-next-line no-console
                console.log('final caught error', error);
            });
    }

    function render() {
        if (checkAlreadySubmitted()) {
            eventEmitter.emit('authenticated');
            return;
        }
        $el.find('.password-protected-download__form__submit').off();
        $el.find(':input').off();
        $el.html(formTemplate({ i18n }));
        $el.find('.password-protected-download__form__submit').on(
            'click',
            onSubmitClicked,
        );
        $el.find(':input').on('keydown', (event) => {
            if (event.which === 13) {
                // eslint-disable-next-line no-use-before-define
                onSubmitClicked(event);
            }
        });
    }

    function destroy() {
        $el.find('.password-protected-download__form__submit').off(
            'click',
            onSubmitClicked,
        );
        $el.find(':input').off('keydown');
    }

    function logout() {
        authAgainstListInstance.logout();
    }

    return {
        EVENT_AUTHENTICATED,
        render,
        eventEmitter,
        destroy,
        logout,
    };
}
