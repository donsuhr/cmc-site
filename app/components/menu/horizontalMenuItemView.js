import Backbone from 'backbone';
import _ from 'underscore';
import $ from 'jquery';

const template = _.template(
    `<ul><% _.each(data.items, function(i) { %>
            <li><a href="#" class="submenu-item"><%= i %></a></li>
        <% }); %></ul>`,
    { variable: 'data' },
);
const VIEW_ALL_TEXT = 'View All';
const LOADING_TEXT = 'Loading...';

export default Backbone.View.extend({
    tagName: 'li',
    className: 'menuItem',
    items: [],

    initialize(options) {
        this.property = options.property;
        this.unselectedText = options.unselectedText;
        this.setItems(options.items);
    },

    events: {
        'click .submenu-item': 'onClick',
    },

    render(event) {
        this.$labelEl = $('<a tabindex="0"></a>');
        this.$el.empty().append(this.$labelEl);
        this.$el.append(template({ items: this.items }));

        if (this.items.length <= 1) {
            this.setSelectedText(LOADING_TEXT);
            this.$el.addClass('disabled');
        } else {
            this.$el.removeClass('disabled');
            this.setSelectedText();
        }
    },

    onClick(event) {
        event.preventDefault();
        let text = $.trim($(event.target).text());
        text = this.isDefaultText(text) ? '' : text;
        this.setSelectedText(text);
        // this.$labelEl.focus();
        this.closeMenu();
        this.$el.trigger('horizontalMenuItem:change', {
            property: this.property,
            value: text,
        });
    },

    setSelectedText(text) {
        text = this.isDefaultText(text) ? this.unselectedText : text;
        this.$labelEl.text(text);
        this.toggleViewAll(text !== this.unselectedText);
    },

    isDefaultText(text) {
        text = $.trim(text);
        return text === '' || text === VIEW_ALL_TEXT;
    },

    closeMenu() {
        this.$el.closest('.sf-js-enabled').superfish('hide');
    },

    toggleViewAll(force) {
        this.$el.find('li:last').toggle(force);
    },

    setItems(items) {
        this.items = items;
        this.items.push(VIEW_ALL_TEXT);
        this.render();
    },
});
