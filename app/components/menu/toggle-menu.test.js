import { beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import { create } from './toggle-menu';

describe('menu toggle-menu', () => {
    beforeEach(() => {
        Object.defineProperties(window.HTMLElement.prototype, {
            offsetHeight: {
                get() {
                    return 100;
                },
            },
        });
        let lastTime = 0;
        window.requestAnimationFrame = function requestAnimationFrame(
            callback,
            element,
        ) {
            const currTime = new Date().getTime();
            const timeToCall = Math.max(0, 16 - (currTime - lastTime));
            const id = window.setTimeout(() => {
                callback(currTime + timeToCall);
            }, timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

        document.body.innerHTML = `
                <div>
                    <button class="theButton">United States</button>
                    <div class="theMenu">
                        <ul class="theMenuList" hidden>
                            <li><a class="menu-item">Menu Item One</a></li>
                            <li><a class="menu-item">Menu Item Two</a></li>
                            <li>
                                <a class="menu-item">Menu Item Two</a>
                                <ul hidden>
                                    <li><a class="sub-menu-item">Sub Menu Item One</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>                    
              `;
    });

    it('can create', () => {
        const menu = create({
            buttonSelector: '.theButton',
            menuSelector: '.theMenu',
            listSelector: '.theMenuList',
        });
        expect(menu.button).not.to.be.null();
    });

    it('starts closed', () => {
        const menu = create({
            buttonSelector: '.theButton',
            menuSelector: '.theMenu',
            listSelector: '.theMenuList',
        });
        expect(menu.button.getAttribute('aria-expanded')).to.equal('false');
        expect(menu.list.getAttribute('hidden')).to.not.be.null();
    });

    it('opens on click', () => {
        const menu = create({
            buttonSelector: '.theButton',
            menuSelector: '.theMenu',
            listSelector: '.theMenuList',
        });
        expect(menu.button.getAttribute('aria-expanded')).to.equal('false');
        const clickEvent = new window.Event('click');
        menu.button.dispatchEvent(clickEvent);
        expect(menu.button.getAttribute('aria-expanded')).to.equal('true');
        expect(menu.list.getAttribute('hidden')).to.equal(null);
    });

    it('closes on click', (done) => {
        const menu = create({
            buttonSelector: '.theButton',
            menuSelector: '.theMenu',
            listSelector: '.theMenuList',
        });
        const clickEvent = new window.Event('click');
        menu.button.dispatchEvent(clickEvent);
        menu.button.dispatchEvent(clickEvent);
        window.requestAnimationFrame(() => {
            window.requestAnimationFrame(() => {
                const transitionEvent = new window.Event('transitionend');
                transitionEvent.propertyName = 'height';
                menu.list.dispatchEvent(transitionEvent);
                window.requestAnimationFrame(() => {
                    expect(menu.button.getAttribute('aria-expanded')).to.equal(
                        'false',
                    );
                    expect(menu.list.getAttribute('hidden')).not.to.equal(null);
                    done();
                });
            });
        });
    });

    it('sets anchors to role button', () => {
        const menu = create({
            buttonSelector: '.theMenuList > li:nth-child(3) > a',
            listSelector: '.theMenuList > li:nth-child(3) > ul',
        });
        expect(menu.button.getAttribute('role')).to.equal('button');
    });

    it('closes on escape', (done) => {
        const menu = create({
            buttonSelector: '.theButton',
            listSelector: '.theMenuList',
        });
        menu.openMenu();
        const keyupEvent = new window.Event('keyup');
        keyupEvent.key = 'Escape';
        menu.button.dispatchEvent(keyupEvent);
        window.requestAnimationFrame(() => {
            window.requestAnimationFrame(() => {
                const transitionEvent = new window.Event('transitionend');
                transitionEvent.propertyName = 'height';
                menu.list.dispatchEvent(transitionEvent);
                window.requestAnimationFrame(() => {
                    expect(menu.button.getAttribute('aria-expanded')).to.equal(
                        'false',
                    );
                    expect(menu.list.getAttribute('hidden')).not.to.equal(null);
                    done();
                });
            });
        });
    });

    it('closes the inner, keeps outer open', () => {
        const outer = create({
            buttonSelector: '.theButton',
            listSelector: '.theMenuList',
        });
        const inner = create({
            buttonSelector: '.theMenuList > li:nth-child(3) > a',
            listSelector: '.theMenuList > li:nth-child(3) > ul',
        });
        outer.openMenu();
        inner.openMenu();
        expect(outer.button.getAttribute('aria-expanded')).to.equal('true');
        const clickEvent = new window.Event('click');
        inner.button.dispatchEvent(clickEvent);
        expect(outer.button.getAttribute('aria-expanded')).to.equal('true');
        expect(inner.button.getAttribute('aria-expanded')).to.equal('false');
    });

    it('sets the height of the list on open', () => {
        const menu = create({
            buttonSelector: '.theButton',
            listSelector: '.theMenuList',
        });
        menu.openMenu();
        expect(menu.list.style.height).not.to.equal('0px');
    });

    it('sets the height of the list on close', async () => {
        const menu = create({
            buttonSelector: '.theButton',
            listSelector: '.theMenuList',
        });
        const transitionEvent = new window.Event('transitionend');
        transitionEvent.propertyName = 'height';

        const openPromise = menu.openMenu();
        menu.list.dispatchEvent(transitionEvent);
        await openPromise;
        const closePromise = menu.closeMenu();
        window.requestAnimationFrame(() => {
            window.requestAnimationFrame(() => {
                menu.list.dispatchEvent(transitionEvent);
                window.requestAnimationFrame(async () => {
                    await closePromise;
                    expect(menu.list.style.height).to.equal('0px');
                });
            });
        });
    });

    it('can do stuff after the open/close', async () => {
        const menu = create({
            buttonSelector: '.theButton',
            listSelector: '.theMenuList',
        });
        const transitionEvent = new window.Event('transitionend');
        transitionEvent.propertyName = 'height';
        const openPromise = menu.openMenu().then(() => {
            menu.button.textContent = 'Close Menu';
        });
        menu.list.dispatchEvent(transitionEvent);
        await openPromise;
        expect(menu.button.textContent).to.equal('Close Menu');
        const closePromise = menu.closeMenu().then(() => {
            menu.button.textContent = 'Open Menu';
            transitionEvent.propertyName = 'height';
            menu.list.dispatchEvent(transitionEvent);
        });
        window.requestAnimationFrame(() => {
            window.requestAnimationFrame(() => {
                menu.list.dispatchEvent(transitionEvent);
                window.requestAnimationFrame(async () => {
                    await closePromise;
                    expect(menu.button.textContent).to.equal('Open Menu');
                });
            });
        });
    });

    it('can destroy', () => {
        const inner = create({
            buttonSelector: '.theMenuList > li:nth-child(3) > a',
            listSelector: '.theMenuList > li:nth-child(3) > ul',
        });
        inner.destroy();
        expect(inner.button.getAttribute('hidden')).to.be.null();
        expect(inner.button.getAttribute('aria-expanded')).to.be.null();
        expect(inner.button.getAttribute('role')).to.be.null();
    });

    it('closes on body click', (done) => {
        const menu = create({
            buttonSelector: '.theButton',
            listSelector: '.theMenuList',
            listenToBodyClick: true,
        });
        const transitionEvent = new window.Event('transitionend');
        transitionEvent.propertyName = 'height';
        menu.openMenu().then(() => {
            const clickEvent = new window.Event('click');
            document.body.dispatchEvent(clickEvent);
            window.requestAnimationFrame(() => {
                window.requestAnimationFrame(() => {
                    menu.list.dispatchEvent(transitionEvent);
                    window.requestAnimationFrame(() => {
                        expect(
                            menu.button.getAttribute('aria-expanded'),
                        ).to.equal('false');
                        expect(menu.list.getAttribute('hidden')).not.to.equal(
                            null,
                        );
                        done();
                    });
                });
            });
        });
        menu.list.dispatchEvent(transitionEvent);
    });

    it('calls callback on open', (done) => {
        const domButton = document.querySelector('.theButton');
        const menu = create({
            buttonSelector: '.theButton',
            listSelector: '.theMenuList',
            onOpen: ({ button, list }) => {
                expect(button).to.equal(domButton);
                done();
            },
        });
        menu.openMenu();
    });

    it('calls callback on close', (done) => {
        const domButton = document.querySelector('.theButton');
        const menu = create({
            buttonSelector: '.theButton',
            listSelector: '.theMenuList',
            onClose: ({ button, list }) => {
                expect(button).to.equal(domButton);
                done();
            },
        });
        menu.openMenu().then(menu.closeMenu);
        const transitionEvent = new window.Event('transitionend');
        transitionEvent.propertyName = 'height';
        menu.list.dispatchEvent(transitionEvent);
    });
});
