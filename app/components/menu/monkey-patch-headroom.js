import Headroom from 'headroom.js';
import $ from 'jquery';

let instance;
let menuHeight;

Headroom.prototype.shouldUnpin = function shouldUnpin(
    currentScrollY,
    toleranceExceeded,
) {
    const scrollingDown = currentScrollY > this.lastKnownScrollY;
    const pastOffset = currentScrollY >= this.offset + menuHeight;

    return scrollingDown && pastOffset && toleranceExceeded;
};

Headroom.prototype.origUpdate = Headroom.prototype.update;

Headroom.prototype.update = function update() {
    const localLastKnownScrollY = this.lastKnownScrollY;

    const $mainNav = $('.left-nav-menu__toggle-button');
    if ($mainNav.attr('aria-expanded') === 'true') {
        return;
    }
    if ($('body').hasClass('js-smooth-scrolling')) {
        return;
    }

    this.origUpdate();

    const currentScrollY = this.getScrollY();
    const scrollDirection = currentScrollY > localLastKnownScrollY ? 'down' : 'up';

    if (
        (scrollDirection === 'down'
            && currentScrollY <= this.offset + menuHeight)
        || (scrollDirection === 'up' && currentScrollY <= this.offset)
    ) {
        this.top();
    } else {
        this.notTop();
    }
};

Headroom.prototype.prevAttachEvent = Headroom.prototype.attachEvent;

Headroom.prototype.attachEvent = function attachEvent() {
    this.prevAttachEvent();
    if (this.lastKnownScrollY > this.offset) {
        this.unpin();
    }
    setTimeout(() => {
        $(this.elem).css('visibility', 'visible');
    }, 500);
};

/* export default {
    init(menuInstance) {
        const bannerHeight = document.querySelector('.news-banner-wrapper')
            .offsetHeight;
        menuHeight = document.getElementById('nav-menu').offsetHeight;
        const offset = bannerHeight;

        const $el = $('.layout__bg--top');
        if ($(document).scrollTop() > 0) {
            $el.css('visibility', 'hidden');
        }
        instance = new Headroom($el.get(0), {
            offset,
            onUnpin() {
                if (this.lastKnownScrollY < offset + menuHeight) {
                    // hide the bar for the transition when first starting
                    $(this.elem).css('visibility', 'hidden');
                    setTimeout(() => {
                        $(this.elem).css('visibility', 'visible');
                    }, 500);
                }
                menuInstance.closeMenu();
            },
        });
        instance.init();
        instance.update();
        $el.addClass('headroom--pinned');
        $el.attr('style', '');
    },
    unpin() {
        if (instance) {
            instance.unpin();
        }
    },
}; */
