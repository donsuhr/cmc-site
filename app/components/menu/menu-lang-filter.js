import EventEmitter from 'events'; // eslint-disable-line import/no-extraneous-dependencies
import config from 'config';
import Basil from 'basil.js';
import queryString from 'query-string';
import { checkStatus, parseJSON } from 'fetch-json-helpers';

const EVENT_LANG_CHANGED = 'event: changed';
const COOKIE_KEY = 'menu-lang';
const COOKIE_EVER_REDIRECTED_KEY = 'ever-redirected';
const DEFAULT_LANG = 'en-us';

const eventEmitter = new EventEmitter();

const basil = new Basil({
    storages: ['cookie', 'local', 'session', 'memory'],
    storage: 'cookie',
    namespace: '',
});

let currentLang = basil.get(COOKIE_KEY);
const queryStringLang = queryString.parse(document.location.search).l;

function decorateLinks() {
    if (!navigator.cookieEnabled) {
        const links = Array.from(document.querySelectorAll('a'));
        const currentHostname = document.location.hostname;
        links.forEach((x, i) => {
            if (
                x.hostname.includes(currentHostname)
                && !x.hasAttribute('data-no-lang')
            ) {
                const cleaned = x.href
                    .replace(/(?:[?&])(l=[^&\n]+)/, '')
                    .replace(/\?$/, '');
                const suffixPrefix = cleaned.includes('?') ? '&' : '?';
                x.href = `${cleaned}${suffixPrefix}l=${currentLang}`;
            }
        });
    }
}

function updateLang(lang) {
    basil.set(COOKIE_KEY, lang, {
        sameSite: 'Strict',
        secure: true,
    });
    currentLang = lang;
    decorateLinks();
    eventEmitter.emit(EVENT_LANG_CHANGED, currentLang);
}

function redirect() {
    if (navigator.cookieEnabled) {
        const everRedirected = basil.get(COOKIE_EVER_REDIRECTED_KEY);
        if (
            !everRedirected
            && document.location.pathname === '/'
            && currentLang !== 'en-us'
            && currentLang !== 'pt-br'
            && currentLang !== 'en-uk' // temporary
            && currentLang !== 'es' // temporary
        ) {
            basil.set(COOKIE_EVER_REDIRECTED_KEY, true, {
                sameSite: 'Strict',
                secure: true,
            });
            window.location = `/${currentLang}`;
        }
    }
}

function fetchAcceptLangInfo() {
    return fetch(`${config.domains.api}/location`)
        .then(checkStatus)
        .then(parseJSON)
        .catch((error) => ({
            hasInLang: false,
            hasEuLang: false,
            hasEsLang: false,
        }));
}

function getLang() {
    if (getLang.cachedCall) {
        return getLang.cachedCall;
    }
    let forceLang = [...queryStringLang][0] || config.forceLang;
    const { pathname, hostname } = document.location;
    if (pathname.startsWith('/pt-br') || hostname.endsWith('.br')) {
        forceLang = 'pt-br';
    }
    if (pathname === '/en-in' || pathname === '/en-in/') {
        forceLang = 'en-in';
    }
    if (pathname === '/en-uk' || pathname === '/en-uk/') {
        forceLang = 'en-uk';
    }
    if (pathname === '/es' || pathname === '/es/') {
        forceLang = 'es';
    }
    const fn = !currentLang && !forceLang ? fetchAcceptLangInfo : Promise.resolve;
    getLang.cachedCall = fn
        .call(Promise)
        .then((result) => {
            let destLang = forceLang || currentLang || DEFAULT_LANG;
            if (result && result.hasInLang) {
                destLang = 'en-in';
            }
            if (result && result.hasEuLang) {
                destLang = 'en-uk';
            }
            if (result && result.hasEsLang) {
                destLang = 'es';
            }
            if (currentLang !== destLang) {
                updateLang(destLang);
                redirect();
            }
            return destLang;
        })
        .finally(() => {
            getLang.cachedCall = false;
        });
    return getLang.cachedCall;
}

function setLang(lang) {
    if (lang !== currentLang) {
        updateLang(lang);
    }
}

export {
    setLang, getLang, eventEmitter, EVENT_LANG_CHANGED, DEFAULT_LANG,
};
