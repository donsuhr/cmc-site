/* eslint eslint-comments/no-unlimited-disable: 0 */
/* eslint-disable */
/**
 * jquery.dlmenu.js v1.0.1
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2013, Codrops
 * http://www.codrops.com
 * https://github.com/codrops/ResponsiveMultiLevelMenu
 */
/*globals jQuery*/
(function(factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else {
        // Browser globals
        factory(jQuery);
    }
})(function($) {
    'use strict';

    // global
    var $body = $('body');

    $.DLMenu = function(options, element) {
        this.$el = $(element);
        this._init(options);
    };

    // the options
    $.DLMenu.defaults = {
        // classes for the animation effects
        animationClasses: {
            classin: 'dl-animate-in-1',
            classout: 'dl-animate-out-1',
        },
        // callback: click a link that has a sub menu
        // el is the link element (li); name is the level name
        onLevelClick: function(el, name) {
            return false;
        },
        // callback: click a link that does not have a sub menu
        // el is the link element (li); ev is the event obj
        onLinkClick: function(el, ev) {
            return false;
        },
        backLabel: 'Back',
        // Change to "true" to use the active item as back link label.
        useActiveItemAsBackLabel: false,
        // Change to "true" to add a navigable link to the active item to its child
        // menu.
        useActiveItemAsLink: false,
        // On close reset the menu to root
        resetOnClose: true,
    };

    $.DLMenu.prototype = {
        _init: function(options) {
            this.$el.addClass('dl-menu-active');
            // options
            this.options = $.extend(true, {}, $.DLMenu.defaults, options);
            // cache some elements and initialize some variables
            this._config();

            // animation end event name
            this.animEndEventName = options.animEndEventName;
            // transition end event name
            this.transEndEventName = options.transEndEventName;
            // support for css animations and css transitions
            this.supportAnimations = !!this.animEndEventName;
            this.supportTransitions = !!this.transEndEventName;

            this._initEvents();
        },

        _config: function() {
            this.open = false;
            this.$trigger = this.$el.find('.dl-trigger');
            this.$menu = this.$el.find('.dl-menu');
            this.$menuitems = this.$menu.find(
                '.left-nav-menu__list-item, .sub-menu-flyout',
            );
            this.$el
                .find('.dl-submenu')
                .prepend(
                    '<li class="dl-back"><a href="#">' +
                        this.options.backLabel +
                        '</a></li>',
                );
            this.$back = this.$menu.find('li.dl-back');

            // Set the label text for the back link.
            if (this.options.useActiveItemAsBackLabel) {
                this.$back.each(function() {
                    var $this = $(this),
                        parentLabel = $this
                            .parents('li:first')
                            .find('a:first')
                            .text();
                    $this.find('a').html(parentLabel);
                });
            }
            // If the active item should also be a clickable link, create one and put
            // it at the top of our menu.
            if (this.options.useActiveItemAsLink) {
                this.$el.find('.dl-submenu').prepend(function() {
                    var parentli = $(this)
                        .parents('li:not(.dl-back):first')
                        .find('a:first');
                    return (
                        '<li class="dl-parent"><a href="' +
                        parentli.attr('href') +
                        '">' +
                        parentli.text() +
                        '</a></li>'
                    );
                });
            }
        },

        _initEvents: function() {
            var self = this;
            this.$trigger.on('click.dlmenu', function(event) {
                if (self.open) {
                    self.closeMenu();
                } else {
                    self.openMenu();
                }
                return false;
            });

            this.$menuitems.on('click.dlmenu', function(event) {
                var $item = $(this),
                    $submenu = $item.children('.dl-submenu');
                // Only go to the next menu level if one exists AND the link isn't the
                // one we added specifically for navigating to parent item pages.
                if (
                    $submenu.length > 0 &&
                    !$(event.currentTarget).hasClass('dl-subviewopen')
                ) {
                    event.stopPropagation();
                    var $flyin = $submenu
                        .clone()
                        .css({ opacity: 0 })
                        .insertAfter(self.$menu);
                    var onAnimationEndFn = function() {
                        self.$menu
                            .off(self.animEndEventName)
                            .removeClass(self.options.animationClasses.classout)
                            .addClass('dl-subview');
                        $item
                            .addClass('dl-subviewopen')
                            .parents('.dl-subviewopen:first')
                            .removeClass('dl-subviewopen')
                            .addClass('dl-subview');
                        $flyin.remove();
                        $submenu.find('a:first').trigger('focus');
                    };

                    setTimeout(function() {
                        self.$menu.addClass(
                            self.options.animationClasses.classout,
                        );
                        $flyin.addClass(self.options.animationClasses.classin);
                        // query height to force redraw (safari)
                        $flyin.height();
                        if (self.supportAnimations) {
                            self.$menu.on(
                                self.animEndEventName,
                                onAnimationEndFn,
                            );
                        } else {
                            onAnimationEndFn.call();
                        }
                        self.options.onLevelClick(
                            $item,
                            $item.children('a:first').text(),
                        );
                    });
                    return false;
                } else {
                    self.options.onLinkClick($item, event);
                }
            });

            this.$back.on('click.dlmenu', function(event) {
                var $this = $(this),
                    $submenu = $this.parents('.dl-submenu:first'),
                    $item = $submenu.parent();

                var $flyin = $submenu
                    .clone()
                    .css({ opacity: 0 })
                    .insertAfter(self.$menu);

                var onAnimationEndFn = function() {
                    self.$menu
                        .off(self.animEndEventName)
                        .removeClass(self.options.animationClasses.classin);
                    $flyin.remove();
                    $submenu.prev('a').trigger('focus');
                };

                setTimeout(function() {
                    self.$menu.addClass(self.options.animationClasses.classin);
                    $flyin.addClass(self.options.animationClasses.classout);
                    // query height to force redraw (safari)
                    $flyin.height();
                    if (self.supportAnimations) {
                        self.$menu.on(self.animEndEventName, onAnimationEndFn);
                    } else {
                        onAnimationEndFn.call();
                    }
                    $item.removeClass('dl-subviewopen');
                    var $subview = $this.parents('.dl-subview:first');
                    if ($subview.is('li')) {
                        $subview.addClass('dl-subviewopen');
                    }
                    $subview.removeClass('dl-subview');
                });

                return false;
            });
        },

        closeMenu: function() {
            if (this.open) {
                this._closeMenu();
            }
        },

        _closeMenu: function() {
            var self = this;
            var onTransitionEndFn = function() {
                self.$menu.off(self.transEndEventName);
                if (self.options.resetOnClose) {
                    self._resetMenu();
                }
            };
            this.$menu.removeClass('dl-menuopen');
            this.$menu.addClass('dl-menu-toggle');
            this.$trigger.removeClass('dl-active');

            if (this.supportTransitions) {
                this.$menu.on(this.transEndEventName, onTransitionEndFn);
            } else {
                onTransitionEndFn.call();
            }
            this.open = false;
        },

        openMenu: function() {
            if (!this.open) {
                this._openMenu();
            }
        },

        _openMenu: function() {
            var self = this;
            // clicking somewhere else makes the menu close
            $body
                .off('.dlmenu')
                .on('click.dlmenu touchend.dlmenu', function(event) {
                    var contains = $.contains(self.$el.get(0), event.target);
                    var isTarget =
                        $(event.target).attr('id') === self.$el.attr('id');
                    if (!contains && !isTarget) {
                        self._closeMenu();
                    }
                });
            this.$menu.addClass('dl-menuopen');
            this.$trigger.addClass('dl-active');
            this.open = true;
        },

        _resetMenu: function() {
            // resets the menu to its original state (first level of options)
            this.$menu.removeClass('dl-subview');
            this.$menuitems.removeClass('dl-subview dl-subviewopen');
        },

        destroy: function() {
            var $this = $(this);
            if (!$this) {
                return false;
            }
            this.$el.removeClass('dl-menu-active');
            this.$el.find('.dl-active').removeClass('dl-active');
            this.$el.find('.dl-menuopen').removeClass('dl-menuopen');
            this.$el.find('.dl-subviewopen').removeClass('dl-subviewopen');
            this.$el.find('.dl-subview').removeClass('dl-subview');
            this.$el.find('.dl-back').remove();

            this.$el.off('.dlmenu');
            this.$trigger.off('.dlmenu');
            this.$menuitems.off('.dlmenu');
            this.$menu.off('.dlmenu');
            $body.off('.dlmenu');

            clearTimeout(this.hoverInTimer);
            clearTimeout(this.mouseLeaveTimer);
            this.$el.removeData('dlmenu');
        },
    };

    $.fn.dlmenu = function(options) {
        if (typeof options === 'string') {
            var args = Array.prototype.slice.call(arguments, 1);
            this.each(function() {
                var instance = $.data(this, 'dlmenu');
                if (instance) {
                    if (
                        !$.isFunction(instance[options]) ||
                        options.charAt(0) === '_'
                    ) {
                        throw new Error(
                            'no such method "' +
                                options +
                                '" for dlmenu instance',
                        );
                    }
                    instance[options].apply(instance, args);
                }
            });
        } else {
            this.each(function() {
                var instance = $.data(this, 'dlmenu');
                if (instance) {
                    instance._init();
                } else {
                    instance = $.data(
                        this,
                        'dlmenu',
                        new $.DLMenu(options, this),
                    );
                }
            });
        }
        return this;
    };
});
