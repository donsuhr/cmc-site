import { beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import Menu from './horizontalMenuItemView';
import 'superfish';

let sut;

describe('horizontalMenuItemView', () => {
    beforeEach(() => {
        sut = new Menu({
            property: 'property',
            unselectedText: 'unselectedText',
            items: [],
        });
    });

    it('renders', () => {
        sut.render();
        expect(sut.$el.find('> a').text()).to.equal('Loading...');
        expect(sut.$el.find('.submenu-item').length).to.equal(1);
    });

    it('can get new items', () => {
        sut.setItems(['one', 'two', 'three']);
        expect(sut.$el.find('.submenu-item').length).to.equal(4);
    });

    it('selecting an item sets the menu text', () => {
        sut.setItems(['one', 'two', 'three']);
        sut.$el
            .find('.submenu-item')
            .eq(1)
            .trigger('click');
        expect(sut.$el.find('> a').text()).to.equal('two');
    });
});
