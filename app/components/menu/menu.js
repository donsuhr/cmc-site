import debounce from 'lodash/debounce';
//import { create as createToggleMenu } from './toggle-menu';
import {
    getLang,
    eventEmitter as menuLangEmitter,
    EVENT_LANG_CHANGED,
    DEFAULT_LANG,
} from './menu-lang-filter';

const SIZE_THRESHOLD = 769;
const MENU_DESKTOP = 'BIG';
const MENU_MOBILE = 'SMALL';
let currentWindowSize = 0;
let subMenus;
let currentMenuLang;
let hiddenMainElements = [];

export default {
    init: function init() {
        let urlLang = 'en-us';

        const { pathname, hostname } = document.location;
        if (pathname.startsWith('/en-in')) {
            urlLang = 'en-in';
        }
        if (pathname.startsWith('/en-uk')) {
            urlLang = 'en-uk';
        }
        if (pathname.startsWith('/pt-br') || hostname.endsWith('.br')) {
            urlLang = 'pt-br';
        }

        function updateMenuSize() {
            const proposedMenuSize = window.matchMedia(
                `(min-width: ${SIZE_THRESHOLD}px)`,
            ).matches
                ? MENU_DESKTOP
                : MENU_MOBILE;
            if (proposedMenuSize === currentWindowSize) {
                // no change in window size
                return;
            }
            currentWindowSize = proposedMenuSize;
            if (proposedMenuSize === MENU_DESKTOP) {
                if (subMenus && subMenus.length) {
                    subMenus.forEach((x) => x.destroy());
                }
                subMenus = null;
            } else {
                subMenus = Array.from(
                    document.querySelectorAll('.main-nav__pages-list > li'),
                ).reduce((acc, x, i) => {
                    const hasSubItems = document.querySelector(
                        `.main-nav__pages-list > li:nth-child(${i + 1}) > ul`,
                    );
                    if (hasSubItems) {
                        acc.push(
                            createToggleMenu({
                                buttonSelector: `.main-nav__pages-list > li:nth-child(${
                                    i + 1
                                }) > a`,
                                listSelector: `.main-nav__pages-list > li:nth-child(${
                                    i + 1
                                }) > ul`,
                            }),
                        );
                    }
                    return acc;
                }, []);
            }
        }

        function restoreHiddenMainElements() {
            const menu = document.querySelector('.main-nav__pages-list');
            hiddenMainElements.reverse().forEach((x) => {
                if (!x.before) {
                    menu.append(x.el);
                } else {
                    menu.insertBefore(x.el, x.before);
                }
            });
            hiddenMainElements = [];
        }

        function updateVisibleMenuItems(lang) {
            const menulangCaps = lang
                .split('-')
                .map((x) => x[0].toUpperCase() + x.slice(1))
                .join('');
            restoreHiddenMainElements();
            document
                .querySelectorAll(
                    '.main-nav__pages-list > .main-nav__pages-list-item',
                )
                .forEach((x) => {
                    if (x.dataset[`inNav-${menulangCaps}`] === undefined) {
                        hiddenMainElements.push({
                            el: x,
                            before: x.nextElementSibling,
                        });
                        x.parentNode.removeChild(x);
                    }
                });
            document
                .querySelectorAll(
                    '.main-nav__pages-list > .main-nav__pages-list-item .main-nav__pages-list-item',
                )
                .forEach((x) => {
                    x.hidden = x.dataset[`inNav-${menulangCaps}`] === undefined;
                });
            document.querySelectorAll(`[data-label--${lang}]`).forEach((x) => {
                x.textContent = x.dataset[`label-${menulangCaps}`];
            });
        }

        function updatePageClasses(lang) {
            document.body.classList.forEach((x) => {
                if (x && x.startsWith('menu-lang')) {
                    document.body.classList.remove(x);
                }
            });
            document.body.classList.add(`menu-lang--${lang}`);
        }

        function onLangUpdated(lang) {
            // console.log('on lang updated', lang, 'current:', currentMenuLang);
            if (currentMenuLang !== lang) {
                updateVisibleMenuItems(lang);
                updatePageClasses(lang);
                currentMenuLang = lang;
            }
        }

        /* const instance = createToggleMenu({
            buttonSelector: '.main-nav__open-menu-button',
            listSelector: '.main-nav__pages-list',
            listenToBodyClick: true,
            onOpen: ({ button }) => {
                button.textContent = urlLang === 'pt-br' ? 'Fechar menu' : 'Close Menu';
                document
                    .querySelector('.layout__bg--top')
                    .classList.add('main-nav-open');
            },
            onAfterClose: () => {
                document
                    .querySelector('.layout__bg--top')
                    .classList.remove('main-nav-open');
            },
            onClose: ({ button }) => {
                button.textContent = urlLang === 'pt-br' ? 'Abrir menu' : 'Open Menu';
            },
        });

        createToggleMenu({
            buttonSelector:
                '.main-nav__static-content .top-nav-menu__charm-button--global',
            listSelector: '.main-nav__static-content .global-nav__toggle-contents',
            listenToBodyClick: true,
        });

        const onWindowResize = debounce(updateMenuSize, 750);
        window.addEventListener('resize', onWindowResize);

        menuLangEmitter.on(EVENT_LANG_CHANGED, onLangUpdated);

        updateMenuSize();
        if (urlLang !== 'pt-br') {
            onLangUpdated(DEFAULT_LANG);
            getLang().then(onLangUpdated);
        } else {
            onLangUpdated(urlLang);
        } */

        //return instance;
    },
};
