---
# <!-- @formatter:off -->
title: Promoting Institutional Success with CampusNexus Finance, HR & Payroll
in-nav--en-us: false
in-nav--en-in: false
in-nav--en-uk: false
page-bg-image: /images/pages/higher-education-resources/insights-bkg1.jpg
# <!-- @formatter:on -->
---

{{#extend "hb-layouts/v3"}} {{#content "body"}}
<article>
    <header
        class="container container--3 container--header max-width-66--sm c c-white"
    >
        <h1 class="ht--s--38-45">{{title}}</h1>
    </header>

    <div class="bgc-white c-grey--1000">
        <div class="container container--2">
            <p class="ht--s--28 c-theme-primary-dark">
                Cameron Evans and Raymond Blackwood
            </p>

            <p class="ht--s--20 c-theme-tertiary">
                During this recent Campus Management webinar, Cameron Evans,
                National and Chief Technology Officer, U.S. Education at
                Microsoft Corporation, and Raymond Todd Blackwood, Vice
                President of Product Management at Campus Management, answered
                questions from attending institutions.
            </p>

            <h2 class="b ht--p--18 c-grey--800 header--has-q-graphic">
                Given the state of funding, how are most business officers
                responding, whether it is raising tuition or other
                considerations?
            </h2>

            <p>
                <strong>Cameron Evans:</strong> When you look at it across the
                board, every university, every college, and every leader is
                tailoring financial strategies to their particular situations,
                their locations and constituencies, and strengths. There isn’t a
                one-size-fits-all approach to funding.
            </p>

            <p>
                But most universities are trying to make sure that students are
                successful within that first year. It’s simple math. If you have
                three freshman students and two of them leave in their sophomore
                year, the cost of teaching did not go down because the two
                students left. It just means that the costs now have to be
                lumped onto the remaining student. If you can help more students
                to actually sustain themselves throughout their academic
                careers, then we can see tuition costs actually flatten or at
                least stop increasing so dramatically.
            </p>

            <p>
                So having systems that enable you to track relationships far
                more effectively is a great way to bring costs down and improve
                outcomes at the same time. Colleges and universities are finding
                that their core missions of helping students succeed also helps
                keep costs from escalating.
            </p>

            <h2 class="b ht--p--18 c-grey--800 header--has-q-graphic">
                What about public funding? How is that tied to student outcomes
                and the current landscape in higher education?
            </h2>

            <p>
                <strong>Cameron Evans:</strong> It always depends on the
                institution. If it is a private institution, public funding is
                not the biggest issue on its radar. But for a public
                institution, the funding mix from the federal and the state
                government has changed dramatically and has had an impact. As a
                result, more courses are being offered online, and not always
                for the reasons people think.
            </p>

            <p>
                For example, at one institution it may be that students who live
                off campus are having greater difficulty affording the drive to
                school as resources contract. Having courses online and moving
                courses into the cloud provides a more cost-effective and
                expedient path for students to achieve their academic goals, as
                well as a more cost-effective academic platform for the
                institution. Institutions need to have both a micro and macro
                view of the dynamics shaping their environment, but sometimes
                this insight is buried in the data.
            </p>

            <p>
                <strong>Raymond Blackwood:</strong> In my home state of Florida,
                there is a new model for how schools get funding. Each
                institution is assigned a value based on performance over ten
                different metrics. Among the metrics they look at is the cost of
                undergraduate tuition. To piggyback on what Cameron said about
                retention for those first-year students, Florida actually looks
                at how many of your second-year students remain, and how many of
                those have a 2.0 GPA or higher. So outcomes matter when it comes
                to qualifying for public funding. I think 30 states now have
                different programs similar to this right now, and it is putting
                a lot of pressure on institutions to increase student success.
                At the same time, there is pressure on the administration to cut
                costs and find more efficient ways to operate.
            </p>

            <h2 class="b ht--p--18 c-grey--800 header--has-q-graphic">
                What impact on HR departments are you seeing at various
                colleges?
            </h2>

            <p>
                <strong>Raymond Blackwood:</strong> The Affordable Care Act has
                really put a strain on a lot of the HR departments, especially
                those that manage a high number of adjunct faculty, because they
                really stride that line of benefit eligibility. You have to
                remember, teachers do not teach all year long necessarily. You
                may have some online programs that go all year long, but in many
                cases, they may take 15 or 30 weeks. If your adjunct faculty
                member goes over that 30 hour limit, they are eligible for
                benefits.
            </p>

            <p>
                So we are seeing more and more cutting back on the number of
                credits per term institutions allow for adjunct faculty. And
                those faculty are responding by teaching at more institutions.
                So while they are having to pick up the cost of managing their
                own healthcare, they are also having to figure out how to
                balance teaching at multiple institutions.
            </p>

            <h2 class="b ht--p--18 c-grey--800 header--has-q-graphic">
                How are HR departments responding to increased concerns about
                information privacy, whether it is HIPAA or FERPA?
            </h2>

            <p>
                <strong>Cameron Evans:</strong> Privacy has become the new
                Achilles heel across higher education and has only escalated
                with recent disclosures of government and retail breaches and
                theft of citizens’ confidential information.
            </p>

            <p>
                So as we move more data into the Cloud and onto mobile devices
                to increase productivity and accessibility, the growing issue is
                how to keep this data safe.
            </p>

            <p>
                For FERPA and HIPAA, the laws themselves have not necessarily
                matured with the speed of technology, so it is incumbent upon
                technology companies that serve higher education to be ahead of
                policy in terms of how they approach data security. In fact,
                institutions should maintain custodial control of their data and
                ensure that no other nation can lay claim to their particular
                data assets.
            </p>

            <p>
                As we start integrating more systems, embracing “The Internet of
                Things,” and connecting different machines as a massive source
                of content for big data, we need to have a very fervent
                conversation about privacy. This is super important if you are
                moving to the Cloud. You need to find a provider that you can
                trust and make sure that they have their business practices
                publicly documented and available for you to walk through. You
                need a third-party audit to assure stakeholders and to know with
                any confidence that the business decisions you are making will
                not jeopardize their privacy.
            </p>

            <p>
                <strong>Raymond Blackwood:</strong> I know that Campus
                Management focuses a lot of its time and resources on making
                sure that the self-service capabilities in its solutions protect
                the privacy of the user, even as the institution needs to gather
                a lot of information, including medical information.
            </p>

            <p>
                So having multiple layers of authentication and being very
                diligent about access based on roles at the institution are
                critical to data security.
            </p>

            <p>
                A lot of schools, though, do not have the resources to establish
                roles and responsibilities the way it happens in the corporate
                world. What’s more, learning is about open sharing, free speech
                and openness, so any solutions provider to higher education has
                to address this tricky balance.
            </p>

            <h2 class="b ht--p--18 c-grey--800 header--has-q-graphic">
                How do Campus Management solutions adapt to the change that is
                happening in higher education?
            </h2>

            <p>
                <strong>Raymond Blackwood:</strong> I just read the recent
                Market Guide for Higher Education from the Gartner Group. The
                industry analyst observes that institutions are looking for
                flexible, rule-based, and configurable solutions that provide
                self-service capabilities without having to customize.
            </p>

            <p>
                They are looking for solutions that support flexible term
                structures like non-term, borrower-based, and competency-based
                programs, that strengthen ground programs as well, and that can
                be deployed modularly to keep up with the rate of change.
            </p>

            <p>
                That is really why we partnered with Microsoft. Their platform
                and workflow capabilities along with their Cloud strategy and BI
                tools are really suited to the needs of today’s institutions.
            </p>

            <p>
                <strong>Cameron Evans:</strong> Technology has to be a lot more
                flexible and adaptable to both people and the processes. What
                Campus Management is doing with CampusNexus and Microsoft
                Dynamics AX enables institutions to transform as change occurs.
            </p>

            <h2 class="b ht--p--18 c-grey--800 header--has-q-graphic">
                A lot of institutions are looking at international students and
                online learning as growth areas. What additional steps can
                institutions take to ensure data security internationally?
            </h2>

            <p>
                <strong>Cameron Evans:</strong> As more universities partner
                with other governments and ministries of education outside of
                the United States, privacy becomes an even greater issue. As we
                increase data services and systems for other countries and their
                employees, faculty, and students, we have to understand and
                address their data rules, policies, and legislation.
            </p>

            <p>
                I may have some federal requirements to make sure that data
                stays secure in the United States while another country like
                China says it doesn’t want any of its students’ data in America.
                This comes up more frequently than people realize. Fortunately,
                Microsoft does business around the world, so we can help higher
                education continue to grow internationally.
            </p>

            <p>
                <strong>Raymond Blackwood:</strong> Campus Management is a
                global higher education company and has implementations going on
                throughout the world, including current implementations of
                Microsoft Dynamics for institutions in Africa and Malaysia. So
                we are engaged in these data security issues on a daily basis
                and are aware of the types of data that can be exchanged from
                country to country.
            </p>

            <h2 class="b ht--p--18 c-grey--800 header--has-q-graphic">
                What are some lessons learned or recommendations?
            </h2>

            <p>
                <strong>Cameron Evans:</strong> We still have a lot of
                nontraditional students, including Gen-Xers and Boomers, who
                turn to higher education to find new skills through degree
                programs and continue to participate in society. Millennials
                come into higher education expecting technology to be super
                personalized and adaptive to them. We have to make sure that we
                are leveraging technology to serve the widest range of students
                and to help them optimize their time and academic experience.
            </p>

            <p>
                <strong>Raymond Blackwood:</strong> Cameron is absolutely right
                about the different generations of students, learning styles,
                and expectations today. To optimize capital investments and to
                serve diverse constituencies, institutions must get more
                departments involved in implementations earlier on. It’s no
                longer just a function of IT to solve one-off technology
                problems at an institution, but a cohesive enterprise strategy
                that supports a foundation for managing change and addressing
                student needs over time.
            </p>
        </div>
    </div>
</article>
{{/content}} {{#content "after-body"}} {{>[box-v3/combo--best-practice-7]}}
{{>[pattern-box/consultation-request]}} {{/content}} {{/extend}}
