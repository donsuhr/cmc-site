---
# <!-- @formatter:off -->
title: Seven Steps to Successful Data Migration
in-nav--en-us: false
in-nav--en-in: false
in-nav--en-uk: false
page-bg-image: /images/pages/higher-education-resources/insights-bkg1.jpg
# <!-- @formatter:on -->
---

{{#extend "hb-layouts/v3"}} {{#content "body"}}
<article>
    <header
        class="container container--3 container--header max-width-66--sm c c-white"
    >
        <h1 class="ht--s--38-45">{{title}}</h1>
    </header>

    <div class="bgc-white c-grey--1000">
        <div class="container container--2">
            <p class="ht--s--28 c-theme-primary-dark">
                Excerpted from Campus Management’s CRM best practices series
            </p>

            <p>
                As part of Campus Management’s CRM best practices webinar
                series, strategic consultants Adam Hopkins and Genise Schuette
                provided guidance for ensuring successful data migration and
                integration between platforms and systems. Whether you are
                replacing a legacy system, upgrading databases, establishing a
                new data warehouse, preparing for disaster recovery, reducing
                storage footprints, or merging new data from another source,
                here are the seven steps to ensuring a successful migration.
            </p>

            <h2 class="ht--s--18 c-grey--800 mb0-5">
                Step 1: Identify the data format, location, and sensitivity
            </h2>

            <p>
                The first step in the data migration process is taking stock of
                where you are today with your data. Take a good hard look at
                what you’re migrating. Is there information you’re storing that
                is obsolete and can be excluded in your migration? What format
                is your data in? Where is your data currently stored? Is there
                regulated data that requires security controls and specific
                access management? Determine who is using the data now, who will
                use it in the future, and how it will be used.
            </p>

            <h2 class="ht--s--18 c-grey--800 mb0-5">
                Step 2: Choose a method of tracking your findings
            </h2>

            <p>
                The point here is documentation. It’s not the format that
                counts. It’s the fact that you do it. So whether it’s a
                spreadsheet, a whiteboard, or a good old-fashioned notebook, the
                point is that you need to keep track of what you have found and
                where you are in the process. We all start projects thinking
                “I’m going to remember this. I don’t need to write it down.”
                Keep good notes so that you can keep yourself on track and
                report to others where you are in the process.
            </p>

            <h2 class="ht--s--18 c-grey--800 mb0-5">
                Step 3: Plan for the size and scope of the project
            </h2>

            <p>
                Set up a data migration plan that outlines each step considering
                who will be affected, what the downtime will be, potential
                technical or compatibility issues, how to maintain data
                integrity, and how to protect that data during the migration.
                You will want to make sure that everyone is on the same page
                with goals and objectives and a clearly defined scope of work.
                The project manager must keep everyone up to date with a solid
                communication plan.
            </p>

            <p>
                What resources will you need to accomplish this task? Are there
                budget restrictions? Is there anything new to the institution
                that can disrupt business as usual? Ensure that you have a solid
                training plan and schedule in place to bring all employees up to
                speed with the data management processes you have defined. To
                minimize the amount of downtime and errors, make sure everyone
                impacted by the migration stays informed throughout the project.
            </p>

            <h2 class="ht--s--18 c-grey--800 mb0-5">
                Step 4: Write out a flexible timeline
            </h2>

            <p>
                Any time you talk about a timeline, people get nervous. So it’s
                important to emphasize that this is a flexible timeline. Ask
                yourself questions like “will the migration interfere with
                normal business operations?” “Will it require downtime?”
            </p>

            <p>
                One very important aspect of any project is keeping your
                stakeholders informed. Communicate with them about the timeline.
                Be open to discussion and questions. Be mindful of schedules and
                priorities, backup and replication settings, capacity planning,
                and prioritizing. The important thing to remember when creating
                a timeline is be realistic. It’s important for your team to feel
                successful and to maintain positivity even with challenging
                projects, so make your goals and steps attainable. It will help
                to keep everyone on track and make your migration project
                successful.
            </p>

            <h2 class="ht--s--18 c-grey--800 mb0-5">
                Step 5: Assess staff
            </h2>

            <p>
                Getting the right team in place for a project of any size is
                essential, so determine if your team has the knowledge and
                skills necessary to accomplish the project or if you need to
                reach out to an expert. This can go back to step three:
                identifying resources and budget restrictions.
            </p>

            <p>
                Do you and your team members have time to tackle the project?
                Are there ways of freeing up more of their time to dedicate to
                the project? All of us can relate to the issue of being assigned
                a large project and not having enough time for our other tasks.
                You may not be able to bring in reinforcements, but you might be
                able to help redistribute workloads to accommodate for the extra
                time this project might require.
            </p>

            <p>
                In addition to time, you want to make sure that you have all
                stakeholders represented. And this means you may need someone
                from each department that relies on the data to be involved in
                the decision making. Your end-users may have anxiety over the
                success of the migration project. If you treat the data
                migration project as a business process instead of simply a set
                of technical steps and involve your end users, you’ll have a
                more successful data migration project in less time and at a
                lower cost.
            </p>

            <h2 class="ht--s--18 c-grey--800 mb0-5">
                Step 6: Make sure your data is clean
            </h2>

            <p>
                You don't want to push bad data into a new system, so you need
                to complete an analysis on your databases before moving forward
                with the migration. Even if you’ve done a pretty good job of
                keeping your data clean, you will still benefit from taking the
                time to do a data cleanout before you migrate. If you’re
                familiar with your data, you’ll know what to look for, including
                any anomalies or inconsistencies. Take advantage of those
                queries that you run to look for records that are clearly
                incorrect or inconsistent.
            </p>

            <p>
                You can enlist the help of others to help look for errors and
                correct the inaccuracies. You may want to delete obsolete files,
                abandoned email accounts, and outdated user accounts. Another
                important thing to check for is duplicate records. When you
                migrate with clean data, you can be confident that you’re
                starting in your new system with accurate data.
            </p>

            <p>
                And most importantly, don’t migrate bad habits. As you’re doing
                your data cleanup, you might come across some bad data that is
                the result of bad habits. Make sure you identify and correct the
                problem before you migrate.
            </p>

            <h2 class="ht--s--18 c-grey--800 mb0-5">
                Step 7: Back up all data
            </h2>

            <p>
                This is kind of a no-brainer. You want to make sure that backing
                up all the data takes place before you migrate. Leverage all
                your resources to make sure that this is done correctly and
                thoroughly. No one wants to migrate data and have issues arise
                because a data backup wasn’t performed or was done incorrectly.
            </p>

            <p>
                Whether you are migrating data to a new
                <a href="/products/crm-for-higher-education/">CRM</a>,
                <a href="/products/student-information-system/">SIS</a>, ERP or
                other core system at your institution, these first steps can
                help ensure the data integrity of the new system as well
                maximize the potential of the new solution for improving
                workflows, insight, and results.
            </p>
        </div>
    </div>

    <div class="bgc-grey--100">
        <div class="container container--2">
            <div class="c flexcol">
                <h3 class="ht--s--22-28 c-theme-primary-dark mb2--sm">
                    View the On-Demand Webinar "Seven Steps to Successful Data
                    Migration"
                </h3>
                <a
                    class="pill-button c block bottom"
                    href="https://info.campusmanagement.com/7-steps-to-successful-data-migration"
                    rel="noopener"
                    target="_blank"
                    >View Webinar</a
                >
            </div>
        </div>
    </div>
</article>
{{/content}} {{#content "after-body"}} {{>[box-v3/combo--best-practice-7]}}
{{>[pattern-box/consultation-request]}} {{/content}} {{/extend}}
