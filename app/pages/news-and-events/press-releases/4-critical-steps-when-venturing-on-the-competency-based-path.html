---
# <!-- @formatter:off -->
title: 4 critical steps when venturing on the competency-based path - eCampusNews
publish: 2015-12-01
# <!-- @formatter:on -->
---

{{#extend "hb-layouts/v3"}}
{{#content "body"}}
<article class="container c-grey--1000">

    <h1 class="ht--s--38-45 c-theme-primary-dark ">{{title}}</h1>

    <p class="b  c-grey--800 ">A CBE early adopter talks first steps in creating a basic competency-based
        framework.</p>

    <p><span class="press-release__date-location">By: Karli Grant, M.Ed., M.S.T.M., December 1, 2015</span> - If you go
        to the history section of Rasmussen College’s website, you will see sepia-toned images of male and female
        students behind bulky desks and teachers at chalkboards. It was the early 1900s and the college was ahead of its
        time in recognizing the need for more accounting and business professionals in the new century. It may not have
        been called competency-based education (CBE), but its guiding principle was the same, to help students develop
        practical and demonstrable skills that align with the needs of employers. Over a century later, CBE is now the
        hot topic in higher education, and Rasmussen is among a network of colleges working to address shared challenges
        to designing, developing and scaling competency-based degree programs.</p>

    <p>Today, factors beyond skill deficits are driving the demand for competency-based education. Traditional and
        nontraditional students alike are looking for faster, more flexible, and more cost effective ways to achieve
        their academic goals. At the same time, institutions facing declining enrollments and retention rates are
        looking at CBE as a way to attract more students and keep them on track for graduation.</p>

    <p>Institutions with a focus on mentoring, apprentice-based and vocational learning have embraced the CBE model
        since its inception, but many schools with a variety of academic programs are now exploring the possibility of
        offering competency-based courses. For the many colleges and universities currently testing the waters, the
        question is “where do we start?” Early-adopters like Rasmussen College are partnering with regional accreditors
        and the U.S. Department of Education as best practices begin to emerge, but in the meantime there are steps
        every institution can take to create a basic framework for CBE.</p>

    <h2 class="b  c-grey--800 ">Step 1: Identify programs that are best suited for CBE</h2>

    <p>Institutions should look for areas that allow students with real-world experience to be assessed for
        competencies. Many adult students are looking for formal validation of skills they’ve already acquired.
        Business, medical and allied health programs, technology, and graphic design are among the most common fields
        selected for competency-based study, but any programs that allow students to demonstrate mastery of skills are
        good candidates.</p>

    <p>While these more hands-on programs are popular options for CBE curriculum, the potential exists to transform
        traditional liberal arts courses into CBE offerings. Courses focused on analytical thinking, effective written
        and verbal communication skills, providing constructive feedback, and entrepreneurial and innovative thinking
        can be transformed into effective competency-based learning opportunities.</p>

    <p>When Rasmussen first decided to move forward with CBE, it established a rubric for identifying suitable programs,
        one that asked:</p>

    <ul class="list--normal list--has-lead-in">
        <li>Are there already established programs out there that people might recognize, that provide a built-in level
            of awareness and validation?
        </li>
        <li>Are there areas that might differentiate the college and serve as a competitive advantage?</li>
        <li>What types of students might be involved in these programs? Is it a mature student that might be more
            independent or a newer student who might need more support and guidance?
        </li>
        <li>From an operational perspective, what would allow us to experiment on a small scale initially, and then
            scale if we want to broaden to more students and programs?
        </li>
    </ul>

    <p>Based on this rubric, Rasmussen started with general education requirements within its business school, which
        then were extended to general education requirements within other programs. Today, Rasmussen College’s Flex
        Choice competency-based option is offered in a number of associate’s and bachelor’s completer programs,
        including business, nursing, computer science, criminal justice, and more.
        Institutions should also look at programs that already have a wealth of learning materials (e.g., e-textbooks,
        multimedia and digital assets in the public domain), and open education resources that are easily accessible and
        convenient to students. Part of the value of competency-based programs is that they save students and
        institutions time and money by incorporating existing channels of learning. MOOCs, for example, are often
        included in CBE programs.</p>

    <h2 class="b  c-grey--800 ">Step 2: Build Your Assessment Framework</h2>

    <p>There is no national standard or model for CBE assessments, so institutions need to determine the right types of
        rubrics to demonstrate skill mastery for the CBE programs being considered. A look at current CBE programs
        reveals very different approaches to competency-based assessment, with options that include performance-based,
        portfolio-based, or project-based assessments as well as multiple-choice exams.</p>


    <p>A competency-based framework offers an institution the opportunity to redesign not only its curriculum, but its
        traditional teaching roles as well. To accomplish this, though, faculty should be consulted early in the
        development of programs and assessment standards. To provide the greatest innovation in CBE programs,
        institutions should consider leveraging multiple types and modes of instruction, multimedia learning materials,
        and mentoring by subject-matter experts. Additionally, assessments may be conducted by expert evaluators whose
        sole focus is to grade assignments.</p>

    <p>A successful CBE program will also develop assessment tools with the help of employers in the community.
        Rasmussen works with an advisory board of employers from the community who would potentially hire its graduates.
        The board helps design assessments and programs to make sure that curricula are aligned with employer needs.
        From there, the college works with an instructional designer to create scenarios and projects that enable
        students to demonstrate those skills and abilities.</p>

    <p>Professional organizations and associations are also a valuable resource for input on designing courses and
        programs that meet industry and accrediting standards. One piece of advice institutions will often hear is to
        focus on a singular or small number of programs to launch within the CBE model, then evaluate what is working
        and what is not, reformulate as needed, and then continue to expand the curriculum to include more CBE offerings
        where appropriate.</p>

    <h2 class="b  c-grey--800 ">Step 3: Determine the right pace and price</h2>

    <p>The main objective of CBE is to allow students to move more quickly and cost-effectively through the courses and
        concepts they understand (demonstrating proficiency or testing out of the course), so they can spend more time
        on subjects and skills for which they require academic guidance.</p>

    <p>Most current CBE programs enable students to pay either course-by-course or through a subscription model.
        However, some programs have found that the subscription model provides additional value by giving students a
        time-based goal to complete objectives. Typical subscription periods are often three to six months in length,
        allowing students to master one or many skills during that timeframe.</p>

    <p>Rasmussen’s Flex Choice program is a hybrid model that incorporates self-paced competency-based courses and
        traditional faculty-led courses into a single program. In this model, students can complete some credits at no
        additional cost via self-paced courses that allow students to move quickly through concepts they understand or
        slow down and receive support to learn new material. Their remaining credits are completed in traditional
        faculty-led courses.</p>

    <h2 class="b  c-grey--800 ">Step 4: Pick the right technology and service provider</h2>

    <p>While more institutions now embrace CBE as a viable complement to traditional learning in principle, the
        technology in place at most universities isn’t designed for it. A recent report by the Innosight Institute*
        observes that most student information systems are hardwired for traditional academic terms and have to be
        retrofitted for individualized learning models and borrower-based financial aid. Moreover, many CRM systems are
        focused on recruitment and admission versus student engagement and success. For institutions to truly support
        CBE academic programs, they require a comprehensive student information, financial aid, and CRM solution
        designed for flexibility.</p>

    <p>The chosen technology platform should include, at a minimum, the ability to:</p>

    <ul class="list--normal list--has-lead-in">
        <li>Offer academic programs in a variety of academic calendars</li>
        <li>Package financial aid for individualized programs based on Title IV regulations</li>
        <li>Track student progress, from skill assessments to unique risk factors</li>
        <li>Provide an early-warning system that records student objectives and then flags an advisor automatically to
            check in with the student if he or she misses deadlines
        </li>
        <li>Scale easily as new programs and campuses are added</li>
    </ul>

    <p>How painless and cost effective it is to achieve this basic level of functionality for CBE will depend on the
        flexibility and extensibility of the technology in place.</p>

    <h2 class="b  c-grey--800 ">Moving Forward</h2>

    <p>Competency-based education is rapidly evolving, with new ideas for the model emerging. Some institutions are
        understandably cautious about moving forward until there is clearer direction from the U.S. Department of
        Education and regional and programmatic accrediting bodies. The good news is that support for the model is
        growing. Organizations and initiatives such as the Lumina Foundation and the Competency-Based Network are
        assessing the CBE landscape in higher education and creating an innovative network for institutions to develop
        CBE standards and best practices. Additionally, in early November, the Department of Education announced the
        expansion of the Experimental Sites Initiative and invited institutions to participate in CBE programs through a
        new set of waivers for Federal Title IV aid and Satisfactory Academic Progress. By taking the four basic steps
        now, your institution can be that much more ahead of the game as the model and shape of CBE matures and begins
        to accelerate across higher education.</p>

    <p>*Staker, Heather. Senior Fellow. Innosight Institute. The Engine Behind WGU: Configuration of a competency-based
        information system. U.S. Department of Education experimental sites information: <a
            href="https://experimentalsites.ed.gov/">https://experimentalsites.ed.gov/</a></p>

    <p>Karli Grant is the Product Marketing Manager at Campus Management. In this role, Ms. Grant leads the launching of
        new products to the market, conducts market analysis, plans strategies to inform the market and drive adoption
        of Campus Management’s products and services portfolio, and acts of the voice of the client to internal
        stakeholders.</p>

</article>
{{/content}}

{{#content "after-body"}}
{{>[pattern-box/consultation-request]}}
{{/content}}

{{/extend}}
