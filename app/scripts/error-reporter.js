import config from 'config';
import queryString from 'query-string';
import 'error-tojson';
import { cloneNavigator } from '../components/cloneNavigator';

const isAsmCaDotCom = /Firefox\/23\.0/.test(navigator.userAgent);
const isTestMode = !!queryString.parse(document.location.search).e2eTest;
let errorCounter = 0;

function trackError(event) {
    const {
        message,
        filename: source,
        lineno: line,
        colno: col,
        error,
    } = event;

    errorCounter += 1;
    if (errorCounter > 2) {
        return;
    }

    const body = JSON.stringify({
        referrer: document.referrer,
        locationHref: window.location.href,
        source,
        line,
        col,
        error,
        message,
        navigator: cloneNavigator(),
    });

    window.consoleHistory.push('window.onerror', body);

    const hasSource = !!source;
    const sourceIsCMC = hasSource && source.includes(config.domains.cmc);

    // possibly Buyhatke
    const isPluginError = error.message === 'this.remove is not a function'
        && line === 12
        && col === 14;

    if (
        process.env.NODE_ENV === 'production'
        && !isTestMode
        && !isAsmCaDotCom
        && sourceIsCMC
        && !isPluginError
    ) {
        fetch(`${config.domains.api}/log-error/js/`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body,
        });
    }
}

export { trackError };
