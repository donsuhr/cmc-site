/* eslint no-underscore-dangle: ["error", { "allow": ["__e"] }] */
/* global gNextPath gPrevPath gFirstPath */
import 'core-js/modules/es.promise'; // ie11
import 'core-js/modules/es.array.iterator'; // ie11
import 'whatwg-fetch';
import queryString from 'query-string';
import LazyLoad from 'vanilla-lazyload';
import menu from '../components/menu/menu';
import socialButtons from '../components/social-media-share-buttons';
import smoothScroll from '../components/smoothScroll';
import headroom from '../components/menu/monkey-patch-headroom';
import { api as privacyNotificationApi } from '../components/privacy-update-notice';
// import '../components/web-vitals';

import(
    /* webpackChunkName: "modals" */
    './modals'
);

if (/wd=/.test(window.location.href)) {
    throw new Error();
}

const isTestMode = !!queryString.parse(document.location.search).e2eTest;

window.consoleHistory = [];

if (isTestMode) {
    // eslint-disable-next-line no-console
    const origLog = console.log;
    // eslint-disable-next-line no-console
    console.log = function log(...args) {
        // eslint-disable-line no-console
        // eslint-disable-line no-console
        window.consoleHistory.push(args);
        origLog.apply(window.console, args);
    };
    // eslint-disable-next-line no-console
    console.log('running in e2e test mode');
}

setTimeout(() => {
    // break up long task
    const menuInstance = menu.init();
    //headroom.init(menuInstance);
    setTimeout(() => {
        smoothScroll.init();
        socialButtons.init();
        // liveChat.init({ isAsmCaDotCom, isTestMode });
        // eslint-disable-next-line no-new
        new LazyLoad({
            elements_selector: '.lazyload',
            class_loading: '.lazyload--loading',
        });
    }, 1);
}, 1);

/* ********** */

function callTrackError(event) {
    // prettier-ignore
    import(/* webpackChunkName: "error-reporter-chunk" */ './error-reporter')
        .then(({ trackError }) => trackError(event));
}

window.addEventListener('error', (event) => {
    callTrackError(event);
});
if (window.__e && window.__e.q && window.__e.q.length) {
    window.__e.q.forEach((event) => {
        callTrackError(event);
    });
}

/* ********** */

document.addEventListener('keydown', (event) => {
    if (event.ctrlKey) {
        if (event.key === 'N') {
            document.location = gNextPath;
        }
        if (event.key === 'P') {
            document.location = gPrevPath;
        }
        if (event.key === 'F') {
            document.location = gFirstPath;
        }
    }
});

/* ********** */

privacyNotificationApi.init({
    el: document.querySelector('.location-notification'),
});
