import $ from 'jquery';
import formSerialize from 'form-serialize';
import Basil from 'basil.js';
import form from '../../components/form';

const $el = $('.form-wall-form');
const formDomID = $el.attr('id');
const successRedirectUrl = false;
const leadSource = $el.data('lead-source');
const endpoint = $el.attr('action');
const postToUnify = !!$el.data('post-to-unify');
const basil = new Basil();

const formData = basil.get('webcast-form') || {};
// rehydrate before createForm, which will use garlic
Object.keys(formData).forEach((key) => {
    const skip = [
        'webcast',
        'hsFormId',
        'hsPortalId',
        'pageUrl',
        'pageTitle',
        'hutk',
    ];
    if (!skip.includes(key)) {
        $el.find(`[name="${key}"]`).val(formData[key]);
    }
});

if ($el.find('[name="pageUrl"]').length) {
    $el.find('[name="pageUrl"]').val(window.location.href);
}
if ($el.find('[name="pageTitle"]').length) {
    $el.find('[name="pageTitle"]').val(document.title);
}
if ($el.find('[name="hutk"]').length) {
    $el.find('[name="hutk"]').val(Basil.cookie.get('hubspotutk'));
}

form.createForm({
    leadSource,
    successRedirectUrl,
    formDomID,
    endpoint,
    postToUnify,
    cb: (error) => {
        if (error) {
            console.log(error); // eslint-disable-line no-console
        }
        $('.form-wall-video').css({ display: 'block' });
        $('.form-wall-form-wrapper').css({ display: 'none' });
        const data = formSerialize($el.get(0), {
            hash: true,
            empty: false,
            disabled: true,
        });
        basil.set('webcast-form', data, {
            sameSite: 'Strict',
            secure: true,
        });
    },
});
