import $ from 'jquery';
import api from '../../components/s3-doc-list';
import template from '../../components/s3-doc-list/templates/appView-v2.hbs';
import itemTemplate from '../../components/s3-doc-list/templates/listItemView-v2.hbs';

const $el = $('#s3docListApp');

api.init({
    $el,
    site: $el.data('site'),
    group: $el.data('group'),
    docType: $el.data('doctype'),
    itemsPerPage: $el.data('items-per-page'),
    template,
    itemTemplate,
});
