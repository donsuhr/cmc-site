import {
    getLang,
    eventEmitter as menuLangEmitter,
    EVENT_LANG_CHANGED,
} from '../../components/menu/menu-lang-filter';

const limit = 5;

function filterByLang(lang) {
    const menulangCaps = lang
        .split('-')
        .map((x) => x[0].toUpperCase() + x.slice(1))
        .join('');

    let visibleCounter = 0;

    Array.from(document.querySelectorAll('.recent-news-list-item')).forEach(
        (el) => {
            const hasLang = el && el.dataset[`inNav-${menulangCaps}`] !== undefined;
            el.hidden = !hasLang || visibleCounter >= limit;
            if (hasLang) {
                visibleCounter += 1;
            }
        },
    );
}

getLang().then((lang) => {
    filterByLang(lang);
});
menuLangEmitter.on(EVENT_LANG_CHANGED, filterByLang);
