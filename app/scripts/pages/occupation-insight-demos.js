import $ from 'jquery';
import smoothScroll from '../../components/smoothScroll';

try {
    window.history.scrollRestoration = 'manual';
} catch (e) {
    // noop
}

const watchDemoVideoLink = document.getElementById('WatchDemoVideoLink');
if (watchDemoVideoLink) {
    watchDemoVideoLink.addEventListener('click', (event) => {
        event.preventDefault();
        const div = document.getElementById('DemoVideo');
        div.style.maxHeight = '750px';
        const img = document.getElementById('DemoVideoImg');
        img.src = img.getAttribute('data-src');
    });
}

const closeDemoVideoButton = document.getElementById('CloseDemoVideoButton');
if (closeDemoVideoButton) {
    closeDemoVideoButton.addEventListener('click', (event) => {
        event.preventDefault();
        const div = document.getElementById('DemoVideo');
        div.style.maxHeight = '0px';
        smoothScroll.scrollTo($(watchDemoVideoLink), null, 200);
    });
}

const demoProgramLink = document.getElementsByClassName(
    'cmc-article__link-list-item--hs-cta--program',
)[0];

function showProgramDemo() {
    const div = document.getElementById('DemoProgram');
    const otherDiv = document.getElementById('DemoCareer');
    const iframe = document.getElementById('DemoProgramIframe');
    if (iframe.getAttribute('src') === '') {
        iframe.src = 'https://app.powerbi.com/view?r=eyJrIjoiZDUxZWY5MTYtMjAwZi00YzRmLWEzZmEtMmQ5NWJiNDQ4NWQwIiwidCI6ImEzOGZmODcwLTljODYtNDNkZi05ZTZiLTZiMDFmMjA2YTM5OCIsImMiOjN9';
    }
    const open = () => {
        div.style.maxHeight = '950px';
        smoothScroll.scrollTo($(div));
    };
    if (otherDiv.style.maxHeight && otherDiv.style.maxHeight !== '0px') {
        setTimeout(() => {
            open();
        }, 550);
    } else {
        open();
    }
    otherDiv.style.maxHeight = '0px';
}

// demoProgramLink.addEventListener('click', (event) => {
//     event.preventDefault();
//     showProgramDemo();
// });

const demoCareerLink = document.getElementsByClassName(
    'cmc-article__link-list-item--hs-cta--career',
)[0];

function showCareerDemo() {
    const div = document.getElementById('DemoCareer');
    div.style.maxHeight = '950px';
    const iframe = document.getElementById('DemoCareerIframe');
    if (iframe.getAttribute('src') === '') {
        iframe.src = 'https://app.powerbi.com/view?r=eyJrIjoiMjFiMDNjMGMtNzA5YS00ZTQwLTk1YjMtNTdkMDIwZDU0Y2RmIiwidCI6ImEzOGZmODcwLTljODYtNDNkZi05ZTZiLTZiMDFmMjA2YTM5OCIsImMiOjN9';
    }
    const otherDiv = document.getElementById('DemoProgram');
    otherDiv.style.maxHeight = '0px';
}

// demoCareerLink.addEventListener('click', (event) => {
//     event.preventDefault();
//     showCareerDemo();
// });

const closeDemoProgramButton = document.getElementById(
    'CloseDemoProgramButton',
);
if (closeDemoProgramButton) {
    closeDemoProgramButton.addEventListener('click', (event) => {
        event.preventDefault();
        const div = document.getElementById('DemoProgram');
        div.style.maxHeight = '0px';
        smoothScroll.scrollTo($(demoProgramLink), null, 500);
    });
}

const closeDemoCareerButton = document.getElementById('CloseDemoCareerButton');
if (closeDemoCareerButton) {
    closeDemoCareerButton.addEventListener('click', (event) => {
        event.preventDefault();
        const div = document.getElementById('DemoCareer');
        div.style.maxHeight = '0px';
        smoothScroll.scrollTo($(demoCareerLink), null, 500);
    });
}

if (window.location.hash) {
    let div;
    if (window.location.hash === '#DemoCareer') {
        showCareerDemo();
        div = document.getElementById('DemoCareer');
    }
    if (window.location.hash === '#DemoProgram') {
        showProgramDemo();
        div = document.getElementById('DemoProgram');
    }
    if (div) {
        setTimeout(() => {
            smoothScroll.scrollTo($(div));
        }, 550);
    }
}
