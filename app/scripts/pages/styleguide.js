import $ from 'jquery';
// eslint-disable-next-line no-unused-vars
import transition from 'bootstrap-sass/assets/javascripts/bootstrap/transition';
// eslint-disable-next-line no-unused-vars
import scrollspy from 'bootstrap-sass/assets/javascripts/bootstrap/scrollspy';
// eslint-disable-next-line no-unused-vars
import affix from 'bootstrap-sass/assets/javascripts/bootstrap/affix';

$('.sg-nav-affix').affix({
    offset: {
        top: 260,
    },
});
$('body').scrollspy({ target: '.sg-nav' });
