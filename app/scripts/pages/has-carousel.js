import $ from 'jquery';
import 'bootstrap-sass/assets/javascripts/bootstrap/transition';
import 'bootstrap-sass/assets/javascripts/bootstrap/carousel';

const carouselElement = $('.carousel');

carouselElement.carousel({
    interval: carouselElement.data('interval'),
});
