import $ from 'jquery';
import api from '../../components/admin-xls';

const $el = $('.password-protected-download');

api().init({
    $el,
    site: $el.data('site'),
    group: $el.data('group'),
});
