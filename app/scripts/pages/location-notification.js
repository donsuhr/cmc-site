import { api as notificationApi } from '../../components/location-notice';

notificationApi.init({ el: document.querySelector('.location-notification') });
