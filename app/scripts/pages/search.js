import search, {
    getIndexFromDom,
    getQueryFromSearch,
} from '../../components/search-results';

search(getIndexFromDom(), getQueryFromSearch(window.location.search));
