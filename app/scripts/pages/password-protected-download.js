import $ from 'jquery';
import api from '../../components/password-protected-download';

const $el = $('.password-protected-download');

api().init({
    $el,
    site: $el.data('site'),
    group: $el.data('group'),
    docType: $el.data('doctype'),
});
