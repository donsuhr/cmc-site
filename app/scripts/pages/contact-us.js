/* eslint-disable no-underscore-dangle */

import $ from 'jquery';
import config from 'config';
import queryString from 'query-string';
import form from '../../components/form';
import googleMaps from '../../components/google-maps--contact-us';

googleMaps.init();

const $el = $('#requestDemoForm');
const endpoint = $el.attr('action');
const successRedirectUrl = $el.data('success-redirect-url') || '/contact-us/contact-complete/';
const postToUnify = !!$el.data('post-to-unify');

const _form = form.createForm({
    leadSource: 'contact us form',
    successRedirectUrl,
    formDomID: 'requestDemoForm',
    endpoint,
    postToUnify,
    formalizerOptions: {
        id: config.leadLanderID,
        pairs: {
            // llname : existing element name
            email: 'email',
            firstName: 'firstName',
            lastName: 'lastName',
            phone: 'phone',
            company: 'institution',
        },
    },
});

const _$el = _form.getEl();

function updateHiddenFields() {
    const parsed = queryString.parse(window.location.search);
    const { reason } = parsed;
    if (reason) {
        const Reason = reason.charAt(0).toUpperCase() + reason.slice(1);
        _$el.find('[name^="reasonForInquiry"]').val(Reason);
        _$el.find('[name^="Motivo de la consulta"]').val(Reason);
    }
}

updateHiddenFields();

const reasonInput = document.getElementById('reasonForInquiry');
const solutionLi = document.getElementById('SolutionLi');
const solutionInput = document.getElementById('solution');

function checkForSales() {
    const selectedValue = reasonInput.selectedIndex > 0
        && reasonInput.options[reasonInput.selectedIndex].value;
    if (selectedValue === 'Sales') {
        solutionLi.hidden = false;
        solutionInput.required = true;
    } else {
        solutionLi.hidden = true;
        solutionInput.required = false;
    }
}

checkForSales();

reasonInput.addEventListener('change', () => {
    checkForSales();
});
