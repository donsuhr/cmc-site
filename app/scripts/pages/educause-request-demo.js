/* eslint-disable no-underscore-dangle */

import $ from 'jquery';
import form from '../../components/form';

const $el = $('#requestDemoForm');
const formDomID = $el.attr('id');
const endpoint = $el.attr('action');
const postToUnify = !!$el.data('post-to-unify');
const successRedirectUrl = $el.data('success-redirect-url') || '/contact-us/contact-complete/';

const _form = form.createForm({
    leadSource: 'educause request demo',
    successRedirectUrl,
    formDomID,
    endpoint,
    postToUnify,
});

const _$el = _form.getEl();

const cascadeData = {
    'Wednesday, October 28': [
        '9:30am -10:00am',
        '10:00am - 10:30am',
        '10:30am - 11:00am',
        '11:00am - 11:30am',
        '11:30am - 12:00pm',
        '12:00pm - 12:30pm',
        '12:30pm - 1:00pm',
        '1:00pm - 1:30pm',
        '1:30pm - 2:00pm',
        '2:00pm - 2:30pm',
        '2:30pm - 3:00pm',
        '3:00pm - 3:30pm',
        '3:30pm - 4:00pm',
        '4:00pm - 4:30pm',
        '4:30pm - 5:00pm',
        '5:00pm - 5:30pm',
        '5:30pm - 6:00pm',
        '6:00pm - 6:30pm',
    ],
    'Thursday, October 29': [
        '10:00am - 10:30am',
        '10:30am - 11:00am',
        '11:00am - 11:30am',
        '11:30am - 12:00pm',
        '12:00pm - 12:30pm',
        '12:30pm - 1:00pm',
        '1:00pm - 1:30pm',
        '1:30pm - 2:00pm',
        '2:00pm - 2:30pm',
        '2:30pm - 3:00pm',
        '3:00pm - 3:30pm',
        '3:30pm - 4:00pm',
        '4:00pm - 4:30pm',
    ],
};

function populateSelect(val) {
    const selectOptions = cascadeData[val];
    const $targetSelect = _$el.find('#preferred-time');
    $targetSelect.empty();
    $targetSelect.append($('<option value="">Select Time</option>'));
    $.each(selectOptions, (index, value) => {
        $targetSelect.append(
            $('<option></option>')
                .attr('value', value)
                .text(value),
        );
    });
}

_$el.find('#preferred-day').on('change', (event) => {
    const $select = $(event.currentTarget);
    populateSelect($select.val());
});
