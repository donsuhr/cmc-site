import $ from 'jquery';
import Basil from 'basil.js';

const basil = new Basil();

function checkStorage() {
    const hasAccess = basil.get('product-center') === true;
    $('.product-center-login--has-cookie').css(
        'display',
        hasAccess ? 'block' : 'none',
    );
    $('.product-center-login--no-cookie').css(
        'display',
        hasAccess ? 'none' : 'block',
    );
}

checkStorage();
