import $ from 'jquery';
import api from '../../components/s3-doc-list';
import template from '../../components/webinar-series/appView.hbs';
import itemTemplate from '../../components/webinar-series/listItemView--on-demand.hbs';
import Model from '../../components/webinar-series/model';
import ListCollectionClass from '../../components/webinar-series/listCollection';

const $el = $('#WebinarSeries--upcoming');

api.init({
    $el,
    site: $el.data('site'),
    group: $el.data('group'),
    docType: $el.data('doctype'),
    itemsPerPage: $el.data('items-per-page'),
    type: $el.data('type'),
    template,
    itemTemplate,
    model: Model,
    history: false,
    ListCollectionClass,
    filterMenuOptions: [
        {
            field: 'products',
            text: 'Select Topic',
            itemFnName: 'getUniqueProducts',
        },
    ],
});
