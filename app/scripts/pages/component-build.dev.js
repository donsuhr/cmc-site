import prettier from 'prettier/standalone'; // eslint-disable-line import/no-extraneous-dependencies
import parserHtml from 'prettier/parser-html'; // eslint-disable-line import/no-extraneous-dependencies

import 'garlicjs';
import $ from 'jquery';
import debounce from 'lodash/debounce';

import {
    getBoxType,
    setColorBoxValue,
} from '../../components/component-builder/utils';

import { build as buildHeader } from '../../components/component-builder/header';
import { build as buildFullWidth } from '../../components/component-builder/fullWidth';
import { build as buildBoxV4 } from '../../components/component-builder/box-v4';
import { build as buildSectionTitle } from '../../components/component-builder/sectionTitle';

const buildFormEl = document.querySelector('#BuildForm');
const outputEl = document.querySelector('#Output');
const previewEl = document.querySelector('#Preview');
const presetEl = document.querySelector('#BoxV4ContainerPreset');

$('#BuildForm').garlic();

function updateVisibleFieldsets() {
    const type = getBoxType();
    const fieldsets = document.querySelectorAll(
        '#BuildForm fieldset[class^=fieldset]',
    );
    fieldsets.forEach((x) => {
        const [, className] = x.getAttribute('class').match(/fieldset--(.*)$/);
        x.hidden = className !== type;
    });
}

function build() {
    updateVisibleFieldsets();
    const type = getBoxType();
    const map = {
        header: buildHeader,
        'box-v4': buildBoxV4,
        'section-title': buildSectionTitle,
        'full-width': buildFullWidth,
    };
    const ret = map[type]();

    const clone = ret.cloneNode(true);
    clone.querySelectorAll('.remove').forEach((x) => {
        x.parentNode.removeChild(x);
    });
    const formatted = prettier.format(clone.innerHTML, {
        singleQuote: true,
        trailingComma: 'all',
        tabWidth: 4,
        parser: 'html',
        plugins: [parserHtml],
    });
    const replaced = formatted
        .replace(/inline-img-dims=""/g, 'inline-img-dims')
        .replace(/allowfullscreen=""/g, 'allowfullscreen');
    outputEl.textContent = replaced;
    const old = previewEl.firstChild;
    if (old) {
        previewEl.replaceChild(ret, old);
    } else {
        previewEl.appendChild(ret);
    }
}

function applyPreset(event) {
    const preset = presetEl.options[presetEl.selectedIndex].value;
    document.getElementById('BoxV4ContainerBgColor').value = 'bgc-white';
    document.getElementById('BoxV4ContainerHeight').value = '1';
    document.getElementById('BoxV4Type').value = '2-1';
    document.getElementById('AddIntroTitle').checked = false;
    document.getElementById('BoxV4ContainerBgReverse').checked = false;
    document.getElementById('BoxV4ContainerLeftItemColor').value = 'none';
    document.getElementById('BoxV4ContainerRightItemColor').value = 'none';

    setColorBoxValue('no');
    document.getElementById('AutoContentCount').value = 3;
    document.getElementById('SwapLR').checked = false;
    document.forms.BuildForm.pillButtons.value = '0';
    switch (preset) {
        default:
        case 'imgR':
            document.getElementById('BoxV4ContainerHeight').value = '2';
            document.getElementById('AddIntroTitle').checked = true;
            document.getElementById('LeftContent').value = 'text1';
            document.getElementById('RightContent').value = 'img-md';
            document.forms.BuildForm.pillButtons.value = '1';
            document.getElementById('pillButtonCenter').checked = true;
            document.getElementById('SwapLR').checked = true;
            break;
        case 'imgL':
            document.getElementById('BoxV4ContainerHeight').value = '2';
            document.getElementById('BoxV4Type').value = '1-2';
            document.getElementById('AddIntroTitle').checked = true;
            document.getElementById('LeftContent').value = 'img-md';
            document.getElementById('RightContent').value = 'text1';
            break;
        case '2greyBoxesAuto':
            document.getElementById('BoxV4ContainerBgColor').value = 'bgg-grey-200-to-grey-300';
            document.getElementById('BoxV4Type').value = 'flex-1-2';
            document.getElementById('AutoContentCount').value = 2;
            setColorBoxValue('auto');
            document.getElementById('AutoContent').value = 'greyBoxLonger';
            break;
        case '2greyBoxesManual':
            document.getElementById('BoxV4ContainerBgColor').value = 'bgg-grey-200-to-grey-300';
            document.getElementById('BoxV4Type').value = '1-1';
            document.getElementById('AutoContentCount').value = 2;
            setColorBoxValue('manual');
            document.getElementById('AutoContent').value = 'greyBoxLonger';
            document.getElementById('LeftContent').value = 'text-short';
            document.getElementById('RightContent').value = 'text-short';
            document.getElementById('BoxV4ContainerLeftItemColor').value = 'bgc-grey--200';
            document.getElementById('BoxV4ContainerRightItemColor').value = 'bgc-grey--300';
            break;
        case '3greyBoxes':
            document.getElementById('BoxV4ContainerBgColor').value = 'bgc-grey--200';
            document.getElementById('BoxV4Type').value = 'flex-1-3';
            setColorBoxValue('auto');
            document.getElementById('AutoContent').value = 'greyBox';
            break;
        case 'youtube':
            document.getElementById('BoxV4ContainerBgColor').value = 'bgc-white';
            document.getElementById('BoxV4Type').value = '1-1';
            document.getElementById('LeftContent').value = 'video';
            document.getElementById('RightContent').value = 'text2';
            break;
        case '6greenBoxes':
            document.getElementById('BoxV4ContainerBgColor').value = 'bgc-theme-tertiary';
            document.getElementById('BoxV4Type').value = 'auto';
            document.getElementById('AutoContentCount').value = 6;
            document.getElementById('AutoContent').value = 'img';
            document.getElementById('BoxV4ContainerHeight').value = '2';
            document.getElementById('AddIntroTitle').checked = true;
            break;
        case '3circles':
            document.getElementById('BoxV4ContainerBgColor').value = 'bgc-grey--300';
            document.getElementById('BoxV4Type').value = 'flex';
            document.getElementById('AutoContent').value = 'circle';
            document.getElementById('AddIntroTitle').checked = true;
            break;
        case 'smallImgLeft':
            document.getElementById('BoxV4ContainerBgColor').value = 'bgc-grey--100';
            document.getElementById('BoxV4ContainerHeight').value = '2';
            document.getElementById('BoxV4Type').value = 'auto';
            document.getElementById('AutoContent').value = 'smallImgLeft';
            document.getElementById('AddIntroTitle').checked = true;
            break;
    }
    $('.fieldset--box-v4 :input').each((index, el) => {
        $(el).change();
    });

    presetEl.value = '';
}

buildFormEl.addEventListener('change', debounce(build));
presetEl.addEventListener('change', applyPreset);

build();
