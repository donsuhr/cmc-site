import queryString from 'query-string';
import $ from 'jquery';
import form from '../../components/form';

const $el = $('.js--simple-form:first');
const formDomID = $el.attr('id');
const successRedirectUrl = $el.data('success-redirect-url') || '/contact-us/contact-complete/';
const leadSource = $el.data('lead-source');
const endpoint = $el.attr('action');
const postToUnify = !!$el.data('post-to-unify');

const { c } = queryString.parse(window.location.search);

$('[name="reseller-company"]').val(c);
$('.reseller-company-txt').text(c);

form.createForm({
    leadSource,
    successRedirectUrl,
    formDomID,
    endpoint,
    postToUnify,
});
