const select = document.querySelector('#find-reseller-select');
const items = document.querySelectorAll('.reseller-list-item');

function setListVisibility() {
    items.forEach((node) => {
        const itemRegion = node.getAttribute('data-region');
        if (itemRegion === select.value) {
            if (!node.classList.contains('js-is-visible')) {
                node.classList.add('js-is-visible');
            }
        } else {
            node.classList.remove('js-is-visible');
        }
    });
}

select.addEventListener('change', setListVisibility);

setTimeout(setListVisibility, 200);
