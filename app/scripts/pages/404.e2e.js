/* globals browser,describe,expect,it */

import config from '../../../config';
import noScriptError from '../e2e/no-script-error';

const url = `${config.domains.cmc}/campus?e2eTest=true`;

describe('404 page', () => {
    it('should load the page without an error', () => {
        browser.url(url);
        noScriptError();
    });

    it('shows some search results', () => {
        browser.url(url);
        browser.pause(500); // maybe wait a bit until request is finished
        const elements = browser.elements('.search-result-item');
        expect(elements.value.length).to.be.above(0);
    }, 3);
});
