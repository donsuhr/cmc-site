/* globals browser,describe,expect,it */

import loadDotEnv from 'cmc-load-dot-env';
import config from '../../../config';
import noScriptError from '../e2e/no-script-error';

const url = `${
    config.domains.cmc
}/snippets-and-examples/admin-xls/?e2eTest=true`;

loadDotEnv();

function isSafari9() {
    const capas = browser.desiredCapabilities;
    return capas.browserName === 'safari' && capas.version === '9';
}

function isIe11() {
    const capas = browser.desiredCapabilities;
    return capas.browserName === 'internet explorer' && capas.version === '11';
}

describe('Admin XLS page', () => {
    // eslint-disable-next-line consistent-return
    it('loads page', () => {
        browser.url(url);
        browser.deleteCookie('sign-excel');
        browser.url(url);
        if (isSafari9() || isIe11()) {
            return true;
        }
        noScriptError();
        browser.waitForExist('.password-protected-download__form__username');
        browser
            .element('.password-protected-download__form__username')
            .setValue(process.env.E2E_ADMIN_XSL_USER);
        browser
            .element('.password-protected-download__form__password')
            .setValue(process.env.E2E_ADMIN_XSL_PASSWORD);
        browser.element('.password-protected-download__form__submit').click();
        browser.waitForExist('#password-protected-download__list');
        const elements = browser.elements(
            '.password-protected-download__list-item',
        );
        expect(elements.value.length).to.be.above(0);
        noScriptError();
    });
});
