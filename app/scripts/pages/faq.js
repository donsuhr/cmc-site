import $ from 'jquery';
import 'jquery-ui/ui/core';
import 'jquery-ui/ui/widget';
import 'jquery-ui/ui/widgets/accordion';
import smoothScroll from '../../components/smoothScroll';

const removeMotion = window.matchMedia('(prefers-reduced-motion: reduce), (update: slow)').matches;

$('.faqs').accordion({
    animate: removeMotion ? 0 : 200,
    heightStyle: 'content',
    collapsible: true,
    active: false,
    activate: (event, ui) => {
        if (
            Object.hasOwnProperty.call(ui.newHeader, 'length')
            && ui.newHeader.length > 0
        ) {
            smoothScroll.scrollTo(ui.newHeader);
        }
    },
});
