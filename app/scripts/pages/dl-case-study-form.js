/* global ga */

import config from 'config';
import $ from 'jquery';
import formSerialize from 'form-serialize';
import Basil from 'basil.js';
import queryString from 'query-string';
import { checkStatus, parseJSON } from 'fetch-json-helpers';
import form from '../../components/form';
import { createFunctionWithTimeout } from '../../components/createFunctionWithTimeout';

const $el = $('.dl-case-study-form');
const formDomID = $el.attr('id');
const successRedirectUrl = false;
const leadSource = $el.data('lead-source');
const endpoint = $el.attr('action');
const postToUnify = !!$el.data('post-to-unify');

const basil = new Basil();

// use the formData from webcasts
const formData = basil.get('webcast-form') || {};
// rehydrate before createForm, which will use garlic
Object.keys(formData).forEach((key) => {
    const skip = [
        'caseStudy',
        'hsFormId',
        'hsPortalId',
        'pageUrl',
        'pageTitle',
        'hutk',
    ];
    if (!skip.includes(key)) {
        $el.find(`[name="${key}"]`).val(formData[key]);
    }
});

if ($el.find('[name="pageUrl"]').length) {
    $el.find('[name="pageUrl"]').val(window.location.href);
}
// if ($el.find('[name="pageTitle"]').length) {
//     $el.find('[name="pageTitle"]').val(document.title);
// }
if ($el.find('[name="hutk"]').length) {
    $el.find('[name="hutk"]').val(Basil.cookie.get('hubspotutk'));
}

const { site, group } = queryString.parse(
    endpoint.substring(endpoint.indexOf('?') + 1),
);
const path = window.location.pathname
    .replace(`${$el.data('base')}/`, '')
    .replace(/\/$/, '');
const [id] = path.split('/');
let dbItem;

fetch(`${config.domains.api}/${$el.data('doctype')}/${site}/${group}/${id}/`)
    .then(checkStatus)
    .then(parseJSON)
    .then((result) => {
        if (result.meta.count < 1) {
            $el.closest('.app')
                .find('.error')
                .text('Unable to find document. Please try again later.');
        } else {
            [dbItem] = result.data;
            $el.find('[name="caseStudy"]').val(dbItem.s3url);
            $el.find('[name="pageTitle"]').val(
                `${document.title} - ${site} - ${group} - ${dbItem.s3url}`,
            );
            $el.closest('.app').find('.doc-title').text(` - ${dbItem.title}`);
            $el.css('visibility', 'visible');
        }
    })
    .catch((error) => {
        $el.closest('.app')
            .find('.error')
            .text('Error loading document. Please try again later.');
    })
    .then(() => {
        $el.closest('.app')
            .find('.loading--placeholder')
            .css('display', 'none');
    });

form.createForm({
    leadSource,
    successRedirectUrl,
    formDomID,
    endpoint,
    postToUnify,
    cb: (error) => {
        if (error) {
            console.log(error); // eslint-disable-line no-console
        }
        const data = formSerialize($el.get(0), {
            hash: true,
            empty: false,
            disabled: true,
        });
        basil.set('webcast-form', data, {
            sameSite: 'Strict',
            secure: true,
        });

        const pdfUrl = process.env.NODE_ENV === 'production'
            ? `${config.domains.self}/docs/${$el.data(
                'doctype',
            )}/${site}/${group}/${dbItem.s3url}`
            : `https://${$el.data('s3bucketprefix')}${$el.data(
                'doctype',
            )}--${site}--${group}.s3.amazonaws.com/${dbItem.s3url}`;
        if (typeof ga !== 'undefined') {
            try {
                ga('send', 'event', 'Outbound Link', 'click', pdfUrl, {
                    transport: 'beacon',
                    hitCallback: createFunctionWithTimeout(() => {
                        document.location = pdfUrl;
                    }),
                });
            } catch (e) {
                document.location = pdfUrl;
            }
        } else {
            document.location = pdfUrl;
        }
    },
});
