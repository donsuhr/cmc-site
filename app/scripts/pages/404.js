import config from 'config';
import search, {
    getIndexFromDom,
    getQueryFrom404Url,
} from '../../components/search-results';

const query = getQueryFrom404Url(window.location);
const index = getIndexFromDom();

search(index, query);

const ignore = ['/about-us/board-of-directors/'];
const isIgnored = ignore.includes(window.location.pathname);

if (!isIgnored) {
    fetch(`${config.domains.api}/log-error/404/`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            locationHref: window.location.href,
            referrer: document.referrer,
        }),
    });
}
