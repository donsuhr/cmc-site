import $ from 'jquery';
import config from 'config';
import form from '../../components/form';

const $el = $('.js--simple-form--with-formalizer:first');
const formDomID = $el.attr('id');
const successRedirectUrl = $el.data('success-redirect-url') || '/contact-us/contact-complete/';
const leadSource = $el.data('lead-source');
const endpoint = $el.attr('action');
const postToUnify = !!$el.data('post-to-unify');

form.createForm({
    leadSource,
    successRedirectUrl,
    formDomID,
    endpoint,
    postToUnify,
    formalizerOptions: {
        id: config.leadLanderID,
        pairs: {
            // llname : existing element name
            email: 'email',
            firstName: 'firstName',
            lastName: 'lastName',
            phone: 'phone',
        },
    },
});
