import api from '../../components/current-openings';

api.init({
    el: '#s3docListApp',
    filterMenuOptions: [
        {
            field: 'jobcatname',
            text: 'Select Category',
            itemFnName: 'getUniqueCategory',
        },
        {
            field: 'joblocname',
            text: 'Select Location',
            itemFnName: 'getUniqueLocations',
        },
    ],
});
