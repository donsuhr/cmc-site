import $ from 'jquery';
import api from '../../components/s3-doc-list';
import template from '../../components/s3-doc-list/templates/appView-v3.hbs';
import itemTemplate from '../../components/s3-doc-list/templates/listItemView--case-study.hbs';
import featureItemTemplate from '../../components/s3-doc-list/templates/listItemView--featured.hbs';

const $el = $('#s3docListApp');

api.init({
    $el,
    site: $el.data('site'),
    group: $el.data('group'),
    docType: $el.data('doctype'),
    itemsPerPage: $el.data('items-per-page'),
    firstPageItemsPerPage: $el.data('first-page-items-per-page'),
    template,
    itemTemplate,
    featureItemTemplate,
    filterMenuOptions: [
        {
            field: 'institution-types',
            text: 'Select Institution Type',
            itemFnName: 'getUniqueTypes',
        },
    ],
});
