import Basil from 'basil.js';

const basil = new Basil();

const hasAccess = basil.get('product-center') === true;

if (!hasAccess) {
    window.location.href = '/product-center/';
}
