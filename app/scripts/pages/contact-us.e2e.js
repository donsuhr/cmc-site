/* globals browser,describe,expect,it */

import loadDotEnv from 'cmc-load-dot-env';
import config from '../../../config';
import noScriptError from '../e2e/no-script-error';

const url = `${config.domains.cmc}/contact-us/?e2eTest=true`;

loadDotEnv();

describe('Contact Us page', () => {
    it('shows complete page', () => {
        browser.url(url);
        noScriptError();
        browser.setValue('#firstName', 'firstName');
        browser.setValue('#lastName', 'firstName');
        browser.setValue('#email', 'email@donsuhr.com');
        browser.setValue('#phone', 'phone');
        browser.setValue('#institution', 'institution');
        browser.selectByValue('#reasonForInquiry', 'Website Comments');
        browser.setValue('#message', 'automated end to end test');
        browser.click('.cmc-form__submit-button');
        noScriptError();
        browser.waitUntil(
            () => browser.getText('h1.header__title').includes('Thank You for'),
            5000,
        );
        expect(browser.getUrl()).to.equal(
            `${config.domains.cmc}/contact-us/contact-complete/`,
        );
    });
});
