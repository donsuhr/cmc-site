import $ from 'jquery';
import Basil from 'basil.js';
import form from '../../components/form';

const basil = new Basil();

function onFormSubmitted(err, result) {
    basil.set('product-center', true, { expireDays: 10 });
}

const $el = $('#productCenterRegisterForm');
const formDomID = $el.attr('id');
const successRedirectUrl = $el.data('success-redirect-url')
    || '/product-center/student-lifecycle-menu/';
const leadSource = $el.data('lead-source');
const endpoint = $el.attr('action');
const postToUnify = !!$el.data('post-to-unify');

form.createForm({
    leadSource,
    successRedirectUrl,
    formDomID,
    endpoint,
    postToUnify,
    cb: onFormSubmitted,
});
