import config from 'config';
import algoliasearch from 'algoliasearch/lite';

const searchClient = algoliasearch(
    config.algolia.appId,
    config.algolia.searchKey,
);

export default searchClient;
