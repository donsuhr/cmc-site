/* globals browser  */
import isArray from 'lodash/isArray';
import parseErrorsFromLog from './parse-errors-from-log';

export default function noScriptError() {
    const logs = browser.execute(() => window.consoleHistory);
    const prefix = `${browser.desiredCapabilities.browserName}.${browser
        .desiredCapabilities.version} log check: `;
    logs.value.forEach((log) => {
        console.log(prefix, log); // eslint-disable-line no-console
    });
    const errors = parseErrorsFromLog(logs);
    errors.forEach((log) => {
        const error = isArray(log) && log.length > 1 ? JSON.parse(log[1]) : log;
        console.log(error); // eslint-disable-line no-console
        if (error.error && error.error.stack) {
            console.log(error.error.stack); // eslint-disable-line no-console
        }
    });
    if (errors.length) {
        throw new Error('Should load without script errors');
    }
}
