import isArray from 'lodash/isArray';

export default function parseErrorsFromLog(logs) {
    return logs.value.filter((log) => {
        const parts = isArray(log) ? log.join(' ') : JSON.stringify(log);
        return parts.toLowerCase().includes('error');
    });
}
