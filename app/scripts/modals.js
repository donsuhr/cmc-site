import $ from 'jquery';
// eslint-disable-next-line no-unused-vars
import 'bootstrap-sass/assets/javascripts/bootstrap/modal';

$('.modal').on('shown.bs.modal', (event) => {
    const target = $(event.target);
    target.trigger('focus');
    if (target.hasClass('search-modal')) {
        setTimeout(() => {
            target.find('input[name="q"]').trigger('focus');
        }, 1000);
    }
});
