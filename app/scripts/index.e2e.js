/* globals browser,describe,expect,it */

import config from '../../config';
import noScriptError from './e2e/no-script-error';
import parseErrorsFromLog from './e2e/parse-errors-from-log';

const url = `${config.domains.cmc}?e2eTest=true`;

describe('index script', () => {
    it('should load without any errors', () => {
        browser.url(url);
        noScriptError();
    });

    it('adds an error to the fake console', (done) => {
        browser.url(url);
        browser.execute(() => {
            setTimeout(() => {
                throw new Error('Test Error');
            }, 100);
        });
        browser.pause(200);
        const logs = browser.execute(() => window.consoleHistory);
        const errors = parseErrorsFromLog(logs);
        expect(errors.some((x) => x.includes('error'))).to.equal(true);
    });
});
