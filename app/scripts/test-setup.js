/* eslint-env node */

// const nock = require('nock');
require('@babel/polyfill');

const jsdom = require('jsdom-global'); // eslint-disable-line import/no-extraneous-dependencies
const chai = require('chai'); // eslint-disable-line import/no-extraneous-dependencies
const dirtyChai = require('dirty-chai'); // eslint-disable-line import/no-extraneous-dependencies
const sinonChai = require('sinon-chai'); // eslint-disable-line import/no-extraneous-dependencies

const fs = require('fs');
const handlebars = require('handlebars');
const underscore = require('underscore');

const safeValueHelper = require('../metalsmith/helpers/safeValue');
const ifEqHelper = require('../metalsmith/helpers/ifEq');

handlebars.registerHelper({ safeValue: safeValueHelper });
handlebars.registerHelper({ ifEq: ifEqHelper });

require.extensions['.hbs'] = (module, filename) => {
    const raw = fs.readFileSync(filename).toString();
    module.exports = handlebars.compile(raw);
};

require.extensions['.html'] = (module, filename) => {
    const raw = fs.readFileSync(filename).toString();
    module.exports = (data) => {
        const compiled = underscore.template(raw);
        return compiled({ data });
    };
};

require.extensions['.json'] = (module, filename) => {
    const raw = fs.readFileSync(filename).toString();
    if (/nls[/\\]strings\.json/.test(filename)) {
        module.exports = JSON.parse(raw).root;
    } else {
        module.exports = JSON.parse(raw);
    }
};

chai.use(dirtyChai);
chai.use(sinonChai);

jsdom(undefined, { url: 'https://api.suhrthing.com:9000' });
global.self = global;
const $ = require('jquery'); // eslint-disable-line import/order
require('whatwg-fetch');

global.$ = $;
global.jQuery = $;
window.jQuery = $;

window.matchMedia = window.matchMedia
    || (() => ({
        matches: false,
        addListener() {},
        removeListener() {},
    }));

// nock.recorder.rec();
