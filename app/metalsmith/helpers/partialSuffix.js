'use strict';

module.exports = function partialSuffix(partialName, options) {
    const langPrefix = options.data.root['url-lang-prefix'];
    const footerSuffix = langPrefix && langPrefix.length
        ? `-${langPrefix.replace(/\//, '')}`
        : '';
    return `${partialName}${footerSuffix}`;
};
