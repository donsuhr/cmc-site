'use strict';

module.exports = function transformPaginationNameArchive(name, options) {
    const matches = name.match(/(\d{4})-([a-z]+)/i);
    const month = matches[2];
    const year = matches[1];
    return `${month}, ${year}`;
};
