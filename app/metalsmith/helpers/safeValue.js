'use strict';

const Handlebars = require('handlebars');

module.exports = function safeValue(value, safeVal, options) {
    const ret = value || safeVal;
    if (ret.indexOf('{{') !== -1) {
        const template = Handlebars.compile(ret);
        return template(options.data.root);
    }
    return ret;
};
