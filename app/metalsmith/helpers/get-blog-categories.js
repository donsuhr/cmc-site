'use strict';

const Handlebars = require('handlebars');
const ldGet = require('lodash/get');

module.exports = function getBlogCategories(partial, options) {
    const categories = options.data.root.paginationCategories.map((name) => ({
        first: options.data.root.paginationUse[name].first,
        key: name,
    }));
    const hbPartial = Handlebars.partials[partial];
    const template = typeof hbPartial === 'string'
        ? Handlebars.compile(hbPartial)
        : hbPartial;
    return new Handlebars.SafeString(
        template({
            items: categories,
            currentPath: options.data.root.path,
            paginationFirst: ldGet(
                options,
                'data.root.pagination.first.path',
                '',
            ),
        }),
    );
};
