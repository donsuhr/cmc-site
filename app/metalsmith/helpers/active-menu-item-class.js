'use strict';

module.exports = function activeMenuItemClass(file, options) {
    const isIndex = file.originalFilename && file.originalFilename.endsWith('index.html');
    let ret = file.isActiveFile ? ' menu-item--active-file ' : '';
    ret += file.isActiveCollection ? ' menu-item--in-active-level ' : '';
    ret += isIndex ? ' menu-item--index ' : '';
    return ret;
};
