'use strict';

module.exports = function hasOtherAltLang(en, enin, ptbr, es, options) {
    if (en || enin || ptbr || es) {
        return options.fn(this);
    }
    return options.inverse(this);
};
