'use strict';

module.exports = function hasBgImg(opts) {
    if (
        Object.hasOwnProperty.call(this, 'page-bg-image')
        || Object.hasOwnProperty.call(this, 'page-bg-image--xs')
        || Object.hasOwnProperty.call(this, 'page-bg-image--sm')
        || Object.hasOwnProperty.call(this, 'page-bg-image--md')
        || Object.hasOwnProperty.call(this, 'page-bg-image--lg')
    ) {
        return opts.fn(this);
    }
    return opts.inverse(this);
};
