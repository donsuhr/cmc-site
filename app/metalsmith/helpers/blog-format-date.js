'use strict';

const moment = require('moment');

module.exports = function blogFormatDate(date, options) {
    return moment(date, 'M/D/YYYY').format('MMMM D, YYYY');
};
