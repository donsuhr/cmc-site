'use strict';

module.exports = function homeLinkHref(options) {
    if (
        this.originalFilename === 'index.html'
        || this.originalFilename === 'en-in/index.html'
    ) {
        return '#main';
    }
    return `${this.config.domains.self}${this['url-lang-prefix']}/`;
};
