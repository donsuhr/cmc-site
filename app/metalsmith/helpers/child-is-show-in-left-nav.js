'use strict';

module.exports = function childIsShowInLeftNav(files, options) {
    const hasChildInMenu = files
        && files.some
        && files.some(
            (x) => x['in-nav--en-us']
                || x['in-nav--en-in']
                || x['in-nav--en-uk']
                || x['in-nav--pt-br'],
        );
    if (hasChildInMenu) {
        return options.fn(this);
    }
    return options.inverse(this);
};
