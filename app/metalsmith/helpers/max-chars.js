'use strict';

module.exports = function maxChars(value, length, options) {
    return value.length > length ? `${value.substr(0, length)}...` : value;
};
