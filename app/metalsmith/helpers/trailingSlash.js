'use strict';

module.exports = function trailingSlash(path, options) {
    if (path !== '' && !path.endsWith('/')) {
        return `${path}/`;
    }
    return path;
};
