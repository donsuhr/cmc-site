'use strict';

// http://stackoverflow.com/questions/15088215/handlebars-js-if-block-helper

module.exports = function ifModEq(a, b, c, opts) {
    if (a % b === c) {
        return opts.fn(this);
    }
    return opts.inverse(this);
};
