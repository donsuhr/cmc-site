'use strict';

module.exports = function ifDefined(a, opts) {
    if (typeof this[a] !== 'undefined') {
        return opts.fn(this);
    }
    return opts.inverse(this);
};
