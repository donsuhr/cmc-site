'use strict';

module.exports = function thisYear(options) {
    return new Date().getFullYear();
};
