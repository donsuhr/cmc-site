'use strict';

const Handlebars = require('handlebars');
const moment = require('moment');

const forEach = require('lodash/forEach');
const ldTake = require('lodash/take');
const sortBy = require('lodash/sortBy');
const filter = require('lodash/filter');

module.exports = function sortPressReleases(
    take,
    maxDate,
    collectionString,
    partial,
    options,
) {
    take = take || 999;
    maxDate = maxDate || new Date(2220, 0, 0);
    collectionString = collectionString || 'news-and-events==>press-releases';
    let items = options.data.root.collections[collectionString];
    items = filter(
        items,
        (file) => !file.originalFilename.endsWith('index.html'),
    );
    items = filter(items, (file) => {
        const pubDate = new Date(file.publish);
        return Number.isNaN(pubDate.getTime()) || pubDate < maxDate;
    });
    items = sortBy(items, (file) => -moment(file.publish).toDate());
    items = ldTake(items, take);
    items = forEach(items, (file) => {
        const momentPublished = moment.utc(file.publish);
        file.label = file.label || file.title;
        file.publishFormatted = momentPublished.format('M/D/YYYY');
        file.products = file.products || [];
        file.publishDateJSON = JSON.stringify(momentPublished);
        file.path = file.path.replace(/\\/g, '/');
    });

    const hbPartial = Handlebars.partials[partial];
    const template = typeof hbPartial === 'string'
        ? Handlebars.compile(hbPartial)
        : hbPartial;
    return new Handlebars.SafeString(template({ items }));
};
