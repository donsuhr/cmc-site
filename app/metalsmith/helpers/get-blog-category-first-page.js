'use strict';

module.exports = function getBlogCategoryFirstPage(key, options) {
    return options.data.root.paginationUse[key].first.path;
};
