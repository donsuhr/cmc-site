'use strict';

module.exports = function fixEnUs(value, options) {
    return value === 'en-us' ? 'en' : value;
};
