'use strict';

module.exports = function removePtBr(path, options) {
    return path.replace(/pt-br\//, '');
};
