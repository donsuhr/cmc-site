'use strict';

const prevNextPagination = function prevNextPagination(config) {
    return (files, metalsmith, done) => {
        const sorted = Object.keys(files).sort();

        Object.keys(files).forEach((filename) => {
            const index = sorted.indexOf(filename);
            const file = files[filename];
            const nextIndex = index + 1 >= sorted.length ? 0 : index + 1;
            const nextFile = files[sorted[nextIndex]];
            file.nextFilePath = `/${nextFile.path}/`;
            const prevIndex = index === 0 ? sorted.length - 1 : index - 1;
            const prevFile = files[sorted[prevIndex]];
            file.prevFilePath = `/${prevFile.path}/`;
            const firstName = filename.split('/').slice(0, -2).join('/');
            const firstIndex = sorted.findIndex((x) => x.startsWith(firstName));
            const firstFile = files[sorted[firstIndex]];
            file.firstFilePath = `/${firstFile.path}/`;
        });
        setImmediate(done);
    };
};

module.exports = prevNextPagination;
