'use strict';

const stringReplaceAsync = require('string-replace-async');
const { resolve } = require('path');
const { promisify } = require('util');
const sizeOf = promisify(require('image-size'));

async function replacer(imgTag, ...rest) {
    const [, src] = /src=['"]?(.*?)['"]/i.exec(imgTag);
    try {
        const imgPath = resolve(process.cwd(), `./app${src}`);
        const { width, height } = await sizeOf(imgPath);
        return imgTag.replace(
            'inline-img-dims',
            `width="${width}" height="${height}"`,
        );
    } catch (err) {
        console.error(err); // eslint-disable-line no-console
        return '';
    }
}

const inlineImgDims = function inlineImgDims(config) {
    return async (files, metalsmith, done) => {
        const hrstart = process.hrtime();
        // eslint-disable-next-line no-restricted-syntax
        for (const filename of Object.keys(files)) {
            const file = files[filename];
            const contents = file.contents.toString();
            const foundRegex = /<img(?:[^>]*)(inline-img-dims)(?:[^>]*)>/gi;
            file.contents = Buffer.from(
                // intentionally serial
                // eslint-disable-next-line no-await-in-loop
                await stringReplaceAsync(contents, foundRegex, replacer),
            );
        }
        const hrend = process.hrtime(hrstart);
        // eslint-disable-next-line no-console
        console.info(
            'Metalsmith - inlineImgDims \t\tExecution time: %ds %dms',
            hrend[0],
            hrend[1] / 1000000,
        );
        setImmediate(done);
    };
};

module.exports = inlineImgDims;
