'use strict';

const stub = function stub(config) {
    return (files, metalsmith, done) => {
        setImmediate(done);
    };
};
module.exports = stub;
