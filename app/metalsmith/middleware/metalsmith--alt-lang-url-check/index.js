'use strict';

const path = require('path');

const altLangUrlCheck = function altLangUrlCheck(config) {
    return (files, metalsmith, done) => {
        const hrstart = process.hrtime();

        Object.keys(files).forEach((filename) => {
            const { 'alt--en': altEn, 'alt--en-in': altEnIn, language } = files[
                filename
            ];
            const fileErrors = [];
            if (altEn) {
                const altFilePath = `${altEn.substr(1)}index.html`.replace(/\//g, path.sep);
                if (!files[altFilePath]) {
                    fileErrors.push(`alt--en file missing in  \t\t${filename}`);
                } else {
                    const { language: altFileLang } = files[altFilePath];
                    if (altFileLang !== 'en-us') {
                        fileErrors.push(
                            `alt--en references en-in file in \t\t${filename}`,
                        );
                    }
                }
                if (language === 'en-us') {
                    fileErrors.push(`found alt--en in en file \t\t${filename}`);
                }
            }
            if (altEnIn) {
                const altFilePath = `${altEnIn.substr(1)}index.html`.replace(/\//g, path.sep);
                if (!files[altFilePath]) {
                    fileErrors.push(`alt--en-in file missing in  \t\t${filename}`);
                } else {
                    const { language: altFileLang } = files[altFilePath];
                    if (altFileLang !== 'en-in') {
                        fileErrors.push(
                            `alt--en-in references en file in \t\t${filename}`,
                        );
                    }
                }
                if (language === 'en-in') {
                    fileErrors.push(
                        `found alt--en-in in en-in file \t\t${filename}`,
                    );
                }
            }
            files[filename].fileErrors = fileErrors;
            if (fileErrors.length) {
                fileErrors.forEach((error) => {
                    // eslint-disable-next-line no-console
                    console.error(`Metalsmith ! alt lang Error: ${error}`);
                });
            }
        });
        const hrend = process.hrtime(hrstart);
        // eslint-disable-next-line no-console
        console.info(
            'Metalsmith - altLangUrlCheck \t\tExecution time: %ds %dms',
            hrend[0],
            hrend[1] / 1000000,
        );
        setImmediate(done);
    };
};

module.exports = altLangUrlCheck;
