'use strict';

const path = require('path');
const ldGet = require('lodash/get');
const imageSize = require('image-size');

const addPageImgMetadata = function addPageImgMetadata(config) {
    return (files, metalsmith, done) => {
        const hrstart = process.hrtime();

        Object.keys(files).forEach((filename) => {
            const file = files[filename];
            const fbDefault = ldGet(
                metalsmith.metadata(),
                'config.facebook.fbDefaultImg',
                '',
            );
            const fbImage = file['og-image']
                || file['social-image']
                || file['banner-image']
                || fbDefault;
            if (fbImage) {
                file['fb-image'] = fbImage;
                const fbImgPath = path.resolve(path.join('./app', fbImage));
                try {
                    const fbDimensions = imageSize(fbImgPath);
                    file['fb-image-width'] = fbDimensions.width;
                    file['fb-image-height'] = fbDimensions.height;
                } catch (e) {
                    // eslint-disable-next-line no-console
                    console.log('FB Image not found', fbImgPath);
                    delete file['fb-image'];
                }
            }

            const googleImg = file['google-image']
                || file['social-image']
                || file['banner-image']
                || 'disabled';
            if (googleImg && googleImg !== 'disabled') {
                file['google-image'] = googleImg;
                const googleImgPath = path.resolve(
                    path.join('./app', googleImg),
                );
                try {
                    const googleDimensions = imageSize(googleImgPath);
                    file['google-image-width'] = googleDimensions.width;
                    file['google-image-height'] = googleDimensions.height;
                } catch (e) {
                    // eslint-disable-next-line no-console
                    console.log('Google Image not found', googleImgPath);
                    delete file['google-image'];
                }
            }

            const twitterImg = file['twitter-image']
                || file['social-image']
                || file['banner-image']
                || fbDefault;
            if (twitterImg) {
                file['twitter-image'] = twitterImg;
                const twitterImgPath = path.resolve(
                    path.join('./app', twitterImg),
                );
                try {
                    const twitterDimensions = imageSize(twitterImgPath);
                    file['twitter-image-width'] = twitterDimensions.width;
                    file['twitter-image-height'] = twitterDimensions.height;
                } catch (e) {
                    // eslint-disable-next-line no-console
                    console.log('Twitter Image not found', twitterImgPath);
                    delete file['twitter-image'];
                }
            }
        });
        const hrend = process.hrtime(hrstart);
        // eslint-disable-next-line no-console
        console.info(
            'Metalsmith - addPageImgMetadata \tExecution time: %ds %dms',
            hrend[0],
            hrend[1] / 1000000,
        );
        setImmediate(done);
    };
};

module.exports = addPageImgMetadata;
