'use strict';

const pagination = require('metalsmith-pagination');
const assignIn = require('lodash/assignIn');
const kebabCase = require('lodash/kebabCase');

const categoryPagination = function categoryPagination(config) {
    return (files, metalsmith, done) => {
        const hrstart = process.hrtime();
        const metadata = metalsmith.metadata();
        let collectionFiles = metadata.collections[config.collection];
        collectionFiles = collectionFiles.filter(
            (file) => file.originalFilename.indexOf('index.html') === -1,
        );
        metadata.paginationCategories = [];

        collectionFiles.forEach((file) => {
            file.category.forEach((category) => {
                if (metadata.paginationCategories.indexOf(category) === -1) {
                    metadata.paginationCategories.push(category);
                }
            });
        });
        const cb = function paginationCallback() {
            return true;
        };

        metadata.paginationCategories.forEach((category) => {
            const pageMetadata = assignIn(
                {},
                {
                    byline: `Blog Category: ${category}`,
                    pageCssClass: 'has-byline',
                },
                config.paginationBlogMetadata,
            );
            pagination({
                'collections.blog': {
                    layout: config.layout || 'cmc-site.hbs',
                    path: 'blog/:name.html',
                    pageContents: config.pageContents,
                    key: category,
                    filter: (file) => !/index.html/.test(file.originalFilename)
                        && file.category.indexOf(category) !== -1,
                    pageMetadata,
                    groupBy: (file, index, options) => {
                        if (!{}.hasOwnProperty.call(file, 'categoryKeys')) {
                            file.categoryKeys = {};
                        }
                        const page = Math.ceil((index + 1) / options.perPage);
                        const catString = kebabCase(category);
                        const key = `${catString}--${page}`;
                        file.categoryKeys[key] = category;
                        return key;
                    },
                },
            }).apply(this, [files, metalsmith, cb]);
        });
        const hrend = process.hrtime(hrstart);
        // eslint-disable-next-line no-console
        console.info(
            'Metalsmith - categoryPagination \tExecution time: %ds %dms',
            hrend[0],
            hrend[1] / 1000000,
        );
        setImmediate(done);
    };
};

module.exports = categoryPagination;
