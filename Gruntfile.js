'use strict';

const timeGrunt = require('time-grunt');
const loadGruntConfig = require('load-grunt-config');
const path = require('path');
const loadDotEnv = require('cmc-load-dot-env');

if (
    !{}.hasOwnProperty.call(process.env, 'AWS_ACCESS_KEY_ID__CMC_DEMOCENTER_DL')
) {
    loadDotEnv();
}

const modulesDir = path.resolve(require.resolve('cmc-load-dot-env'), '../..');

module.exports = function gruntfile(grunt) {
    const config = {
        app: 'app',
        dist: 'dist',
        sassOutput: '.tmp/sass-out/',
        livereloadPort: 35729,
        testLiveReload: 35730,
        modulesDir,
    };

    timeGrunt(grunt);

    loadGruntConfig(grunt, {
        configPath: path.join(process.cwd(), '/build/grunt'),
        data: config,
        jitGrunt: {
            staticMappings: {
                sprite: 'grunt-spritesmith',
                scsslint: 'grunt-scss-lint',
            },
        },
    });

    grunt.registerTask('serve', (target) => {
        if (target === 'dist') {
            grunt.task.run(['build', 'connect:dist:keepalive']);
        } else {
            grunt.task.run([
                'exec:check-npm',
                'clean',
                'exec:write-config',
                'exec:build-js',
                'exec:metalsmith-dist',
                'exec:sass',
                'exec:postcss',
                'connect:livereload',
                'concurrent:serve',
            ]);
        }
    });

    grunt.registerTask('test', (target) => {
        if (target !== 'fromWatch') {
            grunt.task.run([
                // 'clean'
            ]);
        }

        grunt.task.run(['connect:test', 'mocha']);
    });

    grunt.registerTask('testServer', () => {
        grunt.task.run(['connect:testServer', 'watch:testServer']);
    });

    grunt.registerTask('build', [
        'clean',
        'exec:write-config',
        'concurrent:dist',
        'exec:write-sitemap',
        'exec:metalsmith-dist',
        'exec:critical-css',
        'exec:postcss-min',
        'filerev',
        'usemin',
        'exec:inline',
        'htmlmin',
    ]);

    grunt.registerTask('default', ['test', 'build']);

    grunt.registerTask('styles', [
        'exec:sass',
        'exec:critical-css',
        'exec:postcss-min',
        'copy:dist',
        'filerev',
        'notify:styles',
    ]);

    grunt.registerTask('pages', [
        'exec:write-config',
        'exec:write-sitemap',
        'exec:metalsmith-dist',
        'usemin',
        'exec:inline',
        'htmlmin',
    ]);

    grunt.registerTask('prelighthouse', [
        'clean',
        'exec:write-config',
        'concurrent:dist',
        'exec:write-sitemap',
        'exec:metalsmith-dist',
        'exec:critical-css',
        'exec:postcss-min',
        'exec:inline',
        'htmlmin',
    ]);

    grunt.registerTask('lighthouseUpdateCss', [
        'exec:sass',
        'exec:critical-css',
        'exec:postcss-min',
        'exec:metalsmith-dist',
        'exec:inline',
        'htmlmin',
    ]);

    grunt.registerTask('lighthouseUpdateJs', [
        'exec:build-js',
        'exec:metalsmith-dist',
        'exec:inline',
        'htmlmin',
    ]);

    grunt.registerTask('dist', ['build', 'notify:dist']);

    grunt.registerTask('netlify', ['build', 'build-search-index']);

    grunt.registerTask('build-search-index', () => {
        if (process.env.BRANCH !== 'netlify') {
            // eslint-disable-next-line no-console
            console.log('not "netlify" branch, skipping build index');
            return;
        }
        grunt.task.run([
            'aws_s3:search-index-json--get',
            'exec:build-search-index',
            'aws_s3:search-index-json--put',
        ]);
    });

    // download and index: NODE_ENV=production npm run grunt build-search-index-docs
    // just check index:    NODE_ENV=production npm run build:search-docs -- --dry-run
    grunt.registerTask('build-search-index-docs', [
        'aws_s3:search-index-json--get',
        'aws_s3:search-index-docs--get-general',
        'aws_s3:search-index-docs--get-product-sheets--pt-br',
        'aws_s3:search-index-docs--get-product-sheets--en-us',
        'aws_s3:search-index-docs--get-case-studies--en-us',
        'aws_s3:search-index-docs--get-case-studies--pt-br',
        'aws_s3:search-index-docs--get-white-papers--en-us',
        'exec:build-search-index--docs',
        'aws_s3:search-index-json--put',
    ]);
};
