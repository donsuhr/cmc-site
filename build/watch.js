/* eslint-disable no-console */

'use strict';

const fs = require('fs');
const chokidar = require('chokidar');
const { argv } = require('yargs');
const livereload = require('livereload');
const stylelint = require('stylelint');
const debounce = require('lodash/debounce');
// console.log(stylelintConfig);

const { ESLint } = require('eslint');

async function eslint() {
    const lint = new ESLint({ cache: true });
    const results = await lint.lintFiles(['**/*.js']);
    const formatter = await lint.loadFormatter('stylish');
    const resultText = formatter.format(results);
    return {
        fileCount: results.length,
        resultText,
    };
}
const debouncedEslint = debounce(() => {
    console.log('start eslint');
    eslint()
        .then((result) => {
            console.log(result.resultText);
            console.log('done linting', result.fileCount, 'files');
        })
        .catch((e) => {
            console.log('error linting');
            console.log(e);
        });
}, 3000);

const debounceStylelint = debounce(() => {
    console.log('start stylelint');
    stylelint
        .lint({
            formatter: 'string',
            cache: true,
            files: './app/**/*.scss',
        })
        .then((result) => {
            if (result.errored) {
                console.log(result.output);
            }
            console.log(`done linting ${result.results.length} files`);
        })
        .catch((e) => {
            console.log('error linting');
            console.log(e);
        });
}, 3000);

chokidar.watch(['./app/**/*.scss']).on('change', (file) => {
    console.log('File Change', file, 'stylelint queued...');
    debounceStylelint();
});

chokidar
    .watch([
        './**/*.js',
        './**/*.jsx',
        '!./app/scripts/vendor/**',
        '!./node_modules/**',
        '!./dist/**',
        '!./.tmp/**',
        '!./test/scripts/**',
        '!./bower_components/**',
    ])
    .on('change', (file) => {
        console.log('File Change', file, 'eslint queued...');
        debouncedEslint();
    });

const lrOptions = {
    debug: false,
};

if (argv.secure) {
    lrOptions.https = {
        key: fs.readFileSync('/etc/letsencrypt/live/suhrthing.com/privkey.pem'),
        cert: fs.readFileSync('/etc/letsencrypt/live/suhrthing.com/cert.pem'),
        protocol: 'http2',
    };
}

const server = livereload.createServer(lrOptions);
server.watch(['.tmp/styles/**/*.css', 'app/images/{,*/}*', 'dist/**/*.html']);
