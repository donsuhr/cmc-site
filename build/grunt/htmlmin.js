'use strict';

module.exports = function htmlmin(grunt, options) {
    return {
        dist: {
            options: {
                caseSensitive: true,
                collapseBooleanAttributes: true,
                decodeEntities: false,
                removeComments: true,
                removeRedundantAttributes: true,

            },
            files: [
                {
                    expand: true,
                    cwd: options.dist,
                    src: ['**/*.html', '!sso-iframe.html'],
                    dest: options.dist,
                },
            ],
        },
    };
};
