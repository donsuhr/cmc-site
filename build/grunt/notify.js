'use strict';

module.exports = function notify(grunt, options) {
    return {
        styles: {
            options: {
                title: 'Grunt Task',
                message: 'Styles Done',
            },
        },
        scripts: {
            options: {
                title: 'Grunt Task',
                message: 'Script Done',
            },
        },
        dist: {
            options: {
                title: 'Grunt Task',
                message: 'Dist Done',
            },
        },
    };
};
