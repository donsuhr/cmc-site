'use strict';

module.exports = function mocha(grunt, options) {
    return {
        all: {
            options: {
                urls: ['http://0.0.0.0:9002/index.html'],
                run: false,
                log: true,
                logErrors: true,
                bail: true,
                dest: 'output',
            },
        },
    };
};
