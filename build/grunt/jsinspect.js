'use strict';

module.exports = function jsinspect(grunt, options) {
    return {
        jsinspect: {
            options: {
                threshold: 30,
                diff: true,
                identifiers: false,
                failOnMatch: false,
                suppress: 100,
                reporter: 'default',
            },
            src: [
                `${options.app}/scripts/**/*.js`,
                `!${options.app}/scripts/vendor/**/*.js`,
            ],
        },
    };
};
