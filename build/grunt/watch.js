'use strict';

module.exports = function watch(grunt, options) {
    const config = {
        js: {
            files: [
                './**/*.js',
                './**/*.jsx',
                '!./app/scripts/vendor/**',
                '!./node_modules/**',
                '!./dist/**',
                '!./.tmp/**',
                '!./test/scripts/**',
                '!./bower_components/**',
            ],
            tasks: ['exec:lint'],
        },
        jstest: {
            files: ['test/spec/{,*/}*.js', `${options.app}/scripts/{,*/}*.js`],
            tasks: ['test:fromWatch'],
        },
        html: {
            files: [
                'app/pages/**/*.html',
                'app/metalsmith/layouts/**/*.hbs',
                'app/metalsmith/partials/**/*.hbs',
                'app/metalsmith/helpers/**/*.js',
            ],
            tasks: ['exec:metalsmith-dist'],
        },
        testServer: {
            options: {
                livereload: options.testLiveReload,
            },
            files: ['test/spec/{,*/}*.js', `${options.app}/scripts/{,*/}*.js`],
        },
        sass: {
            files: ['./app/**/*.scss'],
            tasks: ['exec:lint-sass'],
        },
        livereload: {
            options: {
                livereload: {
                    port: options.livereloadPort,
                },
            },
            files: [
                '.tmp/styles/**/*.css',
                `${options.app}/images/{,*/}*`,
                '/test/{,*/}*.js',
                `${options.dist}/**/*.html`,
            ],
        },
    };

    if (grunt.option('secure')) {
        config.livereload.options.livereload.key = grunt.file.read(
            '/etc/letsencrypt/live/suhrthing.com/privkey.pem',
        );
        config.livereload.options.livereload.cert = grunt.file.read(
            '/etc/letsencrypt/live/suhrthing.com/cert.pem',
        );
    }
    return config;
};
