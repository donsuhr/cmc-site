'use strict';

module.exports = function copyConfig(grunt, options) {
    return {
        dist: {
            files: [
                {
                    // touch icons, webp imgs, robots
                    expand: true,
                    dot: true,
                    cwd: options.app,
                    dest: options.dist,
                    src: [
                        '*.{ico,png,txt}',
                        'images/{,*/}*.webp',
                        '_redirects',
                        'browserconfig.xml',
                        'site.webmanifest',
                    ],
                },
                {
                    // fonts
                    expand: true,
                    dot: true,
                    cwd: `${options.app}/fonts/`,
                    dest: `${options.dist}/fonts`,
                    src: ['**/*.*'],
                },
                {
                    // unprocessed css
                    expand: true,
                    dot: true,
                    cwd: `${options.app}/css/`,
                    src: ['**/*.*'],
                    dest: `${options.dist}/styles`,
                },
                {
                    // vendor scripts
                    expand: true,
                    dot: true,
                    cwd: `${options.app}/scripts/vendor`,
                    src: ['**/*.*', '!jwplayer-7.1.4/skins/**'],
                    dest: `${options.dist}/scripts/vendor`,
                },
                {
                    expand: true,
                    cwd: `${options.app}/images`,
                    src: '**/*.{gif,jpeg,jpg,png,svg}',
                    dest: `${options.dist}/images`,
                },
            ],
        },
        netlify: {
            files: [
                {
                    // netlify not doing imagemin, it does it on its own
                    expand: true,
                    dot: true,
                    cwd: `${options.app}/images/`,
                    src: ['**/*.*'],
                    dest: `${options.dist}/images`,
                },
            ],
        },
        overlay: {
            files: [
                {
                    // netlify not doing imagemin, it does it on its own
                    expand: true,
                    dot: true,
                    cwd: 'file-asset-overlay',
                    src: ['**/*.*'],
                    dest: options.dist,
                },
            ],
        },
    };
};
