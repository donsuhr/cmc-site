'use strict';

const critical = require('critical');
const fs = require('fs');
const path = require('path');

// https://stackoverflow.com/questions/13542667/create-directory-when-writing-to-file-in-node-js
function ensureDirectoryExistence(filePath) {
    const dirname = path.dirname(filePath);
    if (!fs.existsSync(dirname)) {
        ensureDirectoryExistence(dirname);
        fs.mkdirSync(dirname);
    }
}

function loadForceFile(src) {
    return new Promise((resolve, reject) => {
        const file = path.resolve(src);
        fs.readFile(file, 'utf8', (rfErr, data) => {
            if (rfErr) {
                return reject(rfErr);
            }
            return resolve(data);
        });
    });
}

function generate({ src, name }) {
    return critical
        .generate({
            base: './dist/',
            src,
            css: ['./.tmp/sass-out/main.css'],
            // dest: `../.tmp/styles/critical-${name}.css`,
            minify: false,
            inline: false,
            extract: false,
            ignore: {
                atrule: ['@font-face'],
                rule: [
                    /^\.search-form/,
                    /modal/,
                    /^\.cmc-footer/,
                    /^\.main-nav__pages?-list/,
                    '.main-nav__toggle-contents',
                    '.main-nav__pages-sub-list',
                    /^.global-nav__/,
                    /^\.social-media-share__button--(.*):before/,
                    /^\.top-nav-menu__charm-button--(.*):after/,
                    '.left-nav-menu__toggle-button:before',
                ],
            },
            dimensions: [
                {
                    width: 414,
                    height: 896,
                },
                {
                    width: 1280,
                    height: 960,
                },
            ],
            penthouse: {
                screenshots: {
                    basePath: `./.tmp/crit-out/${name}`,
                },
            },
        })
        .then((result) => ({
            src,
            name,
            result,
        }));
}

const items = [
    { src: 'index.html', name: 'home' },
    { src: 'products/crm-for-higher-education/index.html', name: 'crm' },
    { src: 'products/radius-by-campus-management/index.html', name: 'radius' },
];

loadForceFile('./.tmp/sass-out/crit-forced.css').then((forceCss) => {
    ensureDirectoryExistence(path.resolve(process.cwd(), './.tmp/crit-out/foo'));
    Promise.all(items.map(generate)).then((results) => {
        const combinedResults = `${forceCss} \n${results
            .map((x) => x.result.css)
            .join('\n')}`;
        const combinedOutputFile = path.resolve(
            process.cwd(),
            './.tmp/sass-out/critical.css',
        );
        fs.writeFileSync(combinedOutputFile, combinedResults, 'utf8');

        ensureDirectoryExistence(
            path.resolve(process.cwd(), './.tmp/crit-out'),
        );

        results.forEach(({ src, name, result }) => {
            const outputFile = path.resolve(
                process.cwd(),
                `./.tmp/crit-out/critical-${name}.css`,
            );
            ensureDirectoryExistence(outputFile);
            fs.writeFileSync(outputFile, result.css, 'utf8');
        });
    });
});
