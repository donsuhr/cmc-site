/* eslint-disable no-console */

'use strict';

const glob = require('glob');
const path = require('path');
const util = require('util');
const PromisePool = require('es6-promise-pool'); // eslint-disable-line import/no-extraneous-dependencies
const execFile = util.promisify(require('child_process').execFile);
const cwebp = require('cwebp-bin'); // eslint-disable-line import/no-extraneous-dependencies

function* generatePromises(files) {
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < files.length; i++) {
        const file = files[i];
        const filename = `${path.basename(file, path.extname(file))}.webp`;
        const pathname = file
            .replace('/app/', '/dist/')
            .replace('/.tmp/', '/dist/');
        const dest = path.join(path.dirname(pathname), filename);
        // console.log(file, '=>', dest)
        yield execFile(cwebp, [file, '-quiet', '-o', dest]);
    }
}

glob('./app/images/{pages,ui}/**/*.{jpg,png}', (er, files) => {
    // glob('./app/images/pages/home/homebagnner-radius-logo.png', (er, files) => {
    const promiseIterator = generatePromises(files);
    const pool = new PromisePool(promiseIterator, 3);

    pool.addEventListener('rejected', (event) => {
        console.log(`Rejected: ${event.data.error.message}`);
    });

    pool.start().then(() => console.log('Complete'));
});

glob('./.tmp/images/**/*.{jpg,png}', (er, files) => {
    Promise.all(generatePromises(files)).then(() => {
        console.log('sprite complete');
    });
});
