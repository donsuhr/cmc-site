/* eslint-disable no-console */

'use strict';

const glob = require('glob');
const path = require('path');
// const fs = require('fs');
const PromisePool = require('es6-promise-pool'); // eslint-disable-line import/no-extraneous-dependencies
const { FileSniffer, asArray } = require('filesniffer'); // eslint-disable-line import/no-extraneous-dependencies

let EXITCONDITION = false;

const searchPath = path.resolve(process.cwd(), 'app');

const matchFilePromise = (searchTerm, file) => new Promise((resolve, reject) => {
    FileSniffer.create()
        .path(searchPath)
        .depth(99) // 10 levels
        .collect(asArray())
        .find(searchTerm)
        .then((matches) => {
            resolve({ matches, file });
        });
});

function* generatePromises(files, includeExtension = true) {
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < files.length; i++) {
        const file = files[i];
        const filename = path.basename(file);
        const filenameWithOutExtension = path.basename(
            file,
            path.extname(file),
        );
        const searchTerm = includeExtension
            ? new RegExp(filename, 'i')
            : new RegExp(filenameWithOutExtension, 'i');
        const val = matchFilePromise(searchTerm, file);
        yield val;
    }
}

glob('./app/images/{ui,pages}/**/*.{jpg,png,gif}', (er, files) => {
    console.log('checking', files.length, 'files');
    const promiseIterator = generatePromises(files);
    const pool = new PromisePool(promiseIterator, 1);

    pool.addEventListener('fulfilled', (event) => {
        if (event.data.result.matches.length === 0) {
            const filePath = path.resolve(
                process.cwd(),
                event.data.result.file,
            );
            console.log(filePath);
            // fs.unlink(filePath);
        }
    });

    pool.addEventListener('rejected', (event) => {
        console.log(`Rejected: ${event.data.error.message}`);
    });

    pool.start()
        .then(() => {
            console.log('Complete');
            EXITCONDITION = true;
        })
        .catch((error) => {
            console.log('some catch error');
        });
});

function wait() {
    if (!EXITCONDITION) {
        setTimeout(wait, 1000);
    }
}

wait();
