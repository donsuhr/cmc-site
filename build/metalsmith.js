'use strict';

const dotEnv = require('dotenv');
const path = require('path');
const fs = require('fs');
const { argv } = require('yargs');
const merge = require('lodash/merge');
const assignIn = require('lodash/assignIn');
const moment = require('moment');
const isEqual = require('lodash/isEqual');

dotEnv.config();

const blogHbs = fs.readFileSync(
    path.resolve(`${__dirname}/../app/metalsmith/partials/hb-layouts/blog.hbs`),
);

const Metalsmith = require('metalsmith');
const layouts = require('metalsmith-layouts');
const inPlace = require('metalsmith-in-place');
const helpers = require('metalsmith-register-helpers');
const publish = require('metalsmith-publish');
const chokidar = argv.watch ? require('chokidar') : null;
const collections = require('metalsmith-collections');
const defaultValues = require('metalsmith-default-values');
const permalinks = require('metalsmith-permalinks');
const pagination = require('metalsmith-pagination');
const metalsmithEnv = require('metalsmith-env');
const sitemap = require('metalsmith-sitemap');
const addOriginalFilename = require('metalsmith-add-orig-file-name');

const handlebars = require('handlebars');
const handlebarsLayouts = require('handlebars-layouts');
// assemble helpers have the handlebars-layouts in it. assemble first

require('handlebars-helpers')({
    handlebars,
}); // http://assemble.io/helpers/

// overwrite second
handlebars.registerHelper(handlebarsLayouts(handlebars));

const categoryPagination = require('../app/metalsmith/middleware/metalsmith-catagory-pagination');
const addPrevNextPath = require('../app/metalsmith/middleware/metalsmith--add-prev-next');
const addFormatedDate = require('../app/metalsmith/middleware/metalsmith--add-pub-date-formatted');
const addPageImgMetadata = require('../app/metalsmith/middleware/metalsmith--add-page-img-metadata');
const inlineImgDims = require('../app/metalsmith/middleware/metalsmith--inline-img-dims');
const altLangUrlCheck = require('../app/metalsmith/middleware/metalsmith--alt-lang-url-check');
const webpackAssets = require('../app/metalsmith/middleware/metalsmith-webpack-assets');
const middlewareStub = require('../app/metalsmith/middleware/metalsmith--stub');

require('@babel/register')({
    only: [/config.js/],
    presets: ['@babel/preset-env'],
});

const config = require('../config').default;

const production = process.env.NODE_ENV === 'production';

const publishConfig = {
    draft: !production,
    private: !production,
    future: !production,
};

const paginationBlogMetadata = {
    collection: ['blog'],
    'menu-priority': 0,
    pageCssClass: 'has-byline',
    title: 'Blog',
    'banner-image': '/images/pages/news-and-events/Banner_pr.gif',
    private: true,
    'additional-stylesheets': [
        { href: '/styles/pages/blog.css', inline: true },
    ],
};

const webpackAssetsFilePath = 'webpack-manifest.json';
let webpackManifestJson = JSON.parse(
    fs.readFileSync(path.resolve(process.cwd(), webpackAssetsFilePath)),
);

function compareWebpackManifest() {
    const newJson = JSON.parse(
        fs.readFileSync(path.resolve(process.cwd(), webpackAssetsFilePath)),
    );
    const result = isEqual(webpackManifestJson, newJson);
    webpackManifestJson = newJson;
    return result;
}

function build(clean, collectionsObj, changedFile) {
    const t1 = process.hrtime();
    const ms = new Metalsmith(path.resolve(`${__dirname}/../`));
    console.log('Metalsmith build start'); // eslint-disable-line no-console
    const altLangMiddleware = changedFile
        ? middlewareStub()
        : altLangUrlCheck();
    ms.clean(!!clean)
        .source('app/pages')
        .destination('dist')
        .metadata({
            config,
        })
        .ignore((currentPath, lstat) => {
            // true to ignore
            if (/DS_Store/.test(currentPath)) {
                return true;
            }
            if (argv['dev-folder-only']) {
                return !currentPath.includes('app/pages/dev');
            }
            if (changedFile) {
                if (lstat.isDirectory()) {
                    // don't ignore directories, need to search sub dirs
                    return false;
                }
                let parent = changedFile
                    .split(path.sep)
                    .slice(0, -1)
                    .join(path.sep);
                while (parent.length) {
                    if (
                        currentPath.indexOf(
                            `${parent}${path.sep}index.html`,
                        ) !== -1
                    ) {
                        // is parent index file, dont ignore
                        return false;
                    }
                    parent = parent.split(path.sep).slice(0, -1).join(path.sep);
                }
                // check for same directory sibling files and below
                return currentPath.indexOf(changedFile) === -1;
            }
            // no file changed, ignore nothing
            return false;
        })
        .use(metalsmithEnv())
        .use(
            defaultValues([
                {
                    pattern: 'snippets-and-examples/**/*.html',
                    defaults: {
                        'in-nav--en-us': process.env.NODE_ENV !== 'production',
                        'in-nav--en-in': false,
                        'in-nav--en-uk': false,
                        'in-nav--es': false,
                        publish: 'private',
                        private: true,
                    },
                },
                {
                    pattern: 'demos/**/*.html',
                    defaults: {
                        'in-nav--en-us': process.env.NODE_ENV !== 'production',
                        'in-nav--en-in': false,
                        'in-nav--en-uk': false,
                        'in-nav--es': false,
                        private: true,
                    },
                },
                {
                    pattern: 'dev/**/*.html',
                    defaults: {
                        'in-nav--en-us': process.env.NODE_ENV !== 'production',
                        'in-nav--en-in': false,
                        'in-nav--en-uk': false,
                        'in-nav--es': false,
                        private: true,
                    },
                },
                {
                    pattern: 'blog/**/*.html',
                    defaults: {
                        'in-nav--en-us': process.env.NODE_ENV !== 'production',
                        'in-nav--en-in': false,
                        'in-nav--en-uk': false,
                        'in-nav--es': false,
                        private: true,
                        title: 'Blog',
                        'banner-image':
                            '/images/pages/news-and-events/Banner_pr.gif',
                        'additional-stylesheets': [
                            { href: '/styles/pages/blog.css', inline: true },
                        ],
                    },
                },
                {
                    pattern: '**/*.html',
                    defaults: {
                        layout: 'cmc-site.hbs',
                        language: 'en-us',
                        'url-lang-prefix': '',
                        'menu-priority': 0.5,
                        'og-type': 'article',
                        leftNavStartFrom: 'pages',
                        'in-nav--en-us': true,
                        'in-nav--en-in': true,
                        'in-nav--en-uk': true,
                        'in-nav--pt-br': false,
                        'in-nav--es': false,
                    },
                },
            ]),
        )
        .use(publish(publishConfig))
        .use(addOriginalFilename({}))
        .use(addFormatedDate({}))
        .use(addPageImgMetadata({}))
        .use(webpackAssets({}))
        .use(collections(collectionsObj))
        .use(
            pagination({
                'collections.blog': {
                    layout: 'cmc-site.hbs',
                    first: 'blog/index.html',
                    path: 'blog/page-:num.html',
                    pageContents: blogHbs,
                    filter: (file) => !/index.html/.test(file.originalFilename),
                    pageMetadata: paginationBlogMetadata,
                },
            }),
        )
        .use(
            pagination({
                'collections.blog': {
                    layout: 'cmc-site.hbs',
                    path: 'blog/:name.html',
                    pageContents: blogHbs,
                    key: 'monthly',
                    filter: (file) => !/index.html/.test(file.originalFilename),
                    groupBy: (file, index, options) => {
                        const date = moment(file.publishFormatted, 'M/D/YYYY');
                        return date.format('YYYY-MMMM');
                    },
                    pageMetadata: assignIn(
                        {},
                        {
                            byline: 'Blog Archive: ',
                            'is-archive-page': true,
                        },
                        paginationBlogMetadata,
                    ),
                },
            }),
        )
        .use(
            categoryPagination({
                collection: 'blog',
                pageContents: blogHbs,
                paginationBlogMetadata,
                layout: 'cmc-site.hbs',
            }),
        )
        .use(
            permalinks({
                relative: false,
            }),
        )
        .use(
            sitemap({
                hostname: process.env.CORS_ORIGIN_SELF,
                omitIndex: true,
                omitExtension: true,
                output: 'pages.xml',
            }),
        )
        .use(altLangMiddleware)
        .use(addPrevNextPath({}))
        .use(
            helpers({
                engine: 'handlebars',
                directory: 'app/metalsmith/helpers',
            }),
        )
        .use(
            // keep 1.4.4, v2+ requires correct extention of .hbs,
            // we currently use .html
            inPlace({
                engine: 'handlebars',
                partials: 'app/metalsmith/partials',
            }),
        )
        .use(
            layouts({
                engine: 'handlebars',
                directory: 'app/metalsmith/layouts',
                partials: 'app/metalsmith/partials',
            }),
        )
        .use(inlineImgDims({}))
        .build((err, files) => {
            if (err) {
                console.log(err); // eslint-disable-line no-console
                console.log('Error rendering files. Stack:'); // eslint-disable-line no-console
                console.log(err.stack); // eslint-disable-line no-console
            } else {
                const hrend = process.hrtime(t1);
                // eslint-disable-next-line no-console
                console.info(
                    `Metalsmith - Done ${
                        Object.keys(files).length
                    } files written \tExecution time: %ds %dms`,
                    hrend[0],
                    hrend[1] / 1000000,
                );
            }
        });
}

function getdirSync(dirPath, parentName, result) {
    const stat = fs.lstatSync(dirPath);
    result = result || {};
    if (stat.isDirectory()) {
        const name = parentName
            ? `${parentName}==>${path.basename(dirPath)}`
            : path.basename(dirPath);
        const localName = name.replace(/^pages==>/, '');
        if (parentName) {
            // ignore root dir
            result[localName] = {
                path: dirPath,
                name: localName,
                refer: false,
                pattern: `${localName.replace(/==>/g, '/')}/*.html`,
            };
        }

        fs.readdirSync(dirPath).map((child) => getdirSync(`${dirPath}/${child}`, name, result));
    }
    return result;
}

const autoCollections = getdirSync(path.resolve(`${__dirname}/../app/pages`));
const specificCollections = {
    // add links and other 'sub-site' metadata here; page metadata in defaultValues
    /* link example -- moved to files:
    'about-us': {n
        metadata: {
            links: [
                {
                    'menu-priority': 0.9,
                    href: '/about-us/careers/mission-and-vision/',
                    label: 'Mission, Values & Vision',
                    'show-in-quick-launch': true,
                    'show-in-left-nav': true,
                },
            ],
        },n
    },
    */
    blog: {
        pattern: 'blog/**.html',
        sortBy: 'publishDate',
        reverse: true,
    },
};

const collectionsObj = merge({}, autoCollections, specificCollections);

if (!argv.watch_only) {
    build(!!argv.clean, collectionsObj);
}

let pendingBuild = new Set();
let buildTimeout;
const debounceBuild = (bool, collObj, limitToFolder) => {
    if (limitToFolder === false) {
        pendingBuild = 'all';
    }
    if (pendingBuild !== 'all') {
        pendingBuild.add(limitToFolder);
    }
    clearTimeout(buildTimeout);
    buildTimeout = setTimeout(() => {
        if (pendingBuild === 'all') {
            build(bool, collObj, false);
        } else {
            pendingBuild.forEach((x) => {
                build(bool, collObj, x);
            });
        }
        pendingBuild = new Set();
    }, 500);
};

if (argv.watch) {
    console.log('Metalsmith watching files...'); // eslint-disable-line no-console
    chokidar
        .watch([
            'app/pages/**/*.html',
            'app/metalsmith/layouts/**/*.hbs',
            'app/metalsmith/partials/**/*.hbs',
            'app/metalsmith/helpers/**/*.js',
            webpackAssetsFilePath,
        ])
        .on('change', (file) => {
            const folder = path.dirname(file);
            const type = path.extname(file);
            console.log('Metalsmith Watch: change', file); // eslint-disable-line no-console
            const limitToFolder = type === '.html' ? folder : false;
            if (file === webpackAssetsFilePath) {
                if (!compareWebpackManifest()) {
                    // eslint-disable-next-line no-console
                    console.log(
                        'webpack manifest file changed -- will rebuild',
                    );
                    debounceBuild(false, collectionsObj, false);
                } else {
                    // eslint-disable-next-line no-console
                    console.log(
                        'webpack manifest file NOT changed -- skip build',
                    );
                }
            } else {
                debounceBuild(false, collectionsObj, limitToFolder);
            }
        });
}
