'use strict';

const { argv } = require('yargs');
const loadDotEnv = require('cmc-load-dot-env');
const algoliasearch = require('algoliasearch');

const { buildSearchIndexDocs } = require('build-algolia-index');

if (!{}.hasOwnProperty.call(process.env, 'CORS_ORIGIN_TLD')) {
    // eslint-disable-next-line no-console
    console.log('no CORS_ORIGIN_TLD set, will load .env');
    loadDotEnv();
}

const searchClient = algoliasearch(
    process.env.ALGOLIA_APPID,
    process.env.ALGOLIA_ADMIN_KEY,
);

const dryRun = !!argv['dry-run'];

const groups = [
    {
        json: 'search-index-docs--cmc--en-us.json',
        algoliaIndex: 'cmc--en-us',
        src: [
            {
                docType: 'general',
                site: 'cmc',
                group: '',
            },
            {
                docType: 'product-sheets',
                site: 'cmc',
                group: 'en-us',
            },
            {
                docType: 'case-studies',
                site: 'cmc',
                group: 'en-us',
            },
            {
                docType: 'white-papers',
                site: 'cmc',
                group: 'en-us',
            },
        ],
    },
];

const groupsPTBR = [
    {
        json: 'search-index-docs--cmc--pt-br.json',
        algoliaIndex: 'cmc--pt-br',
        src: [
            {
                docType: 'product-sheets',
                site: 'cmc',
                group: 'pt-br',
            },
            {
                docType: 'case-studies',
                site: 'cmc',
                group: 'pt-br',
            },
        ],
    },
];

buildSearchIndexDocs(
    groups,
    searchClient,
    dryRun,
    process.env.CORS_ORIGIN_CMC,
    process.env.CORS_ORIGIN_API,
    process.env.AWS_S3_BUCKET_PREFIX,
);

buildSearchIndexDocs(
    groupsPTBR,
    searchClient,
    dryRun,
    process.env.CORS_ORIGIN_PTBR,
    process.env.CORS_ORIGIN_API,
    process.env.AWS_S3_BUCKET_PREFIX,
);
