'use strict';

const glob = require('glob');
const { inlineSource } = require('inline-source');
const path = require('path');
const fs = require('fs');
const webpackAssets = require('../webpack-assets.json');

/*
 first test:
 node ./build/compile-robots.js && \
 npm run grunt sass && \
 npm run build:js && \
 npm run grunt makeFontsJs && \
 npm run build:html && \
 npm run grunt cssmin && \
 node run ./build/critical-css.js && \
 node ./build/inline.js

 second test:
 npm run build:html && \
 node ./build/inline.js

 test _headers file:
 node ./build/compile-robots.js && \
 NODE_ENV=production npm run build:js &&\
 node ./build/inline.js

*/

/**
 * Replace </scripts/xxxx.js> in _headers file where 'xxxx' matches a file in webpack-assets.json
 * usemin replaces link to css file
 * most scripts now injected in compile-robots.js
 */

const headerFile = path.resolve('./dist/_headers');
fs.readFile(headerFile, 'utf8', (rfErr, data) => {
    if (rfErr) {
        throw rfErr;
    }
    let result = data;
    Object.keys(webpackAssets).forEach((bundleName) => {
        const bundleInfo = webpackAssets[bundleName];
        const type = Object.keys(bundleInfo)[0];
        const bundleFile = bundleInfo[type];
        const regex = new RegExp(`</scripts/${bundleName}.js>`);
        result = result.replace(regex, `<${bundleFile}>`);
    });
    fs.writeFile(headerFile, result, 'utf8', (wfErr) => {
        if (wfErr) {
            throw wfErr;
        }
    });
});

glob('./dist/**/*.html', (er, files) => {
    files.forEach((file) => {
        const htmlpath = path.resolve(file);
        fs.readFile(htmlpath, 'utf8', (rfErr, data) => {
            if (rfErr) {
                throw rfErr;
            }
            let result = data;
            Object.keys(webpackAssets).forEach((bundleName) => {
                const bundleInfo = webpackAssets[bundleName];
                const type = Object.keys(bundleInfo)[0];
                const bundleFile = bundleInfo[type];
                const regex = new RegExp(
                    `<script\\s+src=["']/scripts/${bundleName}.js["']`,
                );
                result = result.replace(regex, `<script src="${bundleFile}"`);
            });

            fs.writeFile(htmlpath, result, 'utf8', async (wfErr) => {
                if (wfErr) {
                    throw wfErr;
                }
                try {
                    const html = await inlineSource(htmlpath, {
                        compress: false,
                        ignore: ['a'],
                        rootpath: path.resolve('dist'),
                        handlers: [
                            (source, context) => {
                                if (
                                    source
                                    && source.fileContent
                                    && (source.type === 'js'
                                        || source.type === 'css')
                                ) {
                                    // eslint-disable-next-line no-console
                                    // console.log('replacing', source.match);
                                    const smPath = source.match.match(
                                        /(?:href|src)=['"]([^'"]*)/,
                                    );
                                    const smDir = path.dirname(smPath[1]);
                                    source.fileContent = source.fileContent.replace(
                                        /# sourceMappingURL=([^\s*/]*)/,
                                        `# sourceMappingURL=${smDir}/$1\n`,
                                    );
                                }
                                return Promise.resolve();
                            },
                        ],
                    });
                    fs.writeFile(htmlpath, html, (fsError) => {
                        if (fsError) {
                            throw fsError;
                        }
                    });
                } catch (err) {
                    // eslint-disable-next-line no-console
                    console.log('\ninline-source error');
                    // eslint-disable-next-line no-console
                    console.log(err);
                }
            });
        });
    });
});
