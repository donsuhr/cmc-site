'use strict';

const express = require('express'); // eslint-disable-line import/no-extraneous-dependencies
const path = require('path');
const compression = require('compression');

const app = express();

app.use(compression());
app.set('case sensitive routing', true);
app.use(express.static(path.resolve(__dirname, '../../dist')));

app.listen(3000, () => {
    // eslint-disable-next-line no-console
    console.log('Example app listening on port 3000!');
});
