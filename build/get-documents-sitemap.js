'use strict';

const path = require('path');
const fs = require('fs');
const loadDotEnv = require('cmc-load-dot-env');
const fetch = require('node-fetch');
const _ = require('underscore');
const { checkStatus, parseJSON } = require('fetch-json-helpers');

const filePath = path.resolve(__dirname, '../app/documents.xml.tmpl');
const template = fs.readFileSync(filePath, { encoding: 'UTF-8' });

if (!{}.hasOwnProperty.call(process.env, 'CORS_ORIGIN_TLD')) {
    // eslint-disable-next-line no-console
    console.log('no CORS_ORIGIN_TLD set, will load .env');
    loadDotEnv();
}

const options = {
    outputPath: path.resolve(__dirname, '../dist'),
    src: [
        {
            docType: 'product-sheets',
            site: 'cmc',
            group: 'en-us',
        },
        {
            docType: 'case-studies',
            site: 'cmc',
            group: 'en-us',
        },
        {
            docType: 'general',
            site: 'cmc',
            group: '',
        },
    ],
};

const fetches = options.src.map((group) => {
    const site = group.site ? `${group.site}/` : '';
    const gr = group.group ? `${group.group}/` : '';
    const fetchUrl = `${process.env.CORS_ORIGIN_API}/${group.docType}/${site}${gr}`;
    return fetch(fetchUrl)
        .then(checkStatus)
        .then(parseJSON)
        .then((response) => response.data
            .filter((item) => item.display === true)
            .map((item) => {
                const file = encodeURI(
                    `${process.env.CORS_ORIGIN_SELF}/docs/${group.docType}/${site}${gr}${item.s3url}`,
                );
                const generalFile = encodeURI(
                    `${process.env.CORS_ORIGIN_SELF}/documents/${item.s3url}`,
                );
                const url = group.docType === 'general' ? generalFile : file;
                const currentLang = group.group === 'en-us' ? 'en' : group.group;
                const hasAltLang = (!!item['alt--en'] && currentLang !== 'en')
                        // || (!!item['alt--en-in'] && currentLang !== 'en-in')
                        || (!!item['alt--pt-br'] && currentLang !== 'pt-br');

                function processUrl(altLangUrl) {
                    return `${process.env.CORS_ORIGIN_CMC}${encodeURI(
                        altLangUrl.replace(process.env.CORS_ORIGIN_CMC, ''),
                    )}`;
                }
                const enUrl = item['alt--en']
                    ? processUrl(item['alt--en'])
                    : '';
                const inUrl = item['alt--en-in']
                    ? processUrl(item['alt--en-in'])
                    : '';
                const ptUrl = item['alt--pt-br']
                    ? processUrl(item['alt--pt-br'])
                    : '';

                return {
                    url,
                    hasAltLang,
                    currentLang,
                    'alt--en': enUrl,
                    'alt--en-in': inUrl,
                    'alt--pt-br': ptUrl,
                };
            }));
});

Promise.all(fetches)
    .then((results) => {
        const flattened = results.reduce((prev, cur) => prev.concat(cur), []);
        const compiled = _.template(template, { variable: 'data' });
        const result = compiled({
            items: flattened,
        });
        fs.writeFile(
            `${options.outputPath}/documents.xml`,
            result,
            'utf8',
            (writeConfigError) => {
                if (writeConfigError) {
                    // eslint-disable-next-line no-console
                    console.log(
                        'error writing file, documents.xml',
                        writeConfigError,
                    );
                } else {
                    // eslint-disable-next-line no-console
                    console.log(
                        `Generated new documents.xml file in folder: ${options.outputPath}`,
                    );
                }
            },
        );
    })
    .catch((error) => {
        console.log('error loading', error); // eslint-disable-line no-console
    });
