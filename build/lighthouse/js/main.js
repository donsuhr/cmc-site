/* global Vue */
// eslint-disable-next-line import/extensions
import App from '../components/App.js';

new Vue({
    render: (h) => h(App),
}).$mount('#app');
