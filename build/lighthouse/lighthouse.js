'use strict';

/*
when runnng do something like:
npm run serve:lighthouse
then rebuild changes with:
NODE_ENV=production npm run grunt lighthouseUpdateCss
NODE_ENV=production npm run grunt lighthouseUpdateJs
http-server this folder for index.html
*/

const lighthouse = require('lighthouse'); // eslint-disable-line import/no-extraneous-dependencies
const chromeLauncher = require('chrome-launcher'); // eslint-disable-line import/no-extraneous-dependencies
const fs = require('fs').promises;
const path = require('path');

const dataFile = path.resolve(__dirname, './data/', 'runs.json');

const MODE_DESKTOP = 'desktop'; // eslint-disable-line no-unused-vars
const MODE_MOBILE = 'mobile'; // eslint-disable-line no-unused-vars

const TEST_URL = 'https://cmc.suhrthing.com:9002/about-us/careers-at-Campus-Management/mission-and-vision/';
const TEST_DESCRIPTION = 'Mission & Vision - no tracking - minified html - broke up task v2 -ga';
const MODE = MODE_MOBILE;
const NUM_RUNS = 4;

const chromeOpts = {
    chromeFlags: ['--show-paint-rects', '--headless'],
};

const blockedUrlPatterns = [
    // '*.googletagmanager.com',
    // '*.google-analytics.com',
    '*.licdn.com',
    '*.googleadservices.com',
    '*.doubleclick.net',
    '*.hs-scripts.com',
    '*.hs-analytics.net',
    '*.leadlander.com',
];

async function launchChromeAndRunLighthouse(url, config) {
    const chrome = await chromeLauncher.launch(chromeOpts);
    const options = {
        ...config,
        logLevel: 'silent',
        output: 'json',
        onlyCategories: ['performance'],
        skipAudits: [
            'uses-rel-preload',
            'uses-rel-preconnect',
            'font-display',
            'performance-budget',
            'timing-budget',
            'resource-summary',
            'uses-long-cache-ttl',
            'offscreen-images',
            'unminified-css',
            'uses-webp-images',
        ],
        port: chrome.port,
    };

    const runnerResult = await lighthouse(url, options);
    await chrome.kill();
    return runnerResult;
}

const mobileConfig = {
    extends: 'lighthouse:default',
    settings: {
        maxWaitForFcp: 15 * 1000,
        maxWaitForLoad: 35 * 1000,
        throttlingMethod: 'devtools',

    },
};

const desktopConfig = {
    emulatedFormFactor: 'desktop',
    throttling: {
        rttMs: 40,
        throughputKbps: 10 * 1024,
        cpuSlowdownMultiplier: 1,
        requestLatencyMs: 0, // 0 means unset
        downloadThroughputKbps: 0,
        uploadThroughputKbps: 0,
    },
};

const lighthouseConfig = {
    blockedUrlPatterns,
    ...mobileConfig,
    ...(MODE === MODE_DESKTOP && desktopConfig),
};

async function writeFile(report) {
    const now = new Date();
    const format = new Intl.DateTimeFormat('en-US', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
        hour12: false,
    });
    const [
        { value: month },,
        { value: day },,
        { value: year },,
        { value: hour },,
        { value: minute },,
        { value: second },
    ] = format.formatToParts(now);
    const fileNamePrefix = `${year}-${month}-${day}-${hour}-${minute}-${second}`;
    const reportOutputFile = path.resolve(
        __dirname,
        './data/',
        `${fileNamePrefix}.json`,
    );
    await fs.writeFile(reportOutputFile, report);
    // eslint-disable-next-line no-console
    console.log('  wrote file', `${fileNamePrefix}.json`);
}

function getResultObject(lhr) {
    return {
        fetchTime: lhr.fetchTime,
        'largest-contentful-paint':
            lhr.audits['largest-contentful-paint'].numericValue,
        'largest-contentful-paint-element':
            lhr.audits['largest-contentful-paint-element'].details.items[0].node
                .selector,
        'first-meaningful-paint':
            lhr.audits['first-meaningful-paint'].numericValue,
        'cumulative-layout-shift':
            lhr.audits['cumulative-layout-shift'].numericValue,
        interactive: lhr.audits.interactive.numericValue,
        'speed-index': lhr.audits['speed-index'].numericValue,
        'total-blocking-time': lhr.audits['total-blocking-time'].numericValue,
        perfscore: lhr.categories.performance.score,
        'first-contentful-paint':
            lhr.audits['first-contentful-paint'].numericValue,
        'first-cpu-idle': lhr.audits['first-cpu-idle'].numericValue,
        'bootup-time': lhr.audits['bootup-time'].numericValue,
        'server-response-time': lhr.audits['server-response-time'].numericValue,
        emulatedFormFactor: lhr.configSettings.emulatedFormFactor,
        lighthouseVersion: lhr.lighthouseVersion,
    };
}

function waitRandomSeconds(sec) {
    const randomSec = Math.random() * sec + 1;
    const randomMs = Math.floor(randomSec * 1000);
    return new Promise((resolve) => {
        setTimeout(resolve, randomMs);
        // eslint-disable-next-line no-console
        console.log('  waiting', `${randomSec.toFixed(1)}s`);
    });
}

async function compute(currentData) {
    const existingGroup = currentData.groups.find(
        (x) => x.TEST_DESCRIPTION === TEST_DESCRIPTION,
    );
    const group = existingGroup || { TEST_URL, TEST_DESCRIPTION, runs: [] };
    if (!existingGroup) {
        currentData.groups.push(group);
    }

    for (let i = 0; i < NUM_RUNS; i += 1) {
        /* eslint-disable no-await-in-loop */
        // eslint-disable-next-line no-console
        console.log('starting run', i + 1, 'of', NUM_RUNS);
        const results = await launchChromeAndRunLighthouse(
            TEST_URL,
            lighthouseConfig,
        );
        await writeFile(results.report);
        group.runs.push(getResultObject(results.lhr));
        if (i < NUM_RUNS - 1) {
            await waitRandomSeconds(3);
        }
        /* eslint-enable no-await-in-loop */
    }
    return currentData;
}
function writeCurrent(data) {
    return fs.writeFile(dataFile, JSON.stringify(data, null, 2), { flag: 'w' });
}

async function readCurrent() {
    try {
        const data = await fs.readFile(dataFile);
        return JSON.parse(data);
    } catch (e) {
        // eslint-disable-next-line no-console
        console.log('unable to read current');
        return { groups: [] };
    }
}

readCurrent()
    .then(compute)
    .then(writeCurrent)
    .then(() => {
        // eslint-disable-next-line no-console
        console.log('done');
        // eslint-disable-next-line no-console
        console.log('\u0007');
    });
