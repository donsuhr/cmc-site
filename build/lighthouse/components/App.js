/* global httpVueLoader */

export default {
    name: 'App',
    components: {
        GroupList: httpVueLoader('./components/GroupList.vue'),
    },
    template: `
    <div class="container mx-auto p-4">
       <h1>Groups</h1>
      <group-list class="mt-6"></group-list>
    </div>
  `,
};
