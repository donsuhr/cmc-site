#!/usr/bin/env bash
yarn remove cmc-write-config cmc-load-dot-env metalsmith-add-orig-file-name

yarn add \
git+ssh://git@bitbucket.org/donsuhr/cmc-write-config.git \
git+ssh://git@bitbucket.org/donsuhr/cmc-load-dot-env.git \
git+ssh://git@bitbucket.org/donsuhr/metalsmith-add-orig-file-name.git \
