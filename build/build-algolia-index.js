'use strict';

const { argv } = require('yargs');
const loadDotEnv = require('cmc-load-dot-env');
const algoliasearch = require('algoliasearch');

const { buildSearchIndex } = require('build-algolia-index');

if (!{}.hasOwnProperty.call(process.env, 'CORS_ORIGIN_TLD')) {
    // eslint-disable-next-line no-console
    console.log('no CORS_ORIGIN_TLD set, will load .env');
    loadDotEnv();
}

const searchClient = algoliasearch(
    process.env.ALGOLIA_APPID,
    process.env.ALGOLIA_ADMIN_KEY,
);

const dryRun = !!argv['dry-run'];

const groups = [
    {
        json: 'search-index--cmc--en-us.json',
        algoliaIndex: 'cmc--en-us',
        src: [
            'app/pages/**/*.html',
            '!app/pages/dev/**/*.html',
            '!app/pages/demos/**/*.html',
            '!app/pages/blog/**/*.html',
            '!app/pages/snippets-and-examples/**/*.html',
            '!app/pages/en-in/**/*.html',
            '!app/pages/pt-br/**/*.html',
        ],
    },
];

buildSearchIndex(
    groups,
    searchClient,
    dryRun,
    process.env.CORS_ORIGIN_SELF,
    process.env.CORS_ORIGIN_API,
);
