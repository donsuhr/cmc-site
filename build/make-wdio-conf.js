/**
 hub: selenium-standalone start -- -role hub

 ie10machine:  selenium-standalone start -- -role node ^
 -hub http://192.168.1.10:4444/grid/register ^
 -browser "browserName=internet explorer, version=10"

 ieEdgeMachine: selenium-standalone start -- -role node ^
 -hub http://192.168.1.10:4444/grid/register ^
 -browser "browserName=internet explorer, version=11" ^
 -browser "browserName=chrome" ^
 -browser "browserName=MicrosoftEdge" ^
 -browser "browserName=firefox"

 safari10Machine: selenium-standalone start -- -role node \
 -hub http://192.168.1.10:4444/grid/register/ \
 -browser "browserName=safari, version=10" \
 -browser "browserName=firefox" \
 -browser "browserName=chrome"

 safari9Machine: java -jar /Users/bill/Downloads/selenium-server-standalone-2.53.1.jar \
 -role node \
 -hub http://192.168.1.10:4444/grid/register/ \
 -browser "browserName=safari, version=9, platform=MAC"

 */

'use strict';

const path = require('path');
const fs = require('fs');
const { argv } = require('yargs');
const cloneDeep = require('lodash/cloneDeep');
const base = require('../wdio.base.conf');

const includeChrome = argv._.includes('chrome');
const includeFirefox = argv._.includes('firefox');
const includeIe11 = argv._.includes('ie11');
const includeIe10 = argv._.includes('ie10');
const includeEdge = argv._.includes('edge');
const includeSafari10 = argv._.includes('safari10');
const includeSafari9 = argv._.includes('safari9');

const outputFile = path.resolve(__dirname, '../wdio.generated.conf');

const conf = cloneDeep(base.config);
conf.capabilities = [];
if (includeChrome) {
    conf.capabilities.push({
        browserName: 'chrome',
        loggingPrefs: { browser: 'ALL' },
    });
}

if (includeFirefox) {
    conf.capabilities.push({
        browserName: 'firefox',
        loggingPrefs: { browser: 'ALL' },
    });
}

if (includeIe10) {
    conf.capabilities.push({
        browserName: 'internet explorer',
        version: '10',
        loggingPrefs: { browser: 'ALL' },
    });
}

if (includeIe11) {
    conf.capabilities.push({
        browserName: 'internet explorer',
        version: '11',
        loggingPrefs: { browser: 'ALL' },
    });
}

if (includeEdge) {
    conf.capabilities.push({
        browserName: 'MicrosoftEdge',
        loggingPrefs: { browser: 'ALL' },
    });
}

if (includeSafari10) {
    conf.capabilities.push({
        browserName: 'safari',
        version: '10',
        loggingPrefs: { browser: 'ALL' },
    });
}

if (includeSafari9) {
    conf.capabilities.push({
        browserName: 'safari',
        version: '9',
        loggingPrefs: { browser: 'ALL' },
    });
}

conf.beforePlaceholder = {};
let result = `exports.config = ${JSON.stringify(conf, null, '\t')}`;
result = result.replace(
    '"beforePlaceholder": {}',
    `before(capabilities, specs) {
        // eslint-disable-next-line import/no-extraneous-dependencies, global-require
        const chai = require('chai');
        // eslint-disable-next-line import/no-extraneous-dependencies, global-require
        const chaiAsPromised = require('chai-as-promised');

        chai.Should();
        chai.use(chaiAsPromised);
        chaiAsPromised.transferPromiseness = browser.transferPromiseness;
        global.expect = chai.expect;
    },`,
);

fs.writeFile(outputFile, result, 'utf8', (writeConfigError) => {
    if (writeConfigError) {
        // eslint-disable-next-line no-console
        console.log(
            'error writing file, wdio.generated.conf',
            writeConfigError,
        );
    } else {
        // eslint-disable-next-line no-console
        console.log(`Generated new ${outputFile}`);
    }
});
