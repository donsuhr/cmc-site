'use strict';

const purify = require('purify-css'); // eslint-disable-line import/no-extraneous-dependencies

const css = ['.tmp/styles/pages/**/!(styleguide).css', '.tmp/styles/main.css'];
const content = [
    'dist/**/*.html',
    'app/components/**/*.hbs',
    'app/metalsmith/**/*.hbs',
    'app/components/**/*.html',
];
const whitelist = [
    '*carousel*',
    '*data-sal*',
    'parsley-required',
    '*headroom*',
    'applet',
    'object',
    'abbr',
    'acronym',
    'cite',
    'dfn',
    'ins',
    'kbd',
    'samp',
    'strike',
    'tt',
    'legend',
    'caption',
    'tfoot',
    'hgroup',
    'ruby',
    'audio',
    'caption',
    'video-wrapper--16x9 object',
    'video-wrapper--4x3 object',
    'modal-scrollbar-measure',
];

const options = {
    rejected: true,
    whitelist,
};

purify(content, css, options, (result) => {
    // console.log(result);
});
