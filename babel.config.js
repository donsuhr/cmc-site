'use strict';

module.exports = function config(api) {
    api.cache(true);

    const presets = [
        [
            '@babel/preset-env',
            {
                modules: false,
                loose: true,
                useBuiltIns: 'usage',
                corejs: { version: '3.6', proposals: true },
            },
        ],
    ];
    const plugins = ['@babel/plugin-syntax-dynamic-import'];

    if (process.env.NODE_ENV === 'test') {
        return {
            presets: ['@babel/preset-env'],
            plugins: [
                'istanbul',
                [
                    'module-resolver',
                    {
                        alias: {
                            config: './config.js',
                        },
                    },
                ],
            ],
        };
    }
    return {
        presets,
        plugins,
    };
};
